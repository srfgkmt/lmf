#!/bin/bash

SRC=${1:-./}

echo "I found the following configuration keys in $SRC:"
echo

grep -Erh --include *.java '.get[A-Z][a-z]+Configuration *\("[^"]*"(\)|,)' "$SRC" | \
    sed 's/.*get\([A-Z][a-z]*\)Configuration *("\([^""]*\)" *\()\|, *\([^)]*\)\).*/\2 :: \1 (\4)/g' | \
    sort | uniq

