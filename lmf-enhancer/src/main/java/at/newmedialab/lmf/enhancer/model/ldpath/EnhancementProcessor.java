/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.model.ldpath;

import org.apache.marmotta.ldpath.api.transformers.NodeTransformer;
import org.openrdf.model.Value;

import at.newmedialab.lmf.enhancer.services.program.EnhancerProgramServiceImpl;
import at.newmedialab.lmf.enhancer.services.enhancer.EnhancerEngineServiceImpl;
/**
 * Interface for all enhancement result processors used by the lmf-enhancer module. Implemented as a LDPath
 * {@link NodeTransformer} so it easily integrates into LDPath programs. Instances should be @ApplicationScoped services
 * that are automatically injected by the {@link EnhancerProgramServiceImpl}.
 * <p/>
 * The transform() method of the processor needs to implement the actual enhancement, e.g. by calling Apache Stanbol.
 * The method should return a collection of all URIs that identify enhancement structures created by the enhancer and
 * a repository containing the triples representing the actual enhancement. Both are encapsulated in an
 * {@link EnhancementResult} object. These will be used by the  {@link EnhancerEngineServiceImpl} to add relations from
 * the original resource to the enhancement result.
 * <p/>
 * Author: Sebastian Schaffert (sschaffert@apache.org)
 */
public interface EnhancementProcessor extends NodeTransformer<EnhancementResult,Value> {


    /**
     * Return the namespace URI used for registering this result transformer.
     * <p/>
     * Example: "http://www.newmedialab.at/lmf/stanbol/"
     * <p/>
     * @return the namespace URI of the transformer
     */
    public String getNamespaceUri();


    /**
     * Return the namespace prefix used for registering this result transformer as it will
     * be used in LDPath programs.
     * <p/>
     * Example: "stanbol"
     * <P/>
     * @return the namespace prefix used for registering the transformer
     */
    public String getNamespacePrefix();


    /**
     * Return the transformer name used for registering this result transformer.
     * <p/>
     * Example: "engine"
     * <p/>
     * @return the name of the transformer as it will be used in LDPath programs
     */
    public String getLocalName();

}
