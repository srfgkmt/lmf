/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.model.enhancer;

import at.newmedialab.lmf.worker.model.WorkerConfiguration;
import com.google.common.collect.Sets;
import org.apache.marmotta.commons.sesame.filter.SesameFilter;
import org.apache.marmotta.commons.sesame.filter.resource.ResourceFilter;
import org.apache.marmotta.ldpath.model.programs.Program;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;

import java.util.Set;

/**
 * Specification of services that allow enhancement of content associated with resources.
 * <p/>
 * Author: Sebastian Schaffert
 */
public class EnhancerConfiguration extends WorkerConfiguration {

    /**
     * The URI of the context / named graph used by this enhancement engine for storing enhancement results
     */
    private String enhancementContext;



    /**
     * After how many enhancements the optimization should be performed
     */
    private int optimizeLimit = 1000;

    /**
     * The program used to configure this enhancement engine
     */
    private Program<Value> program;


    /**
     * String representation of the program
     */
    private String programString;


    private EnhancerFilter filter;

    public EnhancerConfiguration(String name) {
        super(name);
    }


    @Override
    public String getType() {
        return "Enhancer";
    }

    /**
     * Get the URI of the context / named graph used by this enhancement engine for storing enhancement results
     */
    public String getEnhancementContext() {
        return enhancementContext;
    }

    /**
     * Set the URI of the context / named graph used by this enhancement engine for storing enhancement results
     */
    public void setEnhancementContext(String enhancementContext) {
        this.enhancementContext = enhancementContext;
    }


    public int getOptimizeLimit() {
        return optimizeLimit;
    }

    public void setOptimizeLimit(int optimizeLimit) {
        this.optimizeLimit = optimizeLimit;
    }



    public Program<Value> getProgram() {
        return program;
    }

    public void setProgram(Program<Value> program) {
        this.program = program;
    }


    public String getProgramString() {
        return programString;
    }

    public void setProgramString(String programString) {
        this.programString = programString;
    }

    public EnhancerFilter getEnhancerFilter() {
        if(filter == null) {
            filter = new EnhancerFilter(getEnhancementContext());
        }
        return filter;
    }

    /**
     * A filter implementation that does not accept statements produced by this enhancer. Used for avoiding
     * infinite loops through transaction commits.
     */
    private class EnhancerFilter implements SesameFilter<Statement> {

        private String context;

        private EnhancerFilter(String context) {
            this.context = context;
        }

        @Override
        public boolean accept(Statement object) {
            return !context.equals(object.getContext().stringValue());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnhancerFilter that = (EnhancerFilter) o;

            if (!context.equals(that.context)) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return context.hashCode();
        }
    }
}
