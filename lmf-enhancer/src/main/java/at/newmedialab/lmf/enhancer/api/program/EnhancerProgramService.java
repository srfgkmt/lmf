/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.enhancer.api.program;

import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementProcessor;
import org.apache.marmotta.ldpath.exception.LDPathParseException;
import org.apache.marmotta.ldpath.model.programs.Program;
import org.openrdf.model.Value;

import java.io.InputStream;
import java.io.Reader;
import java.util.Collection;

/**
 * This service provides initialisation and configuration for the LDPath backend as used by the enhancement
 * configuration, and offers convenience functions for parsing LDPath programs.
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface EnhancerProgramService {


    /**
     * Initialise the LDPath backend to use by the enhancer module and register the stanbol:engine transformer
     * (a normal string transformer that is interpreted by the EnhancerEngineService)
     */
    public void initialize() throws LDPathParseException;


    /**
     * Parse a program from the passed InputStream using the backend registered for the enhancer module.
     *
     *
     * @param program
     * @return
     * @throws LDPathParseException
     */
    public Program<Value> parseProgram(InputStream program) throws LDPathParseException;

    /**
     * Parse a program frim the passed Reader using the backend registered for the enhancer module.
     *
     * @param program
     * @return
     * @throws LDPathParseException
     */
    public Program<Value> parseProgram(Reader program) throws LDPathParseException;

    /**
     * return a collection of all registered enhancement processors
     * @return
     */
    Collection<EnhancementProcessor> getProcessors();
}
