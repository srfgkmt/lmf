/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.webservices.rss;

import at.newmedialab.lmf.social.api.rss.RSSService;
import at.newmedialab.lmf.social.model.rss.RSSFeedFacade;
import at.newmedialab.lmf.social.services.importer.RSSImporterImpl;
import at.newmedialab.lmf.social.services.rss.RSSServiceImpl;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.codehaus.jackson.annotate.JsonProperty;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.Variant;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

import static org.apache.marmotta.commons.sesame.model.Namespaces.MIME_TYPE_JSON;
import static org.apache.marmotta.commons.sesame.repository.ExceptionUtils.handleRepositoryException;


@ApplicationScoped
@Path(RSSWebService.SERVICE_PATH)
public class RSSWebService {

    public static final String SERVICE_PATH = "/social/rss";
    public static final String UPDATE_PATH  = "/cloud/update";

    @Inject
    private Logger          log;

    @Inject
    private RSSImporterImpl rssImporter;

    @Inject
    private RSSService      rssService;

    @Inject
    private SesameService sesameService;

    @GET
    @Produces(MIME_TYPE_JSON)
    public List<Feed> getFeeds() {
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {

                final Collection<RSSFeedFacade> rssF;// = facadingService.createFacade(rss,
                // RSSFeedFacade.class);
                rssF = rssService.getFeedFacades(conn);
                final ArrayList<Feed> result = new ArrayList<RSSWebService.Feed>();
                for (RSSFeedFacade f : rssF) {
                    final Feed json = new Feed(f);
                    json.scheduledUpdate = rssService.getScheduledUpdate(json.getUri());
                    result.add(json);
                }

                return result;
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException e) {
            handleRepositoryException(e, RSSServiceImpl.class);
            return Collections.emptyList();
        }
    }

    @POST
    public Response addFeed(@QueryParam("url") String url, @QueryParam("type") String format) {
        final URL u;
        try {
            u = new URL(url);
            final URLConnection con = u.openConnection();
            if (format == null) {
                format = con.getContentType();
            }
            if (format == null) {
                format = "application/rss+xml";
            }
            log.debug("received import request for news-feed '{}' ({})", url, format);
            if (!rssImporter.getAcceptTypes().contains(format)) return Response.notAcceptable(
                    Lists.transform(new ArrayList<String>(rssImporter.getAcceptTypes()), new Function<String, Variant>() {
                        @Override
                        public Variant apply(String input) {
                            return new Variant(MediaType.valueOf(input),(String)null,"UTF-8");
                        }
                    })).build();

            if (rssService.addFeed(url) > 0) return Response.status(202).build();
            else
                return Response.noContent().build();
        } catch (MalformedURLException e) {
            log.error("Invalid URL: '{}'", url);
            return Response.status(400).entity(e.getMessage()).build();
        } catch (IOException e) {
            log.error("IOException while loading RSS/ATOM feed: {}", e.getMessage());
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @PUT
    public Response updateFeed(@QueryParam("url") String url) {
        final URL u;
        try {
            u = new URL(url);
            final int updates = rssService.updateFeed(u);
            if (updates > 0) return Response.ok().entity("Updated " + updates + " items." ).build();
            else
                return Response.ok().entity("No updates found").build();
        } catch (MalformedURLException e) {
            log.error("Invalid URL: '{}'", url);
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path(UPDATE_PATH)
    public Response cloudUpdateConfirmation(@QueryParam("url") String url, @QueryParam("challenge") String challenge) {
        if (url == null) return Response.status(Status.BAD_REQUEST).entity("Parameter 'url' required").build();
        if (rssService.isKnownFeedSource(url)) {
            if (challenge != null) {
                log.debug("confirmed update notification request for <{}>", url);
                return Response.status(Status.ACCEPTED).entity(challenge).build();
            } else
                return cloudUpdate(url);
        } else {
            log.warn("Received request to confirm notification update registration for unknown feed {}",
                    url);
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    @POST
    @Path(UPDATE_PATH)
    public Response cloudUpdate(@FormParam("url") String url) {
        if (url == null) return Response.status(Status.BAD_REQUEST).entity("Parameter 'url' required").build();
        if (rssService.isKnownFeedSource(url)) {
            log.info("Received update ping for {}", url);
            rssService.updateFeed(url);
            return Response.status(Status.ACCEPTED).build();
        } else {
            log.warn("Received update ping for unknown feed {}", url);
            return Response.status(Status.NOT_FOUND).build();
        }
    }

    protected static class Feed {

        @JsonProperty
        protected String uri, title;

        @JsonProperty
        protected Date   created, updated, scheduledUpdate;

        public Feed(RSSFeedFacade facade) {
            uri = facade.getDelegate().toString();
            title = facade.getTitle();
            updated = facade.getUpdated();
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Date getUpdated() {
            return updated;
        }

        public void setUpdated(Date updated) {
            this.updated = updated;
        }

    }
}
