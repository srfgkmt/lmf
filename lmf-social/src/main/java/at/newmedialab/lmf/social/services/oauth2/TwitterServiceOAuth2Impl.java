/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services.oauth2;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.constraints.NotNull;

import at.newmedialab.lmf.social.api.twitter.TwitterService;
import at.newmedialab.lmf.social.model.google.GoogleUserProfile;
import at.newmedialab.lmf.social.model.twitter.TwitterUserProfile;
import java.io.IOException;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.apache.marmotta.platform.core.api.http.HttpClientService;
import org.apache.marmotta.platform.core.util.http.HttpRequestUtil;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.slf4j.Logger;

/**
 * Google service OAuth2 implemented with basi http requests
 * 
 * @todo: dietmar
 *
 */
@ApplicationScoped
public class TwitterServiceOAuth2Impl implements TwitterService {
        
    private static final String USERINFO = "https://www.googleapis.com/oauth2/v1/userinfo";
    
    @Inject
    private Logger log;

    @Inject
    private HttpClientService httpClientService;
    @Override
    public TwitterUserProfile getUserProfile(@NotNull String accessToken) {
         try {
            String response = getRequest(USERINFO, accessToken);
            ObjectMapper mapper = new ObjectMapper(); 
            TwitterUserProfile userProfile = mapper.readValue(response, TwitterUserProfile.class);
            if (StringUtils.isBlank(userProfile.getId())) {
                log.error("Response without valid ID");
                throw new UnsupportedOperationException("Response without valid ID");
            } else {
                return userProfile;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new UnsupportedOperationException("No user info found", e);
        }
    }
    private String getRequest(String uri, String accessToken) throws ClientProtocolException, IOException, JSONException {
        HttpGet get = new HttpGet(uri + "?access_token=" + accessToken);
        HttpRequestUtil.setUserAgentString(get, "LMF Social Module");
        get.setHeader("Accept", "application/json");
        return httpClientService.execute(get, new ResponseHandler<String>() {
            @Override
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                if (200 == response.getStatusLine().getStatusCode()) {
                    HttpEntity entity = response.getEntity();
                    return EntityUtils.toString(entity);
                } else {
                    throw new IOException("Resource not found");
                }
            }
        });
    }
}
