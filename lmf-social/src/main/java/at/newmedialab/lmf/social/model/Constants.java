/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.model;


import org.apache.marmotta.commons.sesame.model.Namespaces;

/**
 * Created by IntelliJ IDEA.
 * User: sstroka
 * Date: 28.07.2011
 * Time: 12:11:37
 * To change this template use File | Settings | File Templates.
 */
public class Constants {

    public static final String NS_KIWI_SOCIAL    = "http://www.kiwi-project.eu/kiwi/social/";

    public static final String NS_FB    = "http://www.facebook.com/";
    public static final String NS_FB_USER    = "http://www.facebook.com/";
    public static final String NS_FB_VIDEO    = "http://www.facebook.com/v/";
    public static final String NS_GOOGLE    = "http://www.google.com/";
    // TODO: find correct namespaces...
    public static final String NS_GOOGLE_USER    = "http://www.google.com/user/";
    public static final String NS_GOOGLE_VIDEO    = "http://www.google.com/video/";

    public static final String NS_YOUTUBE    = "http://www.youtube.com/";
    public static final String NS_YOUTUBE_USER    = "http://www.youtube.com/user/";
    public static final String NS_YOUTUBE_VIDEO    = "http://www.youtube.com/video/";
    public static final String NS_YOUTUBE_SCHEMA = "http://gdata.youtube.com/schemas/2007#";

    public static final String NS_FCP_RSS = Namespaces.NS_FCP_CORE + "rss/";

}
