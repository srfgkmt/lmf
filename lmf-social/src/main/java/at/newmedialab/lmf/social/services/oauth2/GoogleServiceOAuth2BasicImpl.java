/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services.oauth2;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.apache.marmotta.commons.http.UriUtil;
import org.apache.marmotta.platform.core.api.http.HttpClientService;
import org.apache.marmotta.platform.core.api.triplestore.ContextService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.ldpath.api.LDPathService;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import at.newmedialab.lmf.social.api.google.GoogleService;
import at.newmedialab.lmf.social.cache.CacheHelper;
import at.newmedialab.lmf.social.model.UserAccount;
import at.newmedialab.lmf.social.model.google.GoogleUserProfile;

/**
 * Google service OAuth2 implemented with just basic http requests
 * 
 * @author Sergio Fernández
 *
 */
@ApplicationScoped
public class GoogleServiceOAuth2BasicImpl implements GoogleService {
    
    private static final String USERINFO = "https://www.googleapis.com/oauth2/v1/userinfo";
    
    //https://developers.google.com/youtube/2.0/reference#Standard_feeds	
    private static final String UPLOADS = "https://gdata.youtube.com/feeds/api/users/default/uploads";
    private static final String FAVORITES = "https://gdata.youtube.com/feeds/api/users/default/favorites";
    private static final String HISTORY = "https://gdata.youtube.com/feeds/api/users/default/watch_history";
    
    @Inject
    private Logger log;

    @Inject
    private HttpClientService httpClientService;
	
	@Inject
	SesameService sesameService;
	
	@Inject
	ContextService contextService;
	
	@Inject
	LDPathService ldpathService;
    
    @Override
    public GoogleUserProfile getUserProfile(@NotNull String accessToken) {
        try {
            String response = getRequest(USERINFO, accessToken);
            ObjectMapper mapper = new ObjectMapper(); 
            GoogleUserProfile userProfile = mapper.readValue(response, GoogleUserProfile.class);
            if (StringUtils.isBlank(userProfile.getId())) {
                log.error("Response without valid ID");
                throw new UnsupportedOperationException("Response without valid ID");
            } else {
                return userProfile;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new UnsupportedOperationException("No user info found", e);
        }
    }
    
    private String getRequest(String uri, String accessToken) throws ClientProtocolException, IOException, JSONException {
        return getRequest(uri, accessToken, "");
    }

	private String getRequest(String uri, String accessToken, String optional)
			throws ClientProtocolException, IOException {
		String url = uri + "?access_token=" + accessToken + optional;
		log.error("Requesting '{}'...", url);
		HttpGet get = new HttpGet(url);
        return httpClientService.execute(get, new ResponseHandler<String>() {
            @Override
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                if (200 == response.getStatusLine().getStatusCode()) {
                    HttpEntity entity = response.getEntity();
                    return EntityUtils.toString(entity);
                } else {
                    throw new IOException(response.getStatusLine().getReasonPhrase());
                }
            }
        });
	}

	@Override
	public void retrieveContent(UserAccount account, String context, String accessToken) {
		Resource ctx = (UriUtil.validate(context) ? contextService.createContext(context) : contextService.getDefaultContext());
		retrieveYoutubeContent(account, accessToken, ctx);
		//what else?
	}

	private void retrieveYoutubeContent(UserAccount account, String accessToken, Resource context) {
		String ldpath = "@prefix mao : <http://www.w3.org/ns/ma-ont#>; \n" +
						"title = mao:title :: xsd:string; \n" +
						"description = mao:description :: xsd:string; \n";
		
		Map<String,Set<String>> videos = new HashMap<String, Set<String>>();
        videos.put("http://gdata.youtube.com/schemas/2007#uploads", retrieveYoutubeFeed(UPLOADS, accessToken));
        videos.put("http://gdata.youtube.com/schemas/2007#favorites", retrieveYoutubeFeed(FAVORITES, accessToken));
        videos.put("http://gdata.youtube.com/schemas/2007#watched", retrieveYoutubeFeed(HISTORY, accessToken));
        
		try {
			RepositoryConnection conn = sesameService.getConnection();
			try {
	            conn.begin();
	            ValueFactory valueFactory = conn.getValueFactory();
	            Resource subject = valueFactory.createURI(account.getUri());            
	            for (Map.Entry<String, Set<String>> entry : videos.entrySet()) {
	            	URI predicate = valueFactory.createURI(entry.getKey());
	            	for (String value: entry.getValue()) {
	            		Resource object = valueFactory.createURI(value);
	            		conn.add(subject, predicate, object, context);
	            		CacheHelper.update(ldpathService, object, ldpath);
	            	}
	            }           
	        } finally {
	            conn.commit();
	            conn.close();
	        }
		} catch (RepositoryException e) {
			throw new RuntimeException("Error creating user account: " + e.getMessage(), e);
		}	            
	}

	private HashSet<String> retrieveYoutubeFeed(String feed, String accessToken) {
		HashSet<String> videos = new HashSet<String>();
        try {
            String response = getRequest(feed, accessToken, "&alt=json&v=2");
            try {
	            JSONObject json = new JSONObject(response);
	            if (json.has("feed")) {
	            	JSONArray entries = json.getJSONObject("feed").getJSONArray("entry");
	            	for (int i = 0; i < entries.length(); i++) {
	            	    JSONObject video = entries.getJSONObject(i);             	   
	            	    String id = video.getJSONObject("media$group").getJSONObject("yt$videoid").getString("$t");
	            	    videos.add("http://youtu.be/" + id);
	            	}
	            } else {
	                log.error("Response for {} without valid json feed", feed);
	            }
            } catch (JSONException e) {
            	log.error("Json exception, so switching to gson for {}....", feed);
                JsonObject json = new Gson().fromJson(response, JsonObject.class);
                if (json.has("feed")) { //TODO: this does not work... read the gson api
                	JsonArray entries = json.get("feed").getAsJsonObject().get("entry").getAsJsonArray();
                	for (int i = 0; i < entries.size(); i++) {
                		JsonObject video = entries.get(i).getAsJsonObject();
                	    String id = video.get("media$group").getAsJsonObject().get("yt$videoid").getAsJsonObject().get("$t").getAsString();
                	    videos.add("http://youtu.be/" + id);
                	}
                } else {
                	log.error("Response for {} without valid gson feed", feed);
                }
            }
        } catch (Exception e) {
            log.error("Error requesting {} feed: {}", feed, e.getMessage());
        }
		return videos;
	}

}
