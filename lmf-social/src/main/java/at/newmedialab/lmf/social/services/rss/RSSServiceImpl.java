/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services.rss;

import at.newmedialab.lmf.scheduler.api.QuartzService;
import at.newmedialab.lmf.social.api.rss.RSSService;
import at.newmedialab.lmf.social.model.rss.RSSCategoryFacade;
import at.newmedialab.lmf.social.model.rss.RSSEntryFacade;
import at.newmedialab.lmf.social.model.rss.RSSFeedFacade;
import at.newmedialab.lmf.social.model.rss.RSSUpdateMechanism;
import at.newmedialab.lmf.social.services.rss.quartz.CloudRegistrationJob;
import at.newmedialab.lmf.social.services.rss.quartz.RSSUpdaterJob;
import com.sun.syndication.feed.module.SyModule;
import com.sun.syndication.feed.rss.Channel;
import com.sun.syndication.feed.rss.Cloud;
import com.sun.syndication.feed.synd.*;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import org.apache.marmotta.commons.sesame.facading.FacadingFactory;
import org.apache.marmotta.commons.sesame.model.Namespaces;
import org.apache.marmotta.commons.sesame.repository.ResourceUtils;
import org.apache.marmotta.kiwi.model.rdf.KiWiResource;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.api.user.UserService;
import org.apache.marmotta.platform.core.exception.UserExistsException;
import org.apache.marmotta.platform.core.model.user.MarmottaUser;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.quartz.*;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.regex.Pattern;

import static org.apache.marmotta.commons.sesame.repository.ExceptionUtils.handleRepositoryException;

@ApplicationScoped
public class RSSServiceImpl implements RSSService {

    private static final String RSS_FEED_TYPE = Namespaces.NS_RSS + "channel";

    private static final String  JOB_GROUP       = "RSS_UPDATING";
    private static final String  UPDATE_JOB_ID   = RSSServiceImpl.class.getName() + "#update";
    private static final String  CLOUD_JOB_KEY   = RSSServiceImpl.class.getName() + "#cloud";

    private static final Pattern HASHTAG_PATTERN = Pattern.compile("(^|[^&])#([A-Za-z0-9_]+)($|[^;])");

    @Inject
    private Logger          log;

    @Inject
    private SesameService sesameService;

    @Inject
    private UserService userService;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private QuartzService quartzService;

    private final JobKey         updateJobKey, cloudRegistrationJobKey;

    @Inject
    public RSSServiceImpl() {
        updateJobKey = new JobKey(UPDATE_JOB_ID, JOB_GROUP);
        cloudRegistrationJobKey = new JobKey(CLOUD_JOB_KEY, JOB_GROUP);
    }

    @Override
    public int addFeed(String feedUrl) {
        try {
            return addFeed(new URL(feedUrl));
        } catch (MalformedURLException e) {
            log.error("Invalid URL {}: {}", feedUrl, e.getMessage());
            return 0;
        }
    }

    @PostConstruct
    public void init()  {
        log.info("RSSServiceImpl is starting up...");

        try {
            final JobBuilder jb = JobBuilder.newJob(RSSUpdaterJob.class).storeDurably().withIdentity(updateJobKey);
            // Add additional setting here
            // jb.usingJobData("fooValue", "BAR");
            quartzService.addJob(jb.build(), true);

            final JobBuilder jb2 = JobBuilder.newJob(CloudRegistrationJob.class).storeDurably().withIdentity(cloudRegistrationJobKey);
            // Add additional setting here
            // jb2.usingJobData("fooValue", "BAR");
            quartzService.addJob(jb2.build(), true);
        } catch (SchedulerException e) {
            log.error("error initialising RSS scheduler task, scheduling not available!");
        }
    }

    @Override
    public int addFeed(URL feedUrl) {
        return importFeed(feedUrl, userService.getCurrentUser());
    }

    @Override
    public Collection<URI> getFeeds() {
        try {
            RepositoryConnection conn = sesameService.getConnection();

            try {
                final URI rdf_type      = conn.getValueFactory().createURI(Namespaces.NS_RDF + "type");
                final URI rss_feed_type = conn.getValueFactory().createURI(RSS_FEED_TYPE);
                final ArrayList<URI> rses = new ArrayList<URI>();

                RepositoryResult<Statement> triples = conn.getStatements(null, rdf_type, rss_feed_type, true);
                while(triples.hasNext()) {
                    Statement t = triples.next();
                    if (t.getSubject() instanceof URI) {
                        rses.add((URI) t.getSubject());
                    }
                }
                triples.close();

                return rses;
            } finally {
                conn.commit();
                conn.close();
            }
        } catch(RepositoryException ex) {
            handleRepositoryException(ex,RSSServiceImpl.class);
            return Collections.emptyList();
        }
    }

    @Override
    public Collection<RSSFeedFacade> getFeedFacades(RepositoryConnection connection) {
        return FacadingFactory.createFacading(connection).createFacade(getFeeds(), RSSFeedFacade.class);
    }

    @Override
    public URI getFeed(String feedUrl) {
        try {
            return getFeed(new URL(feedUrl));
        } catch (MalformedURLException e) {
            log.error("Invalid URL {}: {}", feedUrl, e.getMessage());
            return null;
        }
    }

    @Override
    public URI getFeed(URL feedUrl) {
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {
                URI f = ResourceUtils.getUriResource(conn, feedUrl.toExternalForm());
                if (ResourceUtils.hasType(conn,f, Namespaces.NS_RDF + "type"))
                    return f;
                else
                    return null;
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException ex) {
            handleRepositoryException(ex,RSSServiceImpl.class);
            return null;
        }
    }

    @Override
    public RSSFeedFacade getFeedFacade(String feedUrl, RepositoryConnection connection) {
        try {
            return getFeedFacade(new URL(feedUrl), connection);
        } catch (MalformedURLException e) {
            log.error("Invalid URL {}: {}", feedUrl, e.getMessage());
            return null;
        }
    }

    @Override
    public RSSFeedFacade getFeedFacade(URL feedUrl, RepositoryConnection connection) {
        return FacadingFactory.createFacading(connection).createFacade(getFeed(feedUrl), RSSFeedFacade.class);
    }

    @Override
    public int updateFeed(String feedUrl) {
        try {
            return updateFeed(new URL(feedUrl));
        } catch (MalformedURLException e) {
            log.error("Invalid URL {}: {}", feedUrl, e.getMessage());
            return 0;
        }
    }

    @Override
    public int updateFeed(URL feedUrl) {
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {
                return updateFeed(conn.getValueFactory().createURI(feedUrl.toExternalForm()),conn);
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException ex) {
            handleRepositoryException(ex,RSSServiceImpl.class);
            return 0;
        }
    }

    @Override
    public int updateFeed(URI feed) {
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {
                return updateFeed(feed,conn);
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException ex) {
            handleRepositoryException(ex,RSSServiceImpl.class);
            return 0;
        }
    }

    protected int updateFeed(URI feed, RepositoryConnection conn) throws RepositoryException {
        if (ResourceUtils.hasType(conn,feed, RSS_FEED_TYPE)) {
            final RSSFeedFacade facade = FacadingFactory.createFacading(conn).createFacade(feed, RSSFeedFacade.class);
            URL url = null;
            if (facade.getFeedSource() != null) {
                try {
                    url = new URL(facade.getFeedSource());
                } catch (MalformedURLException e) {
                }
            }
            if (url == null) {
                try {
                    url = new URL(feed.stringValue());
                } catch (MalformedURLException e1) {
                }
            }
            if (url == null) {
                log.error("could not update feed <{}> because of invalid update source '{}'", feed, facade.getFeedSource());
                return 0;
            }

            try {
                final SyndFeedInput input = new SyndFeedInput();
                input.setPreserveWireFeed(true);

                SyndFeed syndFeed = input.build(new XmlReader(url));

                return importFeed(feed, syndFeed, url, userService.getCurrentUser(),conn);
            } catch (FeedException ex) {
                log.error("RSS/Atom feed could not be parsed", ex);
            } catch (IOException ex) {
                log.error("I/O error while building feed from input stream source", ex);
            }
        }
        return 0;
    }


    @SuppressWarnings("unchecked")
    protected int importFeedEntry(final RSSFeedFacade feedF, final SyndEntry entry, URI user, RepositoryConnection conn) {
        final String entryURI = entry.getUri() != null ? entry.getUri() : entry.getLink();
        URI entryR = ResourceUtils.getUriResource(conn,entryURI);
        boolean isnew = false;
        if (entryR == null) {
            entryR = conn.getValueFactory().createURI(entryURI);
            isnew = true;
            if (entry.getAuthor() != null) {
                if (userService.userExists(entry.getAuthor())) {
                    user = userService.getUser(entry.getAuthor());
                } else if (configurationService.getBooleanConfiguration("social.rss.auto-create-author", false)) {
                    try {
                        user = userService.createUser(entry.getAuthor());
                    } catch (UserExistsException e) {}
                }
            }
            if(entryR instanceof KiWiResource) {
                ((KiWiResource)entryR).setCreated(entry.getPublishedDate());
            }
        }
        log.debug("feed entry: {} ({})", entry.getTitle(), entry.getUri());

        RSSEntryFacade entryF = FacadingFactory.createFacading(conn).createFacade(entryR, RSSEntryFacade.class);
        entryF.setTitle(entry.getTitle());
        if (entry.getDescription()!= null) {
            entryF.setDescription(entry.getDescription().getValue());
        }
        entryF.setFeed(feedF);
        entryF.setLink(entry.getLink());
        entryF.setDate(entry.getUpdatedDate() != null ? entry.getUpdatedDate() : entry.getPublishedDate());

        LinkedList<RSSCategoryFacade> cats = entryF.getCategories();
        if (cats == null) {
            cats = new LinkedList<RSSCategoryFacade>();
            entryF.setCategories(cats);
        }
        for (SyndCategory sc : entry.getCategories()) {
            // FIXME: How to create/generate the Category-URI?
            sc.hashCode();
        }

        LinkedList<MarmottaUser> authors = entryF.getAuthors();
        if (authors == null) {
            authors = new LinkedList<MarmottaUser>();
        }
        for (SyndPerson sp : entry.getAuthors()) {
            if (sp.getUri() != null) {
                URI r = conn.getValueFactory().createURI(sp.getUri());
                MarmottaUser a = FacadingFactory.createFacading(conn).createFacade(r, MarmottaUser.class);
                if (sp.getEmail() != null) {
                    a.setMbox("mailto:" + sp.getEmail());
                }
                a.setNick(sp.getName());
                if (!authors.contains(a)) {
                    authors.add(a);
                }
            }
        }
        entryF.setAuthors(authors);

        LinkedList<String> content = entryF.getContent();
        if (content == null) {
            content = new LinkedList<String>();
        }
        for (SyndContent sp : entry.getContents()) {
            String c = sp.getValue();
            if (c != null) {
                if (!content.contains(c)) {
                    content.add(c);
                }
            }
        }
        entryF.setContent(content);

        // scan for Twitter-style hash tags in title, descr and content (e.g. #kiwiknows, see
        // KIWI-622)
        /*
        Matcher m_hashtag = HASHTAG_PATTERN.matcher(String.format("%s%n%s%n%s", entryF.getTitle(), entryF.getDescription(), entryF.getContent()));
        while(m_hashtag.find()) {
            String tag_label = m_hashtag.group(2);
            if (!taggingService.hasTag(entryR, tag_label)) {
                Collection<Tag> _tags = taggingService.getTagsByLabel(tag_label);

                Tag tag = null;
                if(_tags.size() > 0) {
                    tag = _tags.iterator().next();
                } else {
                    tag = taggingService.createTag(tag_label);
                }
                taggingService.createTagging(tag_label, entryR, tag.getDelegate(), facadingService.createFacade(user, KiWiUser.class));
            }
        }
        */

        // check for geo information
        /*
    	 GeoRSSModule geoRSSModule = GeoRSSUtils.getGeoRSS(entry);
    	 if(geoRSSModule != null && geoRSSModule.getPosition() != null) {
    		 POI poi = facadingService.createFacade(item.getDelegate(), POI.class);
    		 poi.setLatitude(geoRSSModule.getPosition().getLatitude());
    		 poi.setLongitude(geoRSSModule.getPosition().getLongitude());
    	 }
         */

        /*
    	 // check for media information
    	 MediaEntryModule mediaModule = (MediaEntryModule) entry.getModule( MediaModule.URI );
    	 if(mediaModule != null) {
    		 KiWiMediaContentLiteral[] media = mediaModule.getMediaContents();
    		 if(media.length > 0) {
    			 KiWiMediaContentLiteral m = media[0];
    			 if(m.getReference() instanceof UrlReference) {
    				 URL url = ((UrlReference)m.getReference()).getUrl();

    				 String type = m.getType();
    				 String name = url.getFile();
    				 if(name.lastIndexOf("/") > 0) {
    					 name = name.substring(name.lastIndexOf("/")+1);
    				 }

    				 log.debug("importing media data from URL #0", url.toString());

    				 try {
    					 InputStream is = url.openStream();

    					 ByteArrayOutputStream bout = new ByteArrayOutputStream();

    					 int c;
    					 while((c = is.read()) != -1) {
    						 bout.write(c);
    					 }

    					 byte[] data = bout.toByteArray();

    					 contentItemService.updateMediaContentItem(item, data, type, name);


    					 is.close();
    					 bout.close();
    				 } catch(IOException ex) {
    					 log.error("error importing media content from RSS stream");
    				 }
    			 } else {
    				 log.info("RSS importer can only import media with URL references");
    			 }
    		 } else {
    			 log.warn("media module found without content");
    		 }

    		 Category[] cats = mediaModule.getMetadata().getCategories();
    		 for(Category cat : cats) {
    			ContentItem _cat;

    			String label = cat.getLabel() != null? cat.getLabel() : cat.getValue();

    			if(!taggingService.hasTag(item, label)) {
    				if(cat.getScheme() != null) {
    					_cat = contentItemService.getContentItemByUri(cat.getScheme() + cat.getValue());
    					if(_cat == null) {
    						_cat = contentItemService.createExternContentItem(cat.getScheme() + cat.getValue());
    						contentItemService.updateTitle(_cat, label);
    						_cat.setCreator(user);
    						contentItemService.saveContentItem(_cat);
    					}
    					taggingService.createTagging(label, item, _cat, user);
    				} else {
    					_cat = contentItemService.getContentItemByTitle(label);
    					if(_cat == null) {
    						_cat = contentItemService.createContentItem();
    						contentItemService.updateTitle(_cat, label);
    						_cat.setCreator(user);
    						contentItemService.saveContentItem(_cat);
    					}
    					taggingService.createTagging(label, item, _cat, user);
    				}
    			}
    		 }
    	 }
         */

        /* the flush is necessary, because CIs or tags will
         * otherwise be created multiple times when they
         * appear more than once in one RSS feed */
        log.debug("imported content item '{}' with URI '{}'", entryF.getTitle(), entryF.getDelegate());
        return isnew ? 1 : 0;
    }

    /**
     * Import data from an RSS or atom feed using the ROME SyndFeed representation.
     *
     * @param feed the ROME rss/atom feed representation
     * @param user the user to use as author of all imported data
     * @return count of imported documents
     */
    protected int importFeed(final SyndFeed feed, final URL feedSrc, final URI user, RepositoryConnection conn) {
        final String feedUri = feedSrc != null ? feedSrc.toExternalForm() : feed.getUri() != null ? feed.getUri() : feed
                .getLink();
        if (log.isInfoEnabled()) {
            log.info("importing entries from {} feed '{}' found at '{}'", new Object[] { feed.getFeedType(), feed.getTitle(),
                    feedUri });
        }

        if (feedUri == null) {
            log.error("feed '{}' has neither uri nor link to reference", feed.getTitle());
            return 0;
        }

        URI feedR = conn.getValueFactory().createURI(feedUri);
        if (feedR instanceof KiWiResource) {
            ((KiWiResource)feedR).setCreated(feed.getPublishedDate());
        }

        return importFeed(feedR, feed, feedSrc, user,conn);
    }

    @SuppressWarnings("unchecked")
    protected int importFeed(final URI feedResource, final SyndFeed feed, final URL feedSrc, final URI user, RepositoryConnection conn) {
        RSSFeedFacade feedF = FacadingFactory.createFacading(conn).createFacade(feedResource, RSSFeedFacade.class);
        feedF.setUpdated(new Date());

        // Add/Update general feed props.
        feedF.setTitle(feed.getTitle());
        feedF.setDescription(feed.getDescription());
        feedF.setLanguage(feed.getLanguage());
        if (feed.getLink() != null) {
            feedF.setLink(feed.getLink());
        }
        if (feed.getImage() != null) {
            feedF.setImageUrl(feed.getImage().getUrl());
        }
        if (feedSrc != null) {
            feedF.setFeedSource(feedSrc.toExternalForm());
        } else {
            log.warn("No feedSource for {}, updates will not work!", feedResource);
        }
        // Check for availability of a cloud notification service.
        if (feed.originalWireFeed() instanceof Channel) {
            Channel ch = (Channel) feed.originalWireFeed();
            if (ch.getCloud() != null) {
                Cloud cloud = ch.getCloud();
                RSSUpdateMechanism um = feedF.getUpdateMechanism();
                if (um == null) {
                    um = FacadingFactory.createFacading(conn).createFacade(conn.getValueFactory().createBNode(), RSSUpdateMechanism.class);
                    feedF.setUpdateMechanism(um);
                }
                um.setProtocol(cloud.getProtocol());
                um.setDomain(cloud.getDomain());
                um.setPort(cloud.getPort());
                um.setPath(cloud.getPath());
                um.setProcedure(cloud.getRegisterProcedure());
            }
        }
        final SyModule syMod = (SyModule) feed.getModule(SyModule.URI);
        RSSUpdateMechanism um = feedF.getUpdateMechanism();
        if (um == null) {
            um = FacadingFactory.createFacading(conn).createFacade(conn.getValueFactory().createBNode(), RSSUpdateMechanism.class);
            feedF.setUpdateMechanism(um);
        }

        { // scope blog: interval
            final int freq;
            final IntervalUnit interval;
            if (syMod != null) {
                um.setPollingBaseDate(syMod.getUpdateBase());
                String period = syMod.getUpdatePeriod();
                if (period == null) {
                    period = SyModule.DAILY;
                }
                freq = Math.max(syMod.getUpdateFrequency(), 1);

                if (period.equals(SyModule.HOURLY)) {
                    interval = IntervalUnit.HOUR;
                } else if (period.equals(SyModule.WEEKLY)) {
                    interval = IntervalUnit.WEEK;
                } else if (period.equals(SyModule.MONTHLY)) {
                    interval = IntervalUnit.MONTH;
                } else if (period.equals(SyModule.YEARLY)) {
                    interval = IntervalUnit.YEAR;
                } else {
                    interval = IntervalUnit.DAY;
                }
                um.setPollingPeriod(String.format("%d %S", freq, interval.toString()));
            } else {
                String ppS = um.getPollingPeriod();
                if (ppS == null) {
                    ppS = configurationService.getStringConfiguration("social.rss.default-update-interval", "2 DAYS");
                    um.setPollingPeriod(ppS);
                }
                freq = quartzService.parseTimeFromIntervalString(ppS);
                interval = quartzService.parseUnitFromIntervalString(ppS);
            }
            log.debug("scheduling updates");
            final TriggerBuilder<Trigger> t = TriggerBuilder.newTrigger().forJob(updateJobKey)
                    .usingJobData("feedUrl", feedF.getDelegate().toString());
            t.withIdentity(feedResource.stringValue() + "#update", JOB_GROUP);

            final CalendarIntervalScheduleBuilder sB = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withInterval(
                    freq, interval);
            t.startAt(DateBuilder.futureDate(freq, interval));
            t.withSchedule(sB);

            try {
                final Trigger trigger = t.build();
                final Date d;
                if (quartzService.checkExists(trigger.getKey())) {
                    d = quartzService.rescheduleJob(trigger.getKey(), trigger);
                } else {
                    d = quartzService.scheduleJob(trigger);
                }
                log.info("scheduled update for {}, next will happen {}", feedResource, d);
            } catch (SchedulerException e) {
                log.warn("Could not schedule update for {}: {}", feedF.getDelegate(), e.getMessage());
            }
        }

        LinkedList<RSSCategoryFacade> cats = feedF.getCategories();
        if (cats == null) {
            cats = new LinkedList<RSSCategoryFacade>();
            feedF.setCategories(cats);
        }
        for (SyndCategory sc : feed.getCategories()) {
            // FIXME: How to create/generate the Category-URI?
            sc.hashCode();
        }

        int entries = 0;
        for(final SyndEntry entry : feed.getEntries()) {
            entries += importFeedEntry(feedF, entry, user,conn);
        }

        if (feedF.getUpdateMechanism() != null && feedF.getUpdateMechanism().getProtocol() != null) {
            try {
                TriggerKey tk = new TriggerKey(feedResource.stringValue() + "#cloud", JOB_GROUP);
                if (!quartzService.checkExists(tk)) {
                    TriggerBuilder<Trigger> t = TriggerBuilder.newTrigger().forJob(cloudRegistrationJobKey);
                    t.usingJobData("feedResourceUri", feedResource.stringValue());
                    t.usingJobData("errorCount", 0);
                    t.withIdentity(tk);
                    quartzService.scheduleJob(t.build());
                }
            } catch (SchedulerException e) {
                log.warn("Could not schedule cloud registration for {}: {}", feedResource, e.getMessage());
            }
        } else {
            log.info("No UPDATEs...");
        }

        log.info("{} content items have been imported from RSS/Atom feed", entries);
        return entries;
    }

    @Override
    public int importFeed(URL url, URI user) {
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {

                final SyndFeedInput input = new SyndFeedInput();
                input.setPreserveWireFeed(true);

                final SyndFeed feed = input.build(new XmlReader(url));

                return importFeed(feed, url, user, conn);
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (FeedException ex) {
            log.error("RSS/Atom feed could not be parsed", ex);
        } catch (IOException ex) {
            log.error("I/O error while building feed from input stream source", ex);
        } catch (RepositoryException e) {
            handleRepositoryException(e,RSSServiceImpl.class);
        }
        return 0;
    }

    @Override
    public int importFeed(Reader r, URI user) {
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {

                final SyndFeedInput input = new SyndFeedInput();
                input.setPreserveWireFeed(true);

                final SyndFeed feed = input.build(r);

                return importFeed(feed, null, user,conn);
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (FeedException ex) {
            log.error("RSS/Atom feed could not be parsed", ex);
        } catch (RepositoryException e) {
            handleRepositoryException(e, RSSServiceImpl.class);
        }
        return 0;
    }

    @Override
    public boolean isFeed(Resource feed) {
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {

                return ResourceUtils.hasType(conn,feed, RSS_FEED_TYPE);
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException e) {
            handleRepositoryException(e, RSSServiceImpl.class);
            return false;
        }
    }

    @Override
    public boolean isKnownFeedSource(String url) {
        try {
            RepositoryConnection conn = sesameService.getConnection();
            try {

                if (url == null) return false;
                for (RSSFeedFacade feedFacade : getFeedFacades(conn)) {
                    if (url.equals(feedFacade.getDelegate().toString()) || url.equals(feedFacade.getFeedSource())) return true;
                }
                return false;
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException e) {
            handleRepositoryException(e, RSSServiceImpl.class);
            return false;
        }
    }

    @Override
    public Date getScheduledUpdate(URL url) {
        return getScheduledUpdate(url.toExternalForm());
    }

    @Override
    public Date getScheduledUpdate(String feedUrl) {
        try {
            final Trigger trigger = quartzService.getTrigger(new TriggerKey(feedUrl + "#update", JOB_GROUP));
            if (trigger != null) return trigger.getNextFireTime();
        } catch (SchedulerException e) {
            log.error("could not lookup scheduled update for {}: {}", feedUrl, e.getMessage());
        }
        return null;
    }
}
