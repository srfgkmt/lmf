/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.model.rss;

import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.model.Facade;

import java.util.Date;

import static at.newmedialab.lmf.social.model.Constants.NS_FCP_RSS;

public interface RSSUpdateMechanism extends Facade {

    /*
     * <cloud domain='blog.wordpress.com' port='80' path='/?rsscloud=notify' registerProcedure=''
     * protocol='http-post' />
     */
    @RDF(NS_FCP_RSS + "cloudUpdateProtocol")
    public String getProtocol();
    public void setProtocol(String protocol);

    @RDF(NS_FCP_RSS + "cloudUpdateDomain")
    public String getDomain();
    public void setDomain(String domain);

    @RDF(NS_FCP_RSS + "cloudUpdatePath")
    public String getPath();
    public void setPath(String path);

    @RDF(NS_FCP_RSS + "cloudUpdateProcedure")
    public String getProcedure();
    public void setProcedure(String procedure);

    @RDF(NS_FCP_RSS + "cloudUpdatePort")
    public int getPort();
    public void setPort(int port);


    /*
     * ISO_8601 Time Period
     */
    @RDF(NS_FCP_RSS + "updatePeriod")
    public String getPollingPeriod();
    public void setPollingPeriod(String period);

    @RDF(NS_FCP_RSS + "updatePeriodBase")
    public Date getPollingBaseDate();
    public void setPollingBaseDate(Date baseDate);

}
