/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.marmotta.commons.constants.Namespace.DCTERMS;
import org.apache.marmotta.commons.constants.Namespace.FOAF;
import org.apache.marmotta.commons.constants.Namespace.SIOC;
import org.apache.marmotta.commons.http.UriUtil;
import org.apache.marmotta.commons.vocabulary.SCHEMA;
import org.apache.marmotta.platform.core.api.triplestore.ContextService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.exception.MarmottaException;
import org.apache.marmotta.platform.ldpath.api.LDPathService;
import org.apache.marmotta.platform.sparql.api.sparql.SparqlService;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.SKOS;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.slf4j.Logger;

import at.newmedialab.lmf.social.api.UserAccountService;
import at.newmedialab.lmf.social.cache.CacheHelper;
import at.newmedialab.lmf.social.model.UserAccount;
import at.newmedialab.lmf.social.model.facebook.UserAccountFacebook;

/**
 * User account service implementation 
 * 
 * @author Sergio Fernández
 *
 */
@ApplicationScoped
public class UserAccountServiceImpl implements UserAccountService {
	
	private static final String VCARD = "http://www.w3.org/2006/vcard/ns#";
	private static final String DV = "http://rdf.data-vocabulary.org/#";

    @Inject
    private Logger log;
	
	@Inject
	SesameService sesameService;
	
	@Inject
	ContextService contextService;
	
	@Inject
	SparqlService sparqlService;
	
	@Inject
	LDPathService ldpathService;
	
	@Override
	public UserAccount save(UserAccount account) {
		return save(null, account, null);
	}

	@Override
	public UserAccount save(UserAccount account, String context) {
		return save(null, account, null);
	}
	
	@Override
	public UserAccount save(String person, UserAccount account) {
		return save(person, account, null);
	}

	@Override
	public UserAccount save(String person, UserAccount account, String context) {
		try {
			RepositoryConnection conn = sesameService.getConnection();
			try {
	            conn.begin();
	            ValueFactory valueFactory = conn.getValueFactory();
	        	Resource ctx = (UriUtil.validate(context) ? contextService.createContext(context) : contextService.getDefaultContext());
	            
				final Resource siocUserAccount;	            
	            if (StringUtils.isNotBlank(person)) {
	            	account.setUri(person + "/" + account.getService().substring(7) + "/" + account.getId());
	            	siocUserAccount = valueFactory.createURI(account.getUri());
	            	final Resource foafPerson = valueFactory.createURI(person);
					conn.add(foafPerson, RDF.TYPE, valueFactory.createURI(FOAF.Person), ctx);
	            	conn.add(foafPerson, valueFactory.createURI(FOAF.account), siocUserAccount, ctx);
	            	conn.add(siocUserAccount, valueFactory.createURI(SIOC.account_of), foafPerson, ctx);
	            } else {
	            	siocUserAccount = valueFactory.createBNode();
	            }
	            
	            conn.add(siocUserAccount, RDF.TYPE, valueFactory.createURI(SIOC.UserAccount), ctx);
	            conn.add(siocUserAccount, valueFactory.createURI(FOAF.accountServiceHomepage), valueFactory.createURI(account.getService()), ctx);
	            conn.add(siocUserAccount, valueFactory.createURI(DCTERMS.identifier), valueFactory.createLiteral(account.getId()), ctx);
	            conn.add(siocUserAccount, valueFactory.createURI(FOAF.nick), valueFactory.createLiteral(account.getUsername()), ctx);
	            if (StringUtils.isNotBlank(account.getEmail())) {
	            	conn.add(siocUserAccount, valueFactory.createURI(FOAF.mbox), valueFactory.createURI("mailto:"+account.getEmail()), ctx);
	            }
	            conditionalAdd(conn, valueFactory, siocUserAccount, SIOC.name, account.getName(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, FOAF.firstName, account.getFirstname(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, FOAF.familyName, account.getFamilyname(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, FOAF.gender, account.getGender(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, FOAF.birthday, account.getBirthday(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, VCARD+"adr", account.getAddress(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, FOAF.based_near, account.getLocation(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, DV+"affiliation", account.getAffiliation(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, FOAF.page, account.getHomepage(), ctx);
	            conditionalAdd(conn, valueFactory, siocUserAccount, FOAF.depiction, account.getPicture(), ctx);

                // TODO: this should move probably to the UserAccountFacebook or similar
                if(account instanceof UserAccountFacebook) {
                	
            		String ldpath = "@prefix dct : <http://purl.org/dc/terms/identifier>; \n" +
    								"@prefix sch : <http://schema.org/>; \n" +
    								"name = sch:name :: xsd:string; \n" +
    								"id = dct:identifier :: xsd:string; \n";
                	
                    URI foafInterest = valueFactory.createURI(FOAF.interest);

                    for(String like : ((UserAccountFacebook) account).getLikes()) {
                        conditionalAdd(conn, valueFactory, siocUserAccount, FOAF.interest, like, ctx);
                        URI likeUri = valueFactory.createURI(like);
                        URI documentUri = valueFactory.createURI(FOAF.Document);

                        conn.add(siocUserAccount,foafInterest,likeUri,ctx);
                        conn.add(likeUri, RDF.TYPE, documentUri, ctx);
                        
	            		forceCaching(ldpath, likeUri);
                    }

                    for(String movie : ((UserAccountFacebook) account).getMovies()) {
                        URI fbMovieUri = valueFactory.createURI(movie);

                        conn.add(siocUserAccount,foafInterest,fbMovieUri,ctx);
                        conn.add(fbMovieUri,RDF.TYPE, SCHEMA.Movie,ctx);
                        
                        forceCaching(ldpath, fbMovieUri);
                    }

                    for(String book : ((UserAccountFacebook) account).getBooks()) {
                        URI fbBookUri = valueFactory.createURI(book);

                        conn.add(siocUserAccount,foafInterest,fbBookUri,ctx);
                        conn.add(fbBookUri,RDF.TYPE, SCHEMA.Book,ctx);
                        
                        forceCaching(ldpath, fbBookUri);
                    }

                    for(String music : ((UserAccountFacebook) account).getMusic()) {
                        URI fbGroupUri = valueFactory.createURI(music);

                        conn.add(siocUserAccount,foafInterest,fbGroupUri,ctx);
                        conn.add(fbGroupUri,RDF.TYPE, SCHEMA.MusicGroup,ctx);
                        
                        forceCaching(ldpath, fbGroupUri);
                    }

                    for(String interest : ((UserAccountFacebook) account).getInterests()) {
                        URI fbTopicUri = valueFactory.createURI(interest);

                        conn.add(siocUserAccount,foafInterest,fbTopicUri,ctx);
                        conn.add(fbTopicUri,RDF.TYPE, SKOS.CONCEPT,ctx);
                    }

                    for(String sport : ((UserAccountFacebook) account).getSports()) {
                        URI fbSportUri = valueFactory.createURI(sport);

                        conn.add(siocUserAccount,foafInterest,fbSportUri,ctx);
                        conn.add(fbSportUri,RDF.TYPE, SKOS.CONCEPT,ctx);
                        
                        forceCaching(ldpath, fbSportUri);
                    }

                    for(String friend : ((UserAccountFacebook) account).getFriends()) {
                        URI fbFriendUri = valueFactory.createURI(friend);
                        URI foafPerson  = valueFactory.createURI(FOAF.Person);
                        URI foafKnows   = valueFactory.createURI(FOAF.knows);

                        conn.add(siocUserAccount,foafKnows,fbFriendUri,ctx);
                        conn.add(fbFriendUri,RDF.TYPE, foafPerson,ctx);
                        
                        forceCaching(ldpath, fbFriendUri);
                    }

                }

	            return account;
	        } finally {
	            conn.commit();
	            conn.close();
	        }
		} catch (RepositoryException e) {
			throw new RuntimeException("Error creating user account: " + e.getMessage(), e);
		}
	}

	private void forceCaching(String program, URI context) {
		CacheHelper.update(ldpathService, context, program);
	}
	
	private void conditionalAdd(RepositoryConnection conn, ValueFactory valueFactory, Resource s, String p, String o, Resource ctx) throws RepositoryException {
		if (StringUtils.isNotBlank(o)) {
			Value value = (UriUtil.validate(o) ? valueFactory.createURI(o): valueFactory.createLiteral(o)); 
			conn.add(s, valueFactory.createURI(p), value, ctx);
		}
	}

	@Override
	public boolean exists(String person) {
		String query = "ASK { <" + person + ">  <" + RDF.TYPE.stringValue() + "> <" + FOAF.Person + "> } ";
		try {
			return sparqlService.ask(QueryLanguage.SPARQL, query);
		} catch (MarmottaException e) {
			log.error("Error checking existance of a person: {}", e.getMessage());
			return false;
		}	
	}
	
	@Override
	public List<UserAccount> getUserAccounts(String person) {
		//this way sucks... switch to something different, ldpath, oo mapping or whatever
		String query =  "SELECT * WHERE { \n" +
						"  <" + person + ">  <" + RDF.TYPE.stringValue() + "> <" + FOAF.Person + "> ;\n" +
						"    <" + FOAF.account + "> ?account . \n" +
						"  ?account <" + DCTERMS.identifier + "> ?id ; \n" +
						"    <" + SIOC.name + "> ?name ; \n" +
						"    <" + FOAF.accountServiceHomepage + "> ?service . \n" +
						"  OPTIONAL { ?account <" + FOAF.mbox + "> ?email . } \n" +
						"  OPTIONAL { ?account <" + FOAF.nick + "> ?nick . } \n" +						
						"  OPTIONAL { ?account <" + FOAF.firstName + "> ?firstName . } \n" +
						"  OPTIONAL { ?account <" + FOAF.familyName + "> ?familyName . } \n" +
						"  OPTIONAL { ?account <" + FOAF.gender + "> ?gender . } \n" +
						"  OPTIONAL { ?account <" + VCARD + "adr> ?address . } \n" +
						"  OPTIONAL { ?account <" + FOAF.based_near + "> ?location . } \n" +
						"  OPTIONAL { ?account <" + DV + "affiliation> ?affiliationl . } \n" +
						"  OPTIONAL { ?account <" + FOAF.page + "> ?homepage . } \n" +
						"  OPTIONAL { ?account <" + FOAF.depiction + "> ?picture . } \n" +
					    "} ";
		List<UserAccount> accounts = new ArrayList<UserAccount>();
		try {
			List<Map<String, Value>> results = sparqlService.query(QueryLanguage.SPARQL, query);
			for(Map<String, Value> result : results) {
				UserAccount account = new UserAccount();
				account.setUri(result.get("account").stringValue());
				account.setId(result.get("id").stringValue());
				account.setName(result.get("name").stringValue());
				account.setService(result.get("service").stringValue());
				if (result.containsKey("email")) { account.setEmail(result.get("email").stringValue().substring(7)); }
				if (result.containsKey("nick")) { account.setUsername(result.get("nick").stringValue()); }
				if (result.containsKey("firstName")) { account.setFirstname(result.get("firstName").stringValue()); }
				if (result.containsKey("familyName")) { account.setFamilyname(result.get("familyName").stringValue()); }
				if (result.containsKey("gender")) { account.setGender(result.get("gender").stringValue()); }
				if (result.containsKey("address")) { account.setAddress(result.get("address").stringValue()); }
				if (result.containsKey("location")) { account.setLocation(result.get("location").stringValue()); }
				if (result.containsKey("affiliationl")) { account.setAffiliation(result.get("affiliationl").stringValue()); }
				if (result.containsKey("homepage")) { account.setHomepage(result.get("homepage").stringValue()); }
				if (result.containsKey("picture")) { account.setPicture(result.get("picture").stringValue()); }
				accounts.add(account);
			}
		} catch (MarmottaException e) {
			log.error("Error checking existance of a person: {}", e.getMessage());
		}
		return accounts;
	}

	@Override
	public Map<String, Double> getPreferences(String person) {
		Map<String, Double> preferences = new HashMap<String, Double>();
		String query = 	"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n" +
			            "PREFIX foaf: <http://xmlns.com/foaf/0.1/> \n" +
			            "PREFIX sioc: <http://rdfs.org/sioc/ns#> \n" +
			            "PREFIX mao: <http://www.w3.org/ns/ma-ont#> \n" +
			            "SELECT ?genre (count(?video) as ?count) \n" +
			            "WHERE { \n" +
			            "  <" + person + "> rdf:type foaf:Person ; \n" +
			            "    foaf:account ?account . \n" +
			            "  ?account rdf:type sioc:UserAccount ; \n" +
			            "    ?x ?video . \n" +
			            "  ?video rdf:type mao:VideoTrack ; \n" +
			            "    mao:hasGenre ?genre . \n" +
			            "} \n" +
			            "GROUP BY ?genre \n";
		try {
			List<Map<String, Value>> results = sparqlService.query(QueryLanguage.SPARQL, query);
			int total = 0;
			for(Map<String, Value> result : results) {
				total += Integer.parseInt(result.get("count").stringValue()); //FIXME 2 x ON
			}
			for(Map<String, Value> result : results) {
				String genre = result.get("genre").stringValue().substring(38);  //http://gdata.youtube.com/schemas/2007#News
				int count = Integer.parseInt(result.get("count").stringValue());
				double rank = ((double) count) / total; //TODO: naive
				preferences.put(genre, rank);
				
			}
		} catch (MarmottaException e) {
			log.error(e.getMessage());
		}
		return preferences;
	}

}
