/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.social.model.facebook;

import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import at.newmedialab.lmf.social.model.UserAccount;

import com.restfb.types.User;

/**
 * User account at Facebook
 *
 */
public class UserAccountFacebook extends UserAccount {

	private static final String SERVICE = "http://www.facebook.com";

    private Locale locale;

    private List<String> movies, books, likes, sports, interests, music, friends;

	public UserAccountFacebook(User profile) {
        super(profile.getId(),profile.getUsername(), profile.getName(), profile.getFirstName(), profile.getLastName(),
              profile.getGender(), profile.getBirthday(), profile.getEmail(), profile.getHometownName(), profile.getLocation().getName(),
              profile.getWork().size() > 0 ? profile.getWork().get(0).getEmployer().getName() : null,
                profile.getWebsite(), null);


        if(profile.getLocale() != null) {
            locale = new Locale(profile.getLocale());
        }
        
        if(StringUtils.isBlank(getHomepage())) {
        	setHomepage("http://www.facebook.com/profile.php?id=" + getId());
        }
        
        if(StringUtils.isBlank(getPicture())) {
        	setPicture("http://graph.facebook.com/" + getId() + "/picture");
        }

        setService(SERVICE);
	}

    public Locale getLocale() {
        return locale;
    }


    public List<String> getBooks() {
        return books;
    }

    public void setBooks(List<String> books) {
        this.books = books;
    }

    public List<String> getLikes() {
        return likes;
    }

    public void setLikes(List<String> likes) {
        this.likes = likes;
    }

    public List<String> getMovies() {
        return movies;
    }

    public void setMovies(List<String> movies) {
        this.movies = movies;
    }

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }

    public List<String> getSports() {
        return sports;
    }

    public void setSports(List<String> sports) {
        this.sports = sports;
    }

    public List<String> getMusic() {
        return music;
    }

    public void setMusic(List<String> music) {
        this.music = music;
    }

    public List<String> getFriends() {
        return friends;
    }

    public void setFriends(List<String> friends) {
        this.friends = friends;
    }
}
