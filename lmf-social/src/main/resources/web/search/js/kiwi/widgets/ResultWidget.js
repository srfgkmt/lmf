/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function ($) {

AjaxSolr.ResultWidget = AjaxSolr.AbstractWidget.extend({

  beforeRequest: function () {
    $('#loader').css("display","block");
    //goto top
    window.scrollTo(0,0);
  },

  facetLinks: function (facet_field, facet_values) {
    var links = [];
    if (facet_values) {
      for (var i = 0, l = facet_values.length; i < l; i++) {
        links.push(AjaxSolr.theme('facet_link', facet_values[i], this.facetHandler(facet_field, facet_values[i])));
      }
    }
    return links;
  },

  moreLikeThisHandler: function(doc) {
	  var self = this;
	  return function() {
		  self.manager.store.remove('q');
		  self.manager.store.addByValue('q', 'uri:"'+doc.uri+'"');
		  self.manager.doRequest(0, 'mlt');
		  return false;
	  };
  },
  facetHandler: function (facet_field, facet_value) {
    var self = this;
    return function () {
      self.manager.store.remove('fq');
      self.manager.store.addByValue('fq', facet_field + ':' + facet_value);
      self.manager.doRequest(0);
      return false;
    };
  },
  afterRequest: function () {
    $('#loader').css("display","none");
    $(this.target).empty();

    var match = this.manager.response.match;
    var maxScore = 0;
    if ( match != undefined ) {
    	maxScore = match.maxScore;
    	var resp = this.manager.response;
    	var doc = this.manager.response.match.docs[0];
        var result = AjaxSolr.theme('result', doc, AjaxSolr.theme('snippet', doc, resp), resp);
        
        $(this.target).append(Encoder.htmlDecode(result));
        //$(this.target).append(AjaxSolr.theme('moreLikeThis', doc, this.moreLikeThisHandler(doc) ) );
    	$(this.target).append('<hr/>');
    }    
    for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
      var resp = this.manager.response;
      var doc = this.manager.response.response.docs[i];

      var result = AjaxSolr.theme('result', doc, AjaxSolr.theme('snippet', doc, resp), resp);
      
      $(this.target).append(Encoder.htmlDecode(result));
      $(this.target).append(AjaxSolr.theme('moreLikeThis', doc, this.moreLikeThisHandler(doc) ) );

      //var items = [];
      //items = items.concat(this.facetLinks('id', doc.id));
      //to show tag links
      //AjaxSolr.theme('list_items', '#links_' + doc.id, items);
    }
    if(this.manager.response.response.numFound == 0) {
        var x = this.manager.response.responseHeader.params.q;
        $(this.target).html("No results for query");
    }
  },

  init: function () {
   /*
    $('a.more').livequery(function () {
      $(this).toggle(function () {
        $(this).parent().find('span').show();
        $(this).text('less');
        return false;
      }, function () {
        $(this).parent().find('span').hide();
        $(this).text('more');
        return false;
      });
    });
    */
  }

});

})(jQuery);
