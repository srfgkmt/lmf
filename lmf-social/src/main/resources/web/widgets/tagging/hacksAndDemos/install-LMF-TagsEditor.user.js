/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// ==UserScript==
// @name           LMF Tags Editors
// @include        http://www.readwriteweb.com/*
// @include        http://www.oracle.com/*
// @include        http://local/*

// @description    Injects "LML" TagEditor into some pages
// ==/UserScript==
//

////////////////////////////////////////////////////////////////////////////

/**
 * Just start loading the "TagsEditor" injection script.
 *
 */
(function(){
	var el=document.createElement('script');
	el.type='text/javascript';
	el.src= 'http://localhost:8080/KiWi2/social/widgets/tagging/hacksAndDemos/install-LMF-TagsEditor.js';
	document.body.appendChild(el);
})();

