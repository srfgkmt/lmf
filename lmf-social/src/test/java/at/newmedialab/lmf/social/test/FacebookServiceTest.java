package at.newmedialab.lmf.social.test;

import at.newmedialab.lmf.social.api.facebook.FacebookService;
import at.newmedialab.lmf.social.model.facebook.UserAccountFacebook;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.test.base.EmbeddedMarmotta;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Add file description here!
 * <p/>
 * Author: Sebastian Schaffert (sschaffert@apache.org)
 */
public class FacebookServiceTest {

    private final static Logger log = LoggerFactory.getLogger(FacebookServiceTest.class);

    private static EmbeddedMarmotta lmf;
    private static ConfigurationService configurationService;
    private static FacebookService facebookService;

    private static String accessToken = "AAACEdEose0cBAPtiAoOZCx8DxnPhwMhOCTOEk1jFjCwtulA8pfhqvlD8S20jWvXK9zitnVUROwj2ko8t2v4tPu9cmYQuSPOMTbkhUZBgZDZD";

    @BeforeClass
    public static void setUp() {
        lmf = new EmbeddedMarmotta();
        configurationService = lmf.getService(ConfigurationService.class);
        facebookService      = lmf.getService(FacebookService.class);
    }

    @Test
    @Ignore // set access token and enable
    public void testGetUserProfile() {
        UserAccountFacebook profile = facebookService.getUserProfile(accessToken);

        Assert.assertNotNull(profile.getName());
        Assert.assertNotNull(profile.getFirstname());
        Assert.assertNotNull(profile.getFamilyname());

        log.info("accessed profile of user {}", profile.getName());
    }

}
