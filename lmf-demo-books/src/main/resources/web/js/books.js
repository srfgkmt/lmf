/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var Manager;

(function ($) {
    var ROOT = location.protocol + "//" + location.host + location.pathname.replace(/[^\/]+\/[^\/]+$/, "");
    
  $(function () {
    Manager = new AjaxSolr.Manager({
      solrUrl: ROOT + 'solr/books/'
    });
    Manager.addWidget(new AjaxSolr.ResultWidget({
      id: 'result',
      target: '#docs'      
    }));

    Manager.addWidget(new AjaxSolr.DropDownParamWidget({
      id: 'sort',
      param: 'sort',
      target: 'select.sort',
      values: {"relevance":'score desc', "lmf.date descending": 'created desc', "lmf.date ascending": 'created asc'}
    }));

    Manager.addWidget(new AjaxSolr.DropDownParamWidget({
      id: 'rows',
      param: 'rows',
      target: '#pageSize',
      values: [10,15,25],
	    default: 15
    }));

    Manager.addWidget(new AjaxSolr.PagerWidget({
      id: 'pager',
      target: '#pager',
      prevLabel: '&laquo;',
      nextLabel: '&raquo;',
      innerWindow: 1,
      renderHeader: function (perPage, offset, total) {
        $('#pager-header').html($('<span/>').text('Shown results: ' + Math.min(total, offset + 1) + ' to ' + Math.min(total, offset + perPage) + ' of ' + total));
      }
    }));
    var fields = [ 'thumb', 'genre', 'writer' ];
    for (var i = 0, l = fields.length; i < l; i++) {
      Manager.addWidget(new AjaxSolr.TagcloudWidget({
        id: fields[i],
        target: '#' + fields[i],
        field: fields[i]
      }));
    }
    Manager.addWidget(new AjaxSolr.ColorWidget({
        id: 'color',
        target: '#color',
        field: 'thumb_color'
    }));
    fields.push('thumb_color');

      Manager.addWidget(new AjaxSolr.TextWidget({
                 id: 'search',
                 target: '#search',
                 input: '#searchfield',
                 submit: '#searchbutton',
                 field: 'q'
              }));
 Manager.addWidget(new AjaxSolr.CurrentSearchWidget({
                id: 'currentsearch',
                target: '#selection',
                mappers: {
                    "genre":"(Genre) " , 
                    "title":"(Title) " , 
                    "writer":"(Writer) ", 
                    'thumb_color': function(value) {
                        var a = $("<a>", {href:"#"})
                        a.append($("<span style='width: 20px; height: 20px; display: inline-block; background-color: " + AjaxSolr.ColorWidget.solr2color(value) + "'>&nbsp;</span>"));
                        a.append('&nbsp;');
                        a.append($("<span>", {text: "(Cover)"}));
                        return a.html();
                    } 
                },
                image: {"remove":"images/remove.png"}
            }));

 /*
    Manager.addWidget(new AjaxSolr.AutocompleteWidget({
      id: 'text',
      target: '#search',
      field: 'text_all',
      mappers: {"genre":"(Genre)" , "title":"(Title)" , "writer":"(Writer)"},
      fields: [ 'title' , 'writer', 'genre', 'created', 'text_all']
    }));
*/
    
     
    Manager.init();
    Manager.store.addByValue('q', '*:*');
    var params = {
      facet: true,
      'facet.field': fields,
      'facet.sort':'count',
      'facet.limit': 10,
      'facet.mincount': 1,
      'json.nl': 'map',
      'hl':true,
      'hl.fl':"description"
    };
    for (var name in params) {
      Manager.store.addByValue(name, params[name]);
    }
    
    function getUrlVars () {
        var vars = [], hash;
        var hashes = window.location.search.substr(1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
    
    function getUrlVar(name) {
        return getUrlVars()[name];
    }

    var paramMapping = {
            writer: "writer",
            genre: "genre",
            cover: "thumb_color"
    }
    for (var p in paramMapping) {
        var f = paramMapping[p],
        val = getUrlVar(p);
        if (val != null && val != "") {
            Manager.store.addByValue('fq', f + ':"' + decodeURIComponent(val) + '"');
        }
    }
    var query = getUrlVar("q");
    if (query != null && query != "") {
        Manager.widgets['search'].setQuery(decodeURIComponent(query));
    }
    
    Manager.doRequest();
  });

  $.fn.showIf = function (condition) {
    if (condition) {
      return this.show();
    }
    else {
      return this.hide();
    }
  }

})(jQuery);
