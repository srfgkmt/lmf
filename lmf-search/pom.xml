<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2013 Salzburg Research.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing, software
  ~  distributed under the License is distributed on an "AS IS" BASIS,
  ~  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~  See the License for the specific language governing permissions and
  ~  limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>at.newmedialab.lmf</groupId>
        <artifactId>lmf-parent</artifactId>
        <version>3.1.0</version>
    </parent>

    <artifactId>lmf-search</artifactId>
    <packaging>jar</packaging>

    <name>LMF Module: Search</name>
    <description>LMF Module offering semantic search functionality using Apache SOLR</description>

    <dependencies>

        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-ldpath</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-ldcache</artifactId>
        </dependency>
        <dependency>
            <groupId>at.newmedialab.lmf</groupId>
            <artifactId>lmf-worker</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>at.newmedialab.lmf.util</groupId>
            <artifactId>solr</artifactId>
            <version>${lmf.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.solr</groupId>
            <artifactId>solr-solrj</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.solr</groupId>
            <artifactId>solr-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.solr</groupId>
            <artifactId>solr-clustering</artifactId>
            <scope>runtime</scope>
        </dependency>
        
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.marmotta</groupId>
            <artifactId>marmotta-core</artifactId>
            <classifier>tests</classifier>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-server</artifactId>
            <version>${jetty.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-servlet</artifactId>
            <version>${jetty.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>test</scope>
        </dependency>
        
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.3</version>

                <executions>
                    <execution>
                        <id>solrHomeZip</id>

                        <configuration>
                            <finalName>lmf-solr-data</finalName>
                            <outputDirectory>${project.build.outputDirectory}</outputDirectory>
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptors>
                                <descriptor>conf/solrhome-assembly.xml</descriptor>
                            </descriptors>
                        </configuration>

                        <phase>process-resources</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>solrCoreZip</id>

                        <configuration>
                            <finalName>lmf-solr-core</finalName>
                            <outputDirectory>${project.build.outputDirectory}</outputDirectory>
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptors>
                                <descriptor>conf/solrcore-assembly.xml</descriptor>
                            </descriptors>
                        </configuration>

                        <phase>process-resources</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
