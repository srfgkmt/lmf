<!--

    Copyright (C) 2013 Salzburg Research.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<html>
<head>
    <!--###BEGIN_HEAD###-->
    <title>Classification Services</title>
    <link rel="stylesheet" type="text/css" href="../../core/public/style/center.css"/>
    <!--###END_HEAD###-->
    <link rel="stylesheet" type="text/css" href="../../../../../../lmf-core/src/main/resources/web/public/style/center.css"/>
</head>
<body>
<div id="center" style="width:921px;">
<!--###BEGIN_CONTENT###-->
<h1><a name="Introduction"></a>Introduction<a class="section_anchor" href="#Introduction"></a></h1>

<p>The Linked Media Framework offers generic statistical text classification services (based on maximum entropy 
   classification) that allow classifying text content to URI resources according to the underlying classification 
   model. This functionality can e.g. be used to automatically categorize texts to SKOS thesaurus concepts or for
   sentiment analysis, categorizing texts as "positive", "negative" or "neutral". 
</p>
<p>
    In general, the maximum entropy classification works by comparing an input text with a trained model and assigning
    the text to the categories for which the training data is most similar to the input text (i.e. preserving the maximum 
    entropy between the categories). Each assignment is given a probability value between 0.0 and 1.0, indicating to which
    extent the text matches to the category.
</p>
<p>
    LMF classification is available as web services under the <a href="../doc/rest/classifier/index.html"><code>/classifier</code></a> endpoint.
</p>

<h1><a name="Creating_Classifiers"></a>Creating/Removing Classifiers<a class="section_anchor" href="#Creating_Classifiers"></a></h1>

<p>
    The LMF classification services allow to define an arbitrary number of classifiers that can be trained and used
    individually. This allows users to classify the same content according to different dimensions, e.g. topic, sentiment and
    kind.
</p>
<h2>Creating Classifiers</h2>
<p>
    A new classifier can be created by issuing an HTTP POST request to the <a href="../doc/rest/classifier/{name}/index.html"><code>/classifier/{name}</code></a> endpoint,
    where <code>{name}</code> is the name of the classifier to be created. After the POST request, a new 
    <strong>untrained</strong> classifier will be available in the system. Before the classifier can be used for
    classifying texts, in needs to be trained as described below.
</p>
<h2>Listing Classifiers</h2>
<p>
    All classifiers can be listed by issuing a HTTP GET request to the <a href="../doc/rest/classifier/list/index.html"><code>/classifier/list</code></a> endpoint. The result
    will be a JSON list of classifier names currently registered in the system.
</p>
<h2>Getting Classifier Information</h2>
<p>
    When issuing a HTTP GET request to an individual classifier (i.e. to <a href="../doc/rest/classifier/{name}/index.html"><code>/classifier/{name}</code></a>), a JSON
    description of the classifier, including the name and a list of the managed concepts, will be returned.
</p>
<h2>Removing Classifiers</h2>
<p>
    A classifier can be removed by issuing an HTTP DELETE request to the classifier endpoint. When the optional
    query parameter <code>removeData=true</code> is given, all training and model data created by the classifier will
    also be removed.
</p>

<h1><a name="Training_Classifiers"></a>Training Classifiers<a class="section_anchor" href="#Training_Classifiers"></a></h1>

<p>
    Before text classification can be used, classifiers need to be trained with sample data for each category managed
    by the classifier. Categories need to be URI resources already managed by the LMF system, e.g. a SKOS thesaurus that
    has previously been imported into the system. In simple cases, it is sufficient to just create resources using
    the HTTP POST request to the resource web service.
</p>
<p>
    Training mainly involves uploading a number of text samples for each category. The classification
    service will then automatically take care of creating the model. Training can be carried out incrementally, i.e. 
    when the quality of classification is in sufficient, additional training data can be added and the classifier will
    automatically be retrained.
</p>
    
<p>
   A new training sample is uploaded for a category by a HTTP POST to the <a href="../doc/rest/classifier/{name}/train/index.html"><code>/classifier/{name}/train?resource={category uri}</code></a>
   endpoint with content type "text/plain" and the sample text as the request body. {name} is the name of the classifier
    (as created above) and {category uri} is the URI of the category for which to add training data. For example,
    the following HTTP request would upload training data in the "sample" classifier for the concept <code>http://localhost:8080/LMF/resource/Concept1</code>
    (note that the query parameter should be URL-encoded):
</p>
<pre>
POST http://localhost:8080/LMF/classifier/sample/train?resource=http%3A%2F%2Flocalhost%3A8080%2FLMF%2Fresource%2FConcept1
Content-Type: text/plain

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor purus, ...
</pre>
    
<p>
    A typical figure is to have at least 10 text samples for each category. The more sample texts are uploaded 
    for each category and the higher the representativeness of the text for the category, the better will be the 
    classification results when the classifier is used.
</p>

<p>
    The classifiers are automatically retrained after a certain threshold is reached or a timeout expired. It is also
    possible to trigger immediate retraining manually by sending a POST request to the <a href="../doc/rest/classifier/{name}/retrain/index.html"><code>/classifier/{name}/retrain</code></a> endpoint.
</p>

<h1><a name="Classifying"></a>Classifying Texts<a class="section_anchor" href="#Classifying"></a></h1>

<p>
    When a classifier is sufficiently trained, it can be used to classify text into categories. Text classification
    is straightforward: a text is sent to the classifier endpoint, and the endpoint returns a list of category
    URIs together with the probability that the text belongs to this category. The result list will always be
    ordered in descending order by probability.
</p>
<p>
    To classify a text using the classifier identified by "{name}", it is sent as plain text to the 
    <a href="../doc/rest/classifier/{name}/classify/index.html"><code>/classifier/{name}/classify</code></a> endpoint
    using a HTTP POST request. The endpoint also accepts an optional <code>threshold</code> parameter that can be used
    to provide a minimum probability for the category URIs to return. For example, the following request would try to
    classify a sample text:
</p>
<pre>
POST http://localhost:8080/LMF/classifier/sample/classify
Content-Type: text/plain

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor purus, ...
</pre>
<p>
    The result will be a JSON list of classifications that could look as follows:
</p>
<pre>
[
    {
        concept:  "http://localhost:8080/LMF/resource/Concept1",
        probability: 0.7123
    },
    ...
]
</pre>


<!--###END_CONTENT###-->
</div>
</body>
</html>
