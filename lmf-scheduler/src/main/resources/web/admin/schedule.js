/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* QUARTZ CONFIGURATION */
$(function() {
    var root = _SERVER_URL;
    var service = "quartz/"

    var auto_refresh_interval = ($("input#refresh_interval").val() || 5) * 1000;
    
    var pollerU;
    var refresh = function() {
        clearTimeout(pollerU);
        var tBody = $(document.getElementById("triggers"));
        $.getJSON(root + service + "triggers", function(data, status, xhr) {
            $("tr", tBody).addClass("toRemove");
            $.each(data, function(group, triggers) {
                var gRow = $(document.getElementById("triggerGroup_" + group));
                if (gRow.size() == 0) {
                    gRow = $("<tr />").attr("id", "triggerGroup_" + group);
                    gRow.append($("<th colspan='6' class='group'/>").text("Group: " + group));
                    
                    gRow.appendTo(tBody);
                }
                gRow.removeClass("toRemove");
                
                for(var i in triggers) {
                    var t = triggers[i];

                    var row = $(document.getElementById("trigger_" + t.triggerKey));
                    if (row.size() == 0) {
                        row = $("<tr />");
                        row.attr("id", "trigger_" + t.triggerKey);
                        row.append($("<td/>"));
                        row.append($("<td class='key' />").text(t.key));
                        row.append($("<td class='description' />").text(t.description || ""));
                        row.append($("<td class='job' />").append($("<a>").attr('href', '#job_' + t.jobKey).text(t.jobKey)));
                        row.append($("<td class='fire' />").text(new Date(t.fireTime).strftime("%F %T")));
                        row.append($("<td class='action' />"));
                        
                        row.insertAfter(gRow);
                    } else {
                        $("td.key", row).text(t.key);
                        $("td.description", row).text(t.description || "");
                        $("td.job a", row).attr('href', '#job_' + t.jobKey).text(t.jobKey);
                        $("td.fire", row).text(new Date(t.fireTime).strftime("%F %T"));
                    }
                    
                    row.removeClass("toRemove");
                }
            });
            
            $("tr.toRemove", tBody).slideUp("slow", function() { $(this).remove(); });
            $('#messages div.trigger').slideUp('slow', function() { $(this).remove(); });
        }).error(function() {
            var t = $('#messages div.trigger'); 
            if (t.size() == 0) {
                $('#messages').append($("<div>", {'class':'trigger error'}).text("Trigger update failed"));
            } else {
                t.slideDown();
            }
        });
        
        var jBody = $(document.getElementById("jobs"));
        $.getJSON(root + service + "jobs", function(data, status, xhr) {
            $("tr", jBody).addClass("toRemove");
            $.each(data, function(group, jobs) {
                var gRow = $(document.getElementById("jobGroup_" + group));
                if (gRow.size() == 0) {
                    gRow = $("<tr />").attr("id", "jobGroup_" + group);
                    gRow.append($("<th colspan='6' class='group'/>").text("Group: " + group));
                    
                    gRow.appendTo(jBody);
                }
                gRow.removeClass("toRemove");
                
                for(var i in jobs) {
                    var j = jobs[i];

                    var row = $(document.getElementById("job_" + j.jobKey));
                    if (row.size() == 0) {
                        row = $("<tr />");
                        row.attr("id", "job_" + j.jobKey);
                        row.append($("<td/>"));
                        row.append($("<td class='key' />").text(j.key));
                        row.append($("<td class='description' />").text(j.description || "+"));
                        row.append($("<td class='clazz' />").text(j.jobClass));
                        var tg = $("<td class='triggers' />").appendTo(row);
                        for (var t in j.triggerKeys) {
                            var d = $("<div/>").append(
                                $("<a>").attr('href', '#trigger_' + j.triggerKeys[t]).text(j.triggerKeys[t])
                            );
                            tg.append(d);
                        }
                        row.append($("<td class='action' />"));
                        
                        row.insertAfter(gRow);
                    } else {
                        $("td.key", row).text(j.key);
                        $("td.description", row).text(j.description || "");
                        $("td.clazz", row).text(j.jobClass);
                        var tg = $("td.triggers", row).empty();
                        for (var t in j.triggerKeys) {
                            var d = $("<div/>").append(
                                $("<a>").attr('href', '#trigger_' + j.triggerKeys[t]).text(j.triggerKeys[t])
                            );
                            tg.append(d);
                        }
                    }
                    row.removeClass("toRemove");
                }
            });
            
            $("tr.toRemove", jBody).slideUp("slow", function() { $(this).remove(); });
            
            $('#messages div.job').slideUp('slow', function() { $(this).remove(); });
        }).error(function() {
            var t = $('#messages div.job'); 
            if (t.size() == 0) {
                $('#messages').append($("<div>", {'class':'job error'}).text("Jobs update failed"));
            } else {
                t.slideDown();
            }
        });
    };
    
    $("button#refresh_now").click(refresh);
    $("input#refresh_interval").change(function(event) {
        if ($(this).val() * 1000 > 0) {
            var oVal = auto_refresh_interval;
            auto_refresh_interval = $(this).val() * 1000;
            if (oVal > auto_refresh_interval && $("input#refresh_auto").prop('checked')) {
                refresh();          
            }   
        } else {
            $(this).val(auto_refresh_interval / 1000);
        }
    });
    $("input#refresh_auto").change(function(event) {
        if (this.checked) {
            $("button#refresh_now").attr('disabled','disabled')
            pollerU = setTimeout(refresh, auto_refresh_interval);
        } else {
            $("button#refresh_now").removeAttr('disabled');
            clearTimeout(pollerU);
        }
    });
    refresh();

});
