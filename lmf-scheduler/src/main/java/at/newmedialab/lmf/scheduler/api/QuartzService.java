/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.scheduler.api;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.URI;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.SchedulerMetaData;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;
import org.quartz.UnableToInterruptJobException;
import org.quartz.impl.matchers.GroupMatcher;

public interface QuartzService {

    public boolean isStarted() throws SchedulerException;

    public SchedulerMetaData getMetaData() throws SchedulerException;

    public List<JobExecutionContext> getCurrentlyExecutingJobs() throws SchedulerException;

    public Date scheduleJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException;

    public Date scheduleJob(Trigger trigger) throws SchedulerException;

    public void scheduleJobs(Map<JobDetail, List<Trigger>> triggersAndJobs, boolean replace) throws SchedulerException;

    public boolean unscheduleJob(TriggerKey triggerKey) throws SchedulerException;

    public boolean unscheduleJobs(List<TriggerKey> triggerKeys) throws SchedulerException;

    public Date rescheduleJob(TriggerKey triggerKey, Trigger newTrigger) throws SchedulerException;

    public void addJob(JobDetail jobDetail, boolean replace) throws SchedulerException;

    public boolean deleteJob(JobKey jobKey) throws SchedulerException;

    public boolean deleteJobs(List<JobKey> jobKeys) throws SchedulerException;

    public void triggerJob(JobKey jobKey) throws SchedulerException;

    public void triggerJob(JobKey jobKey, JobDataMap data) throws SchedulerException;

    public void pauseJob(JobKey jobKey) throws SchedulerException;

    public void pauseJobs(GroupMatcher<JobKey> matcher) throws SchedulerException;

    public void pauseTrigger(TriggerKey triggerKey) throws SchedulerException;

    public void pauseTriggers(GroupMatcher<TriggerKey> matcher) throws SchedulerException;

    public void resumeJob(JobKey jobKey) throws SchedulerException;

    public void resumeJobs(GroupMatcher<JobKey> matcher) throws SchedulerException;

    public void resumeTrigger(TriggerKey triggerKey) throws SchedulerException;

    public void resumeTriggers(GroupMatcher<TriggerKey> matcher) throws SchedulerException;

    public void pauseAll() throws SchedulerException;

    public void resumeAll() throws SchedulerException;

    public List<String> getJobGroupNames() throws SchedulerException;

    public Set<JobKey> getJobKeys(GroupMatcher<JobKey> matcher) throws SchedulerException;

    public List<? extends Trigger> getTriggersOfJob(JobKey jobKey) throws SchedulerException;

    public List<String> getTriggerGroupNames() throws SchedulerException;

    public Set<TriggerKey> getTriggerKeys(GroupMatcher<TriggerKey> matcher) throws SchedulerException;

    public Set<String> getPausedTriggerGroups() throws SchedulerException;

    public JobDetail getJobDetail(JobKey jobKey) throws SchedulerException;

    public Trigger getTrigger(TriggerKey triggerKey) throws SchedulerException;

    public TriggerState getTriggerState(TriggerKey triggerKey) throws SchedulerException;

    public boolean interrupt(JobKey jobKey) throws UnableToInterruptJobException;

    public boolean interrupt(String fireInstanceId) throws UnableToInterruptJobException;

    public boolean checkExists(JobKey jobKey) throws SchedulerException;

    public boolean checkExists(TriggerKey triggerKey) throws SchedulerException;

    /**
     * Parse the Interval unit from the string.
     * <b>Format</b>: <code>"x UNIT"</code> where unit is on of SECOND, MINUTE, HOUR, DAY, WEEK,
     * MONTH, YEAR. case insensitivem with an optional trailing 'S'.
     * 
     * @param string <code>"x UNIT"</code>
     * @return <code>UNIT</code>
     * @see IntervalUnit
     */
    public IntervalUnit parseUnitFromIntervalString(String string);

    /**
     * Parse the interval from the string
     * <b>Format</b>: <code>"x UNIT"</code>
     * 
     * @param string <code>"x UNIT"</code>
     * @return <code>x</code>
     */
    public int parseTimeFromIntervalString(String string);

    public URI getQuartzUser();
}
