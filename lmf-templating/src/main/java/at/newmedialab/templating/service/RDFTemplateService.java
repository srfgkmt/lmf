/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.templating.service;

import at.newmedialab.sesame.commons.model.Namespaces;
import at.newmedialab.sesame.facading.api.Facading;
import at.newmedialab.sesame.facading.impl.FacadingImpl;
import at.newmedialab.templating.model.Template;
import at.newmedialab.templating.model.TemplateFacade;
import kiwi.core.api.config.ConfigurationService;
import kiwi.core.api.content.ContentService;
import kiwi.core.api.triplestore.SesameService;
import kiwi.core.exception.WritingNotSupportedException;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static at.newmedialab.sesame.commons.repository.ExceptionUtils.handleRepositoryException;

/**
 * User: Thomas Kurz
 * Date: 27.07.12
 * Time: 13:46
 */
@ApplicationScoped
public class RDFTemplateService {

    @Inject
    private Logger log;

    @Inject
    private ContentService contentService;

    @Inject
    private SesameService sesameService;

    @Inject
    private ConfigurationService configurationService;


    public List<Template> listTemplates() {
        try {
            RepositoryConnection con = sesameService.getConnection();

            try {
                Facading facading = new FacadingImpl(con);


                List<Template> templates = new ArrayList<Template>();

                URI r_type = con.getValueFactory().createURI(Namespaces.NS_RDF+"type");
                URI r_templ = con.getValueFactory().createURI(Namespaces.NS_TEMPLATING+"Template");

                RepositoryResult<Statement> triples = con.getStatements(null,r_type,r_templ,true);

                while(triples.hasNext()) {
                    Statement triple = triples.next();
                    if(facading.isFacadeable(triple.getSubject(),TemplateFacade.class)) {
                        templates.add(new Template(facading.createFacade(triple.getSubject(),TemplateFacade.class)));
                    }
                }
                return templates;
            } finally {
                con.commit();
                con.close();
            }
        } catch (RepositoryException e) {
            log.error("error accessing RDF repository",e);
            return Collections.emptyList();
        }

    }

    public Template addTemplate(String title, InputStream in) throws WritingNotSupportedException {
        try {
            RepositoryConnection conn = sesameService.getConnection();

            try {
                Facading facading = new FacadingImpl(conn);

                //create resource
                URI resource = conn.getValueFactory().createURI(configurationService.getBaseUri()+"resource/"+ UUID.randomUUID());

                // TODO: this should take the same connection as above!
                contentService.setContentStream(resource,in, Namespaces.MIME_TYPE_TEMPLATE);

                //set type and name
                TemplateFacade facade = facading.createFacade(resource, TemplateFacade.class);
                if(title!=null)facade.setTitle(title);
                return new Template(facade);
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException ex) {
            handleRepositoryException(ex,RDFTemplateService.class);
            return null;
        }
    }

    public void deleteTemplate(String resourceString) {
        try {
            RepositoryConnection conn = sesameService.getConnection();

            try {
                URI uri = conn.getValueFactory().createURI(resourceString);
                conn.remove(uri,null,null);
                conn.remove((Resource)null,null,uri);
            } finally {
                conn.commit();
                conn.close();
            }
        } catch (RepositoryException ex) {
            handleRepositoryException(ex,RDFTemplateService.class);
        }
    }
}
