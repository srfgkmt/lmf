<#--

    Copyright (C) 2013 Salzburg Research.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<!DOCTYPE HTML>
<html>

<head>
  <title>Resource in HTML</title>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
  <script src="${baseUri}core/public/js/lib/jquery-1.7.2.js"></script>
  <script src="${baseUri}core/public/js/lib/jquery-ui-1.8.21.js"></script>
  <link href="${baseUri}core/public/style/style1.css" title="screen" rel="stylesheet" type="text/css" />
  <link href="${baseUri}core/public/style/rdfhtml.css" title="screen" rel="stylesheet" type="text/css" />
  <link href="${baseUri}core/public/style/smoothness/jquery-ui-1.8.21.custom.css" title="screen" rel="stylesheet" type="text/css" />
</head>

<body>

<div id="header">
  <div id="logo">
    <a href="${baseUri}">
      <img src="${baseUri}core/public/img/lmf-white.png" alt="LMF" />
    </a>
  </div>
  <h1>LMF Linked Data Explorer - Hello World</h1>
</div>

<#function zebra index>
  <#if (index % 2) == 0>
    <#return "even" />
  <#else>
    <#return "odd" />
  </#if>
</#function>

<#function cacheClass object>
  <#if object.cache?has_content>
    <#return "ldcache" />
  <#else>
    <#return "" />
  </#if>
</#function>

<div id="tabs">

    <ul>
        <li><a href="#tabs-1">raw triples</a></li>
        <li><a href="#tabs-2">inspection</a></li>
        <!-- <li><a href="#tabs-3">bara</a></li> -->
    </ul>
    
    <div id="tabs-1">
   
        <#if resources?has_content>
          <#list resources as resource>
            <div class="subheader">
              <h3>Local description of <a href="${resource.uri}" class="ldcache">${resource.uri}</a>:</h3>
            </div>
            <table>
              <thead>
                <tr class="trClassHeader">
                  <th>property</th>
                  <th>has value</th>
                  <th>context</th>
                  <th id="info">info</th>
                </tr>
              <tbody>
                <#list resource.triples as triple>
                <tr class="${zebra(triple_index)}">
                  <td><a href="${triple.predicate.uri}" class="ldcache">${triple.predicate.curie}</a></td>
                  <#if triple.object.uri?has_content>
                  <td><a href="${triple.object.uri}" class="${cacheClass(triple.object)}">${triple.object.curie}</a></td>
                  <#else> 
                  <td>${triple.object.value}</td>
                  </#if>
                  <td><a href="${triple.context.uri}">${triple.context.curie}</a></td>
                  <td>${triple.info}</td>
                </tr>
                </#list>
              </tbody>
            </table>
            <p id="rawrdf">
              Get this resource in raw RDF: 
              <a href="${baseUri}resource?uri=${resource.encoded_uri}&amp;format=application/rdf%2Bxml">RDF/XML</a>, 
              <a href="${baseUri}resource?uri=${resource.encoded_uri}&amp;format=text/rdf%2Bn3">N3</a>, 
              <a href="${baseUri}resource?uri=${resource.encoded_uri}&amp;format=text/turtle">Turtle</a>, 
              <a href="${baseUri}resource?uri=${resource.encoded_uri}&amp;format=application/rdf%2Bjson">RDF/JSON</a>, 
              <a href="${baseUri}resource?uri=${resource.encoded_uri}&amp;format=application/json">JSON-LD</a>
            </p>
          </#list>
        <#else> 
          <div class='subheader'>
            <h3>No local triples to display!</h3>
          </div>
        </#if>   
        
    </div>
    
    <div id="tabs-2">
	    <#list resources as resource>
            <div class="subheader">
                <h3>Inspection of <a href="${resource.uri}" class="ldcache">${resource.uri}</a>:</h3>
            </div>
            <div>
                <h4><a href="${resource.uri}" class="ldcache">${resource.uri}</a> as Subject</h4>
                <button id="s0">|&lt;</button>
                <button id="s1">&lt;</button>
            	<button id="s2">&gt;</button>
            	<button id="s3">+</button>
            	<button id="s4">-</button>
            	<table id="inspect_subject">
            	<thead>
            	  <tr class="trClassHeader"><th>Subject<th>Property<th>Object<th>Context<th></th>
            	<tbody>
            	</table>
            </div>
            <div>
                <h4><a href="${resource.uri}" class="ldcache">${resource.uri}</a> as Property</h4>
            	<table id="inspect_property">
            	<thead>
            	  <tr class="trClassHeader"><th>Subject<th>Property<th>Object<th>Context<th></th>
            	<tbody>
            	</table>
            </div>
            <div>
                <h4><a href="${resource.uri}" class="ldcache">${resource.uri}</a> as Object</h4>
            	<table id="inspect_object">
            	<thead>
            	  <tr class="trClassHeader"><th>Subject<th>Property<th>Object<th>Context<th></th>
            	<tbody>
            	</table>
            </div>
            <div>
                <h4><a href="${resource.uri}" class="ldcache">${resource.uri}</a> as Context</h4>
            	<table id="inspect_context">
            	<thead>
            	  <tr class="trClassHeader"><th>Subject<th>Property<th>Object<th>Context<th></th>
            	<tbody>
            	</table>
            </div>
            
        <script type="text/javascript">
		  $(document).ready(function() {
		    function loader(uri, type, target) {
			    function createRow(data) {
			    	return $("<tr>", {})
			    		.append($("<td>", {text: data.s}))
			    		.append($("<td>", {text: data.p}))
			    		.append($("<td>", {text: data.o}))
			    		.append($("<td>", {text: data.c}))
			    		.append($());
			    }
		    	return {
		    		resource: uri,
		    		target: $(target),
		    		offset: 0,
		    		limit: 10,
		    		fetch: function() {
		    			var self = this;
		    			$.getJSON("${baseUri}inspect/" + type, {uri: self.resource, start: self.offset, limit: self.limit}, function(data) {
	  						self.target.empty();
		  					for( var i in data) {
		  						var t = data[i];
								self.target.append(createRow(t));							  			
		  					}
		  				});
		    		},
		    		next: function(step) {
		    			step = step || this.limit;
		    			this.offset += step;
		    			this.fetch();
		    		},
		    		prev: function(step) {
		    			step = step || this.limit
		    		    this.offset = Math.max(this.offset - step, 0);
		    		    this.fetch();
		    		},
		    		more: function() {
		    		   	this.limit += 5;
		    		   	this.fetch();
		    		},
		    		less: function() {
		    			this.limit = Math.max(this.limit - 5, 5);
		    			this.fetch();
		    		},
		    		first: function() {
		    			this.offset = 0;
		    			this.fetch();
		    		}
		    	};
		    }
		  	var subj = $("table#inspect_subject tbody");
		    var subjLoader = new loader("${resource.uri}", "subject", subj);
		    subjLoader.fetch();
		    $("#s0").click(function() {subjLoader.first();});
		    $("#s1").click(function() {subjLoader.prev();});
		    $("#s2").click(function() {subjLoader.next();});
		    $("#s3").click(function() {subjLoader.more();});
		    $("#s4").click(function() {subjLoader.less();});

		  	var prop = $("table#inspect_property tbody");
		    var propLoader = new loader("${resource.uri}", "predicate", prop);
		    propLoader.fetch();

		  	var obj = $("table#inspect_object tbody");
		    var objLoader = new loader("${resource.uri}", "object", obj);
		    objLoader.fetch();

		    
	      });   
        </script>
        </#list>
    </div>
    
    <!--
    <div id="tabs-3">
        <p>bar</p>
    </div>
    -->
    
</div>

<div id="footer" class="clear">
    <span><abbr title="Linked Media Framework">LMF</abbr> is a project of <a href="http://www.newmedialab.at/">SNML-TNG</a></span>
</div>

<script type="text/javascript"> 

  $(document).ready(function() {

    $("div#tabs").tabs();
    $("div#tabs div.ui-tabs-panel").css("height", $("div#tabs-1").height()+200);

    $("a.ldcache").each(function(index) { 
      $(this).click(function() { 
        window.location.href = "${baseUri}resource?uri=" + encodeURIComponent($(this).attr("href")); 
        return false; 
      }); 
    }); 

  });

</script> 

</body>

</html>
