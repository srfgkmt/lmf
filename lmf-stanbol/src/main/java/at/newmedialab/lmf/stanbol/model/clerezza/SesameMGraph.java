/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.clerezza;

import org.apache.clerezza.rdf.core.MGraph;
import org.apache.clerezza.rdf.core.NonLiteral;
import org.apache.clerezza.rdf.core.Resource;
import org.apache.clerezza.rdf.core.UriRef;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Sesame graph implementation (ruthlessly stolen and adapted from Clerezza).
 *
 * This class is threadsafe. The <code>SesameMGraph</code> is synchronized
 * against itself. Note that the iteration over triples using an
 * <code>Iterator</code> returned by {@link #iterator()} or
 * {@link #filter(NonLiteral, UriRef, Resource)} must be manually synched.
 *
 * @author szalay
 */
public class SesameMGraph extends AbstractSesameMGraph implements MGraph {

    private final Repository repository;

    private ReentrantLock lock = new ReentrantLock();

    // Sesame context to filter on; will be ignored if null
    private org.openrdf.model.Resource context = null;

    /**
     * Default constructor to create a new instance of
     * <code>SesameMGraph</code>.
     */
    public SesameMGraph(Repository repository) {
        super(repository.getValueFactory(), null);
        this.repository = repository;
    }

    public SesameMGraph(Repository repository, org.openrdf.model.Resource context) {
        super(repository.getValueFactory(), context);
        this.repository = repository;
    }

    /**
     * Aqcuire the repository connection used by this repository, possibly creating a lock to avoid
     * concurrent access to the connection.
     *
     * @return
     */
    @Override
    protected RepositoryConnection aqcuireConnection() {
        try {
            RepositoryConnection conn = repository.getConnection();
            conn.begin();
            return conn;
        } catch (RepositoryException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Release the repository connection used by this repository, possibly releasing resources
     * occupied by the connection.
     *
     * @return
     */
    @Override
    protected void releaseConnection(RepositoryConnection connection) {
        try {
            connection.close();
        } catch (RepositoryException e) {
            throw new RuntimeException(e);
        }
    }
}