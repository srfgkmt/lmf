/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.api;

import at.newmedialab.lmf.stanbol.model.enhancer.Enhancement;
import org.openrdf.repository.Repository;

import java.io.InputStream;
import java.util.List;

/**
 * Allows accessing the enhancer chains of the locally-installed Apache Stanbol server.
 * <p/>
 * Author: Sebastian Schaffert
 */
public interface StanbolEnhancerService {




    /**
     * Return the enhancements for the given text using the default enhancement chain.
     * @param text
     * @return
     */
    public List<Enhancement> getEnhancements(String text);


    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument.
     * @param text
     * @return
     */
    public List<Enhancement> getEnhancements(String text, String chain);



    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     * @param text
     * @return
     */
    public Repository getEnhancementsRDF(String text);


    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     * @param text
     * @param chain
     * @return
     */
    public Repository getEnhancementsRDF(String text, String chain);


    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     *
     * @param content an input stream to read the content from; will be closed when reading is finished
     * @param contentType
     * @return
     */
    public Repository getEnhancementsRDF(InputStream content, String contentType);


    /**
     * Return the enhancements for the given text using the enhancement chain with the name passed as argument as
     * Sesame RDF repository for further processing.
     *
     *
     * @param content an input stream to read the content from; will be closed when reading is finished
     * @param contentType
     *@param chain
     * @return
     */
    public Repository getEnhancementsRDF(InputStream content, String contentType, String chain);

}
