/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.api;

import org.openrdf.model.URI;

import java.util.List;

/**
 * This service handles registration and automatic publishing of LMF contexts to Stanbol managed sites. It maintains
 * a list of published LMF contexts in the configuration variable stanbol.published_contexts (list of URIs).
 * <p/>
 * On startup, it checks whether a managed site exists for each published context; if not, a new site is created and
 * the complete RDF data of this context is published to Stanbol.
 * <p/>
 * On transaction commit, it lists all affected resources and updates the relevant triple data from the published
 * contexts to the managed sites.
 * <p/>
 * The names for the managed sites are derived from the context URI by building a hash (md5 or similar).
 *
 *
 *
 * Author: Sebastian Schaffert
 */
public interface StanbolSyncService {

    /**
     * Return a list of URIs of the contexts that are published as managed sites from the system configuration.
     *
     * Note that this method does not check whether the contexts are also available in Stanbol. See
     * listPublishedContexts() instead.
     *
     * @return
     */
    public List<URI> listEnabledContexts();


    /**
     * Return a list of URIs of the contexts that are actually published as managed sites in Stanbol.
     *
     * @return
     */
    public List<URI> listPublishedContexts();


    /**
     * Return true if the context given as argument is published as managed site in Stanbol.
     * ‚
     *
     * @param context
     * @return
     */
    public boolean isPublished(URI context);

    /**
     * Publish the context with the given URI as Stanbol managed site. If the boolean argument "create" is true,
     * the managed site will be (re-)created and the data republished from the data contained in the LMF. Otherwise,
     * this method will just register the context for syncing on transaction commit.
     *
     * @param uri
     * @param create
     */
    public void activateContext(URI uri, boolean create);


    /**
     * Stop publishing the context with the given URI as Stanbol managed site; If the boolean argument "destroy" is true,
     * the managed site will be destroyed as well. Otherwise, it is just removed from the internal list for transaction
     * notification.
     *
     * @param uri
     * @param destroy
     */
    public void deactivateContext(URI uri, boolean destroy);


    /**
     * Return the URL of the managed site for this context. Does not check for existence, just creates the URL.
     * @param uri
     * @return
     */
    public String getSiteEndpoint(URI uri);


    /**
     * Return the name of the managed site for this context. Does not check for existence, just creates the name.
     * @param uri
     * @return
     */
    public String getSiteName(URI uri);
}
