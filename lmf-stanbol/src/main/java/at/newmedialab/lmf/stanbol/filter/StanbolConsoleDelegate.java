/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.filter;

import org.apache.felix.http.proxy.ProxyServlet;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.modules.MarmottaHttpFilter;
import org.jboss.resteasy.spi.DefaultOptionsMethodException;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.descriptor.JspConfigDescriptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * A filter delegating to the Stanbol Web Console.
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class StanbolConsoleDelegate implements MarmottaHttpFilter {

    public static final String PREFIX = "/stanbol/config";
    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

    private Servlet delegate;

    private ServletContext delegateContext;

    /**
     * Return the pattern (regular expression) that a request URI (relative to the LMF base URI) has to match
     * before triggering this filter.
     *
     * @return
     */
    @Override
    public String getPattern() {
        return PREFIX + "/.*";
    }

    /**
     * Return the priority of the filter. Filters that need to be executed before anything else should return
     * PRIO_FIRST, filters that need to be executed last in the chain should return PRIO_LAST, all other filters
     * something inbetween (e.g. PRIO_MIDDLE).
     *
     * @return
     */
    @Override
    public int getPriority() {
        return PRIO_MIDDLE;
    }

    /**
     * Called by the web container to indicate to a filter that it is being placed into
     * service. The servlet container calls the init method exactly once after instantiating the
     * filter. The init method must complete successfully before the filter is asked to do any
     * filtering work. <br><br>
     * <p/>
     * The web container cannot place the filter into service if the init method either<br>
     * 1.Throws a ServletException <br>
     * 2.Does not return within a time period defined by the web container
     * <p/>
     * Our custom implementation builds a custom servlet context for the OSGi HTTP service, since we want
     * to mount it at a different path than the root.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        delegate = new ProxyServlet();

        delegateContext = new StanbolServletContext(configurationService.getServletContext());
        ServletConfig cfg = new ServletConfig() {
            @Override
            public String getServletName() {
                return "StanbolConsoleDelegate";
            }

            @Override
            public ServletContext getServletContext() {
                return delegateContext;
            }

            @Override
            public String getInitParameter(String name) {
                return null;
            }

            @Override
            public Enumeration<String> getInitParameterNames() {
                return new Vector<String>().elements();

            }
        };
        delegate.init(cfg);

        log.info("started Apache Stanbol Web Console delegate");
    }

    /**
     * The <code>doFilter</code> method of the Filter is called by the container
     * each time a request/response pair is passed through the chain due
     * to a client request for a resource at the end of the chain. The FilterChain passed in to this
     * method allows the Filter to pass on the request and response to the next entity in the
     * chain.<p>
     * A typical implementation of this method would follow the following pattern:- <br>
     * 1. Examine the request<br>
     * 2. Optionally wrap the request object with a custom implementation to
     * filter content or headers for input filtering <br>
     * 3. Optionally wrap the response object with a custom implementation to
     * filter content or headers for output filtering <br>
     * 4. a) <strong>Either</strong> invoke the next entity in the chain using the FilterChain object (<code>chain.doFilter()</code>), <br>
     * * 4. b) <strong>or</strong> not pass on the request/response pair to the next entity in the filter chain to block the request processing<br>
     * * 5. Directly set headers on the response after invocation of the next entity in the filter chain.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(request instanceof HttpServletRequest && ((HttpServletRequest) request).getMethod().equalsIgnoreCase("OPTIONS")) {
            throw new DefaultOptionsMethodException("OPTIONS request not supported by Stanbol", Response.status(Response.Status.BAD_REQUEST).build());
        }


        if (delegate == null) {
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            HttpServletRequest wrapped = new HttpServletRequestWrapper((HttpServletRequest) request) {

                @Override
                public String getPathInfo() {
                    if(super.getPathInfo() != null) return super.getPathInfo().substring(PREFIX.length());
                    else
                        return super.getServletPath().substring(PREFIX.length());
                }

                @Override
                public String getPathTranslated() {
                    return null;
                }

                /**
                 * The default behavior of this method is to return getContextPath()
                 * on the wrapped request object.
                 */
                @Override
                public String getContextPath() {
                    return super.getContextPath() + PREFIX;
                }

                /**
                 * The default behavior of this method is to return getServletPath()
                 * on the wrapped request object.
                 */
                @Override
                public String getServletPath() {
                    //return super.getServletPath().substring(PREFIX.length());
                    return "";
                }


            };

            delegate.service(wrapped, response);
        }
    }

    /**
     * Called by the web container to indicate to a filter that it is being taken out of service. This
     * method is only called once all threads within the filter's doFilter method have exited or after
     * a timeout period has passed. After the web container calls this method, it will not call the
     * doFilter method again on this instance of the filter. <br><br>
     * <p/>
     * This method gives the filter an opportunity to clean up any resources that are being held (for
     * example, memory, file handles, threads) and make sure that any persistent state is synchronized
     * with the filter's current state in memory.
     */
    @Override
    public void destroy() {
        delegate.destroy();
    }

    @SuppressWarnings({ "deprecation", "rawtypes" })
    private class StanbolServletContext implements ServletContext {

        private ServletContext parent;


        private StanbolServletContext(ServletContext parent) {
            this.parent = parent;
        }

        @Override
        public Object getAttribute(String name) {
            return parent.getAttribute(name);
        }

        @Override
        public String getContextPath() {
            return parent.getContextPath() + PREFIX;
        }

        @Override
        public ServletContext getContext(String uripath) {
            return parent.getContext(uripath);
        }

        @Override
        public int getMajorVersion() {
            return parent.getMajorVersion();
        }

        @Override
        public int getMinorVersion() {
            return parent.getMinorVersion();
        }

        @Override
        public String getMimeType(String file) {
            return parent.getMimeType(file);
        }

        @Override
        public Set<String> getResourcePaths(String path) {
            Set<String> result = new HashSet<String>();
            for(Object resource : parent.getResourcePaths(path)) {
                if(resource.toString().startsWith(PREFIX)) {
                    result.add(resource.toString().substring(PREFIX.length()));
                }

            }
            return result;
        }

        @Override
        public URL getResource(String path) throws MalformedURLException {
            return parent.getResource(PREFIX + path);
        }

        @Override
        public InputStream getResourceAsStream(String path) {
            return parent.getResourceAsStream(PREFIX + path);
        }

        @Override
        public RequestDispatcher getRequestDispatcher(String path) {
            return parent.getRequestDispatcher(PREFIX + path);
        }

        @Override
        public RequestDispatcher getNamedDispatcher(String name) {
            return parent.getNamedDispatcher(name);
        }

        @Override
        public Servlet getServlet(String name) throws ServletException {
            return parent.getServlet(name);
        }

        @Override
        public Enumeration getServlets() {
            return parent.getServlets();
        }

        @Override
        public Enumeration getServletNames() {
            return parent.getServletNames();
        }

        @Override
        public void log(String msg) {
            log.info(msg);
        }

        @Override
        public void log(Exception exception, String msg) {
            log.error(msg,exception);
        }

        @Override
        public void log(String message, Throwable throwable) {
            log.error(message,throwable);
        }

        @Override
        public String getRealPath(String path) {
            return parent.getRealPath(PREFIX + path);
        }

        @Override
        public String getServerInfo() {
            return parent.getServerInfo();
        }

        @Override
        public String getInitParameter(String name) {
            return parent.getInitParameter(name);
        }

        @Override
        public Enumeration getInitParameterNames() {
            return parent.getInitParameterNames();
        }

        @Override
        public Enumeration getAttributeNames() {
            return parent.getAttributeNames();
        }

        @Override
        public void setAttribute(String name, Object object) {
            parent.setAttribute(name,object);
        }

        @Override
        public void removeAttribute(String name) {
            parent.removeAttribute(name);
        }

        @Override
        public String getServletContextName() {
            return parent.getServletContextName();
        }

        @Override
        public int getEffectiveMajorVersion() {
            return parent.getEffectiveMajorVersion();
        }

        @Override
        public int getEffectiveMinorVersion() {
            return parent.getEffectiveMinorVersion();
        }

        @Override
        public boolean setInitParameter(String name, String value) {
            return parent.setInitParameter(name,value);
        }

        @Override
        public ServletRegistration.Dynamic addServlet(String servletName, String className) {
            return parent.addServlet(servletName, className);
        }

        @Override
        public ServletRegistration.Dynamic addServlet(String servletName, Servlet servlet) {
            return parent.addServlet(servletName, servlet);
        }

        @Override
        public ServletRegistration.Dynamic addServlet(String servletName, Class<? extends Servlet> servletClass) {
            return parent.addServlet(servletName, servletClass);
        }

        @Override
        public <T extends Servlet> T createServlet(Class<T> clazz) throws ServletException {
            return parent.createServlet(clazz);
        }

        @Override
        public ServletRegistration getServletRegistration(String servletName) {
            return parent.getServletRegistration(servletName);
        }

        @Override
        public Map<String, ? extends ServletRegistration> getServletRegistrations() {
            return parent.getServletRegistrations();
        }

        @Override
        public FilterRegistration.Dynamic addFilter(String filterName, String className) {
            return parent.addFilter(filterName, className);
        }

        @Override
        public FilterRegistration.Dynamic addFilter(String filterName, Filter filter) {
            return parent.addFilter(filterName, filter);
        }

        @Override
        public FilterRegistration.Dynamic addFilter(String filterName, Class<? extends Filter> filterClass) {
            return parent.addFilter(filterName, filterClass);
        }

        @Override
        public <T extends Filter> T createFilter(Class<T> clazz) throws ServletException {
            return parent.createFilter(clazz);
        }

        @Override
        public FilterRegistration getFilterRegistration(String filterName) {
            return parent.getFilterRegistration(filterName);
        }

        @Override
        public Map<String, ? extends FilterRegistration> getFilterRegistrations() {
            return parent.getFilterRegistrations();
        }

        @Override
        public SessionCookieConfig getSessionCookieConfig() {
            return parent.getSessionCookieConfig();
        }

        @Override
        public void setSessionTrackingModes(Set<SessionTrackingMode> sessionTrackingModes) {
            parent.setSessionTrackingModes(sessionTrackingModes);
        }

        @Override
        public Set<SessionTrackingMode> getDefaultSessionTrackingModes() {
            return parent.getDefaultSessionTrackingModes();
        }

        @Override
        public Set<SessionTrackingMode> getEffectiveSessionTrackingModes() {
            return parent.getEffectiveSessionTrackingModes();
        }

        @Override
        public void addListener(String className) {
            parent.addListener(className);
        }

        @Override
        public <T extends EventListener> void addListener(T t) {
            parent.addListener(t);
        }

        @Override
        public void addListener(Class<? extends EventListener> listenerClass) {
            parent.addListener(listenerClass);
        }

        @Override
        public <T extends EventListener> T createListener(Class<T> clazz) throws ServletException {
            return parent.createListener(clazz);
        }

        @Override
        public JspConfigDescriptor getJspConfigDescriptor() {
            return parent.getJspConfigDescriptor();
        }

        @Override
        public ClassLoader getClassLoader() {
            return parent.getClassLoader();
        }

        @Override
        public void declareRoles(String... roleNames) {
            parent.declareRoles(roleNames);
        }
    }

}
