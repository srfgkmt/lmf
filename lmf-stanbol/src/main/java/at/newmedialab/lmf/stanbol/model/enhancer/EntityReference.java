/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.enhancer;

import com.google.common.primitives.Doubles;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

/**
 * Representation of a Stanbol EntityAnnotation (reference to a Linked Data resource).
 * <p/>
 * Author: Sebastian Schaffert
 */
public class EntityReference implements Comparable<EntityReference> {

    private String label;

    /** reference to the Linked Data URI of the entity */
    private URI reference;

    /** collection of types associated with the entity */
    private Set<URI> types;

    /** confidence about the validity of the entity reference as returned by Stanbol */
    private double confidence;


    public EntityReference(String label, URI reference, double confidence) {
        this.label = label;
        this.reference = reference;
        this.types = new HashSet<URI>();
        this.confidence = confidence;
    }


    public EntityReference(String label, URI reference, Set<URI> types, double confidence) {
        this.label = label;
        this.reference = reference;
        this.types = types;
        this.confidence = confidence;
    }

    public URI getReference() {
        return reference;
    }

    public void setReference(URI reference) {
        this.reference = reference;
    }

    public Set<URI> getTypes() {
        return types;
    }

    public void addType(URI type) {
        types.add(type);
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public int compareTo(EntityReference entityReference) {
        return Doubles.compare(entityReference.confidence,confidence);
    }
}
