/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.model.config;

import at.newmedialab.stanbol.configurator.model.LMFKeywordLinkingEngineConfig;

/**
 * Representation of a KeywordLinkingEngine on the LMF side. Will be translated into a REST call to Stanbol to
 * create new KeywordLinkingEngine instances.
 * <p/>
 * Author: Sebastian Schaffert
 */
@Deprecated
public class KeywordLinkingEngineConfig extends LMFKeywordLinkingEngineConfig implements StanbolEngineConfiguration {

    public KeywordLinkingEngineConfig(String name, String site, String labelField) {
        super(name,site,labelField);
    }


    /**
     * Return the name of this web configuration
     *
     * @return
     */
    @Override
    public String getId() {
        return getName();
    }

    /**
     * Update the name of this web configuration
     *
     * @param id
     */
    @Override
    public void setId(String id) {
        setName(id);
    }


}
