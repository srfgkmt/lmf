/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.stanbol.ldpath;

import at.newmedialab.lmf.stanbol.api.StanbolEnhancerService;
import at.newmedialab.lmf.stanbol.model.enhancer.Enhancement;
import org.apache.marmotta.ldpath.api.backend.RDFBackend;
import org.apache.marmotta.ldpath.model.transformers.StringTransformer;
import org.apache.marmotta.platform.ldpath.api.AutoRegisteredLDPathFunction;
import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Apply content enhancement on the selected literals using Apache Stanbol. The function can be called as either:
 * - fn:stanbol(PATH_SELECTOR, CHAIN_NAME): apply content enhancement on the selected literals using the chain given as argument
 * - fn:stanbol(PATH_SELECTOR): apply content enhancement on the selected literals using the default chain
 * - fn:stanbol(): apply content enhancement on the current context node using the default chain
 *
 * The result of the function call is always a list of URI resources pointing to the entities detected by Stanbol
 * (only the best match selected). Path selection can therefore continue after calling this method, e.g.
 *
 * dc:description / fn:stanbol() [rdf:type is dbpedia:Person] / rdfs:label[@en]
 *
 * <p/>
 * Author: Sebastian Schaffert
 */
@ApplicationScoped
public class StanbolLDPathFunction extends AutoRegisteredLDPathFunction {

    @Inject
    private Logger log;

    @Inject
    private StanbolEnhancerService enhancerService;


    private final StringTransformer<Value> transformer = new StringTransformer<Value>();

    /**
     * Apply the function to the list of nodes passed as arguments and return the result as type T.
     * Throws IllegalArgumentException if the function cannot be applied to the nodes passed as argument
     * or the number of arguments is not correct.
     *
     * @param context the context of the execution. Same as using the
     *                {@link org.apache.marmotta.ldpath.api.selectors.NodeSelector} '.' as parameter.
     * @param args    a nested list of KiWiNodes
     * @return
     */
    @Override
    public Collection<Value> apply(RDFBackend<Value> backend, Value context, Collection<Value>... args) throws IllegalArgumentException {

        Iterator<Value> it;
        if(args.length == 0){
            it = Collections.singleton(context).iterator();
        } else {
            it = args[0].iterator();
        }

        String chainName = null;
        if(args.length == 2 && args[1].size() > 0) {
            chainName = transformer.transform(backend,args[1].iterator().next(), null);
        }

        ArrayList<Value> result = new ArrayList<Value>();
        while(it.hasNext()) {
            Value node = it.next();
            if(node instanceof Literal) {
                String text = transformer.transform(backend,node, null).trim();

                if(!"".equals(text)) {
                    List<Enhancement> enhancements;
                    if(chainName != null) {
                        enhancements = enhancerService.getEnhancements(transformer.transform(backend,node, null),chainName);
                    } else {
                        enhancements = enhancerService.getEnhancements(transformer.transform(backend,node, null));
                    }
                    for(Enhancement enhancement : enhancements) {
                        if(enhancement.getEntities().size() > 0) {
                            URI entity = (URI)backend.createURI(enhancement.getEntities().first().getReference().toString());
                            result.add(entity);
                        }
                    }
                }

            }
        }

        return result;
    }

    /**
     * Return the representation of the NodeFunction or NodeSelector in the RDF Path Language
     *
     * @return
     */
    @Override
    public String getLocalName() {
        return "stanbol";
    }

    /**
     * A string describing the signature of this node function, e.g. "fn:content(uris : Nodes) : Nodes". The
     * syntax for representing the signature can be chosen by the implementer. This method is for informational
     * purposes only.
     *
     * @return
     */
    @Override
    public String getSignature() {
        return "fn:stanbol(nodes : LiteralList [, chain_name : String]]) : NodeList";
    }

    /**
     * A short human-readable description of what the node function does.
     *
     * @return
     */
    @Override
    public String getDescription() {
        return "Applies Apache Stanbol enhancement on the literals passed as argument. Optionally, the name of an enhancement" +
                " chain can be given, otherwise the default chain will be used. Can also be used in-path, in which case enhancement" +
                " is applied to the context node.";
    }
}
