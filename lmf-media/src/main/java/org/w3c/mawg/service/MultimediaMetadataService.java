/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.w3c.mawg.service;

import nu.xom.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.marmotta.commons.constants.*;
import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.FacadingFactory;
import org.apache.marmotta.commons.sesame.facading.api.Facading;
import org.apache.marmotta.commons.vocabulary.MA;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.content.ContentService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.codehaus.jackson.map.ObjectMapper;
import org.openrdf.model.*;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.w3c.mawg.model.Constants;
import org.w3c.mawg.model.LocationFacade;
import org.w3c.mawg.model.MediaFormat;
import org.w3c.mawg.model.RatingFacade;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * User: Thomas Kurz
 * Date: 15.02.12
 * Time: 15:43
 */
@ApplicationScoped
public class MultimediaMetadataService {

    @Inject
    SesameService sesameService;

    @Inject
    ContentService contentService;

    @Inject
    ConfigurationService configurationService;

    public URI create(String uri, Repository repository) throws Exception {

        RepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            ValueFactory factory = repository.getValueFactory();

            URI resource = factory.createURI(uri);
            URI property = RDF.TYPE;
            URI object = MA.MediaResource;

            connection.add(resource,property,object);

            connection.commit();

            return resource;

        } catch(Exception e) {
            if(connection != null) connection.rollback();
            throw new Exception("some failure occurred when creating resource");
        } finally {
            if(connection != null) connection.close();
        }
    }

	public void update(String uri, String metadata_url, MediaFormat format, Repository repository) throws Exception {

        RepositoryConnection connection = null;
		try {
            connection = repository.getConnection();
            ValueFactory factory = repository.getValueFactory();

            //set metadata
            URI metadata = null;
            switch(format) {
                case DC: metadata=parseDC(createInputStream(metadata_url),connection,factory);break;
                case YT: metadata=parseYoutube(createInputStream(metadata_url),connection,factory);break;
                default: throw new Exception("Format "+format.name()+" not supported");
            }

            URI resource = factory.createURI(uri);
            URI relation = factory.createURI(format.getRelation());

            connection.add(resource,relation,metadata);
            //set data
            contentService.setContentData(metadata, IOUtils.toByteArray(createInputStream(metadata_url)),format.getMimeType());

            connection.commit();

        } catch(Exception e) {
            if(connection != null) connection.rollback();
            throw new Exception("some failure occurred when creating metadata");
        } finally {
            if(connection != null) connection.close();
        }
    }

	public String read(String uri, MediaFormat format, String[] properties, Repository repository) throws Exception {
        RepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            ValueFactory factory = repository.getValueFactory();

            URI resource = factory.createURI(uri);
            URI metadata = getMetadataResource(resource,format,connection,factory);

            String result = null;
            switch(format) {
                case DC: result=serialize(metadata, properties, Constants.DC_PROPERTIES, MediaFormat.DC,connection,factory);break;
                case YT: result=serialize(metadata, properties, Constants.YT_PROPERTIES, MediaFormat.YT,connection,factory);break;
                default: throw new Exception("Format "+format.name()+" not supported");
            }

            connection.commit();

            return result;

        } catch(Exception e) {
            if(connection != null) connection.rollback();
            throw new Exception("some failure occurred when creating metadata");
        } finally {
            if(connection != null) connection.close();
        }
	}

	public String original(String uri,MediaFormat format, Repository repository) throws Exception {
        RepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            ValueFactory factory = repository.getValueFactory();

            URI resource = factory.createURI(uri);
            URI metadata = getMetadataResource(resource,format,connection,factory);

		    ArrayList<HashMap<String,String>> object = new ArrayList<HashMap<String, String>>();
		    HashMap<String,String> status = new HashMap<String, String>();
		    HashMap<String,String> data = new HashMap<String, String>();
		    status.put("statusCode","200");
		    data.put("originalMetadata",new String(contentService.getContentData(metadata,format.getMimeType()),"UTF-8"));
		    object.add(status);
		    object.add(data);
		    ObjectMapper mapper = new ObjectMapper();
		    StringWriter result = new StringWriter();
		    mapper.writeValue(result,object);
            connection.commit();

		    return result.toString();
        } catch(Exception e) {
            if(connection != null) connection.rollback();
            throw new Exception("some failure occurred when creating metadata");
        } finally {
            if(connection != null) connection.close();
        }
	}

	private URI getMetadataResource(URI resource, MediaFormat format, RepositoryConnection connection, ValueFactory factory) throws Exception {
        URI realtion = factory.createURI(format.getRelation());
        RepositoryResult<Statement> statements = connection.getStatements(resource,realtion,null,false);

		if(!statements.hasNext()) throw new Exception("No metadata for format "+format.name());
		return (URI)statements.next().getObject();
	}

	/**
	 * OK
	 * @param in
	 * @return
	 * @throws Exception
	 */
	private URI parseYoutube(InputStream in,RepositoryConnection connection,ValueFactory factory) throws Exception {

        try {
            URI resource = factory.createURI(configurationService.getBaseUri()+"resource/"+UUID.randomUUID());
  			Builder parser = new Builder();
  			Document doc = parser.build(in);
			for(String key : Constants.YT_MAPPINGS.keySet()) {
				Constants.YT_MAPPINGS.get(key).toTriples(resource,doc,connection,factory,configurationService);
			}
            return resource;
		} catch (ParsingException ex) {
  			System.err.println("malformed dc data");
			throw new Exception("cannot parse input data");
		} catch (IOException ex) {
  			System.err.println("cannot read dc data");
			throw new Exception("cannot read input data");
		}
	}
	private String serialize(URI resource,String[] properties,HashMap<String,String> allowed, MediaFormat format, RepositoryConnection connection, ValueFactory factory) throws IOException {
		ArrayList<HashMap<String,HashMap<String,String>>> object = new ArrayList<HashMap<String,HashMap<String, String>>>();
		Set<String> filtered = new HashSet<String>();
		//filter
		if(properties!=null) {
			for(String s : properties) {
				if(allowed.keySet().contains(s))filtered.add(s);
			}
		} else {
			filtered = allowed.keySet();
		}
		if(filtered.size()==0) {
			HashMap m = new HashMap<String,HashMap<String,Object>>();
			m.put("MediaAnnotation",createReturnItem("","","","","","",462));
			object.add(m);
		} else {
			for(String s:filtered) {
				try {
					List<String> values = getValues(resource,s,format, connection, factory);
					for(String value : values) {
						//special mapping
						HashMap m = new HashMap<String,HashMap<String,Object>>();
						String type =  WordUtils.capitalize(s);
						if(s.equals("date")) type="MADate";
						if(s.equals("location")) {
							m.put("Location",createLocationItem(resource, value,"",format.name().toLowerCase(),"",getMatchingType(s,format),connection,factory));
						}else if(s.equals("rating")) {
							m.put("Rating",createRatingItem(resource, value, "", format.name().toLowerCase(), "", getMatchingType(s, format),connection,factory));
						} else {
                            String language = getLanguage(resource,s,connection,factory);
							m.put(type,createReturnItem(s,value,language,format.name().toLowerCase(),"",getMatchingType(s,format),200));
						}
						object.add(m);
					}
				} catch(Exception e) {
					continue;
				}
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		StringWriter result = new StringWriter();
		mapper.writeValue(result,object);
		return result.toString();
	}

    private String getLanguage(URI resource, String property, RepositoryConnection connection, ValueFactory factory) throws RepositoryException {
        URI propertyURI = factory.createURI(Constants.MA_NAMESPACE+property);

        RepositoryResult<Statement> statements = connection.getStatements(resource,propertyURI,null,false);
        while(statements.hasNext()) {
            Statement s = statements.next();
            if(s.getObject() instanceof Literal) {
                String lang = ((Literal) s.getObject()).getLanguage();
                if(lang != null) return lang;
            }
        }
        return "";
    }

	private String getMatchingType(String property, MediaFormat format) {
		switch(format) {
			case DC: return Constants.DC_PROPERTIES.get(property);
			case YT: return Constants.YT_PROPERTIES.get(property);
			default: return "";
		}
	}

	private List<String> getValues(URI resource, String property, MediaFormat format, RepositoryConnection connection, ValueFactory factory) throws RepositoryException {
		String property_uri = "";
		switch (format) {
			case DC: property_uri = Constants.MA_NAMESPACE+property;break;
			case YT: property_uri = Constants.MA_NAMESPACE+property;break;
		}

        URI propertyURI = factory.createURI(property_uri);

        RepositoryResult<Statement> statements = connection.getStatements(resource,propertyURI,null,false);
		List<String> values = new LinkedList<String>();
        while(statements.hasNext()) {
            Statement s = statements.next();
            if(s.getObject() instanceof Literal) {
                values.add(s.getObject().stringValue());
            } else {
                values.add(null);
            }
        }
        return values;
	}

	/**
	 * OK
	 * @param in
	 * @return
	 * @throws Exception
	 */
	private URI parseDC(InputStream in, RepositoryConnection connection,ValueFactory factory) throws Exception {

        try {
            URI resource = factory.createURI(configurationService.getBaseUri()+"resource/"+UUID.randomUUID());
            Builder parser = new Builder();
            Document doc = parser.build(in);
            Element root = doc.getRootElement();
            for(String property_name : Constants.DC_MAPPINGS.keySet()) {
                Element prop = root.getFirstChildElement(property_name, Constants.DC_NAMESPACE);
                if(prop!=null) {
                    Literal literal = factory.createLiteral(prop.getValue().trim());
                    Attribute lang = prop.getAttribute("lang","http://www.w3.org/XML/1998/namespace");

                    if(lang != null) {
                        literal = factory.createLiteral(prop.getValue().trim(),lang.getValue());
                    }

                    URI property = factory.createURI(Constants.MA_NAMESPACE+Constants.DC_MAPPINGS.get(property_name));
                    connection.add(resource,property,literal);
                }
            }
            return resource;
        } catch (ParsingException ex) {
            System.err.println("malformed dc data");
            throw new Exception("cannot parse input data");
        } catch (IOException ex) {
            System.err.println("cannot read dc data");
            throw new Exception("cannot read input data");
        }
	}

	private InputStream createInputStream(String url_string) throws IOException {
		URL url = new URL(url_string);
        URLConnection connection = url.openConnection();
        return connection.getInputStream();
	}

	private HashMap<String,Object> createReturnItem(String propertyName, String value, String language, String sourceFormat, String fragmentIdentifier, String mappingType, int statusCode) {
		HashMap<String,Object> prop_val = new HashMap<String, Object>();
		prop_val.put("propertyName",propertyName);
		prop_val.put("value",value);
		prop_val.put("language",language);
		prop_val.put("sourceFormat",sourceFormat);
		prop_val.put("fragmentIdentifier",fragmentIdentifier);
		prop_val.put("mappingType",mappingType);
		prop_val.put("statusCode",statusCode);
		if (propertyName.equals("title")) {
			prop_val.put("titleLabel",value);
			prop_val.put("typeLink","");//TBD
			prop_val.put("typeLabel","");//TBD
		} else if (propertyName.equals("identifier")) {
			prop_val.put("identifierLink",value);
		} else if (propertyName.equals("language")) {
			prop_val.put("languageLabel",value);
			prop_val.put("languageLink","");//TBD
		} else if (propertyName.equals("locator")) {
		    prop_val.put("locatorLink",value);
		} else if (propertyName.equals("contributor")) {
			prop_val.put("contributorLink","");//TBD
			prop_val.put("contributorLabel",value);
			prop_val.put("roleLink","");//TBD
			prop_val.put("roleLabel","contributor");
			//prop_val.put("statusCode",206);
		} else if (propertyName.equals("creator")) {
			prop_val.put("creatorLink","");//TBD
			prop_val.put("creatorLabel",value);
			prop_val.put("roleLink","");//TBD
			prop_val.put("roleLabel","creator");
		} else if (propertyName.equals("date")) {
			prop_val.put("date",value);
			prop_val.put("typeLink","");//TBD
			prop_val.put("typeLabel","");//TBD
			//prop_val.put("statusCode",206);
		} else if (propertyName.equals("location")) {
			//prop_val.put("statusCode",206);
		} else if (propertyName.equals("description")) {
			prop_val.put("descriptionLabel",value);
		} else if (propertyName.equals("keyword")) {
			prop_val.put("keywordLabel",value);
			prop_val.put("keywordLink","");//TBD
		} else if (propertyName.equals("relation")) {
			prop_val.put("typeLink","");//TBD
			prop_val.put("typeLabel","");//TBD
			prop_val.put("targetLink","");//TBD
			prop_val.put("targetLabel",value);
			//prop_val.put("statusCode",206);
		} else if (propertyName.equals("copyright")) {
		    prop_val.put("copyrightLabel",value);
			prop_val.put("holderLabel","");
			prop_val.put("holderLink","");
			//prop_val.put("statusCode",206);
		} else if (propertyName.equals("publisher")) {
			prop_val.put("publisherLabel",value);
			prop_val.put("publisherLink","");//TBD
		} else if (propertyName.equals("format")) {
			prop_val.put("formatLabel",value);
			prop_val.put("formatLink","");//TBD
		} else if (propertyName.equals("targetAudience")) {
			prop_val.put("audienceLink","");//TBD
			prop_val.put("audienceLabel",value);
			prop_val.put("classificationSystemLink","");//TBD
			prop_val.put("classificationSystemLabel","");//TBD
			//prop_val.put("statusCode",206);
		} else if (propertyName.equals("compression")) {
			prop_val.put("compressionLink","");//TBD
			prop_val.put("compressionLabel",value);
		} else if (propertyName.equals("duration")) {
			prop_val.put("duration",Integer.parseInt(value));
		} else if(propertyName.equals("location")) {
			//if coordinate system
		}
		return prop_val;
	}

	private HashMap<String,Object> createLocationItem(URI resource, String value, String language, String sourceFormat, String fragmentIdentifier, String mappingType, RepositoryConnection connection, ValueFactory factory) throws RepositoryException {
		HashMap<String,Object> prop_val = new HashMap<String, Object>();
		prop_val.put("propertyName","location");
		prop_val.put("language",language);
		prop_val.put("sourceFormat",sourceFormat);
		prop_val.put("fragmentIdentifier",fragmentIdentifier);
		prop_val.put("mappingType",mappingType);

		if(value!=null) {
	    	prop_val.put("statusCode",200);
			prop_val.put("value",value);
			prop_val.put("locationLink","");
			prop_val.put("locationLabel",value);
			prop_val.put("longitude",0);
			prop_val.put("latitude",0);
			prop_val.put("altitude",0);
			prop_val.put("coordinateSystemLabel","");
			prop_val.put("coordinateSystemLink","");
		} else {
			Location location=new Location();
            URI property = factory.createURI(Constants.MA_NAMESPACE+"location");
            RepositoryResult<Statement> statements = connection.getStatements(resource,property,null,false);

            Facading facading = FacadingFactory.createFacading(connection);

			if(statements.hasNext()) {
				LocationFacade lf = facading.createFacade((Resource)statements.next().getObject(),LocationFacade.class);
				location.lat = lf.getLatitude();
				location.lng = lf.getLongitude();
                if(lf.getAltitude() != null) location.alt = 0.0;
			}
			prop_val.put("statusCode",200);
			prop_val.put("value",location.toString());
			prop_val.put("locationLink","");
			prop_val.put("locationLabel","");
			prop_val.put("longitude",location.lng);
			prop_val.put("latitude",location.lat);
            prop_val.put("altitude",location.alt == 0.0 ? 0 : location.alt);
			prop_val.put("coordinateSystemLabel","");
			prop_val.put("coordinateSystemLink","");
		}

		return prop_val;
	}

	private HashMap<String,Object> createRatingItem(URI resource, String value, String language, String sourceFormat, String fragmentIdentifier, String mappingType, RepositoryConnection connection, ValueFactory factory) throws RepositoryException {
		HashMap<String,Object> prop_val = new HashMap<String, Object>();
		prop_val.put("propertyName","rating");
		prop_val.put("language",language);
		prop_val.put("sourceFormat",sourceFormat);
		prop_val.put("fragmentIdentifier",fragmentIdentifier);
		prop_val.put("mappingType",mappingType);
		prop_val.put("ratingSystemLabel","higherBetter");
		prop_val.put("ratingSystemLink","");
		prop_val.put("statusCode",200);

		//get facade
        URI property = factory.createURI(Constants.MA_NAMESPACE+"rating");
        RepositoryResult<Statement> statements = connection.getStatements(resource,property,null,false);

        Facading facading = FacadingFactory.createFacading(connection);

        if(statements.hasNext()) {
			RatingFacade rf = facading.createFacade((Resource)statements.next().getObject(),RatingFacade.class);
			prop_val.put("value",String.valueOf(rf.getValue()));
			prop_val.put("ratingValue",rf.getValue());
			prop_val.put("minimum",rf.getMin());
			prop_val.put("maximum",rf.getMax());
		}

		return prop_val;
	}

	class Location {
		Double lat = 0.0;
		Double lng = 0.0;
        Double alt = 0.0;
		public String toString() {
		   return lng+" "+lat;
		}
	}
}
