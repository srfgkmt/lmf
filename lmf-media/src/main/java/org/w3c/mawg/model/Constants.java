/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.w3c.mawg.model;

import nu.xom.XPathContext;
import org.w3c.mawg.model.xpath.*;

import java.util.HashMap;
import java.util.Map;

/**
 * User: Thomas Kurz
 * Date: 09.03.12
 * Time: 13:14
 */
public class Constants {
	public static final String MA_NAMESPACE = "http://www.w3.org/ns/ma-ont#";
	public static final String DC_NAMESPACE = "http://purl.org/dc/elements/1.1/";
	public static final Map<String,String> DC_MAPPINGS = new HashMap<String , String>() {{
    	put("title",		"title");
    	put("creator",		"creator");
    	put("subject",		"keyword");
		put("language",		"language");
		put("description",	"description");
		put("publisher",	"publisher");
		put("contributor",	"contributor");
		put("date",			"date");
		put("type",			"genre");
		put("format",		"format");
		put("identifier",	"identifier");
		put("relation",		"relation");
		put("coverage",		"location");
		put("rights",		"copyright");
		put("source",		"collection");
	}};

	public static XPathContext ytContext = new XPathContext();
    static {
        ytContext.addNamespace("atom","http://www.w3.org/2005/Atom" );
        ytContext.addNamespace("media","http://search.yahoo.com/mrss/");
        ytContext.addNamespace("yt","http://gdata.youtube.com/schemas/2007");
        ytContext.addNamespace("gd", "http://schemas.google.com/g/2005");
        ytContext.addNamespace("georss","http://www.georss.org/georss");
        ytContext.addNamespace("gml", "http://www.opengis.net/gml");
    }

	public static final HashMap<String, XPathMapper> YT_MAPPINGS = new HashMap<String , XPathMapper>() {{
        put("title", new XPathStringMapper("rss/channel/item/title","title"));
		put("identifier", new XPathStringMapper("rss/channel/item/media:group/media:content/@url","identifier"));
		put("locator ", new XPathStringMapper("rss/channel/item/media:group/media:content/@url","locator"));
		put("date", new XPathStringMapper("rss/channel/item/yt:recorded","date"));
		put("description", new XPathStringMapper("rss/channel/item/media:group/media:description","description"));
		put("keyword", new XPathKeywordMapper("rss/channel/item/media:group/media:keywords"));
		put("genre", new XPathStringMapper("rss/channel/item/media:group/media:category","genre"));
		put("rating", new XPathRatingMapper("rss/channel/item/gd:rating/"));
		put("targetAudience", new XPathStringMapper("rss/channel/item/media:group/media:rating","targetAudience"));
		put("compression", new XPathStringMapper("rss/channel/item/media:group/media:content/@type","compression"));
		put("duration", new XPathStringMapper("rss/channel/item/media:group/media:content/@duration","duration"));
		put("format", new XPathStringMapper("rss/channel/item/media:group/media:content/@type","format"));
		put("publisher", new XPathStringMapper("rss/channel/item/media:group/media:credit[@role='uploader']","publisher"));
		put("location", new XPathLocationMapper("rss/channel/item/georss:where/gml:Point/gml:pos"));
    }};
	
	public static final HashMap<String,String> DC_PROPERTIES= new HashMap<String,String>() {{
		put("title","exact");
    	put("creator","exact");
    	put("keyword","exact");
		put("language","exact");
		put("description","exact");
		put("publisher","exact");
		put("contributor","exact");
		put("date","related");
		put("genre","exact");
		put("format","exact");
		put("identifier","exact");
		put("relation","exact");
		put("location","exact");
		put("copyright","related");
		put("collection","related");
	}};
	
	public static final HashMap<String,String> YT_PROPERTIES= new HashMap<String,String>() {{
		put("title","exact");
		put("identifier","more specific");
		put("locator","exact");
		put("date","exact");
		put("description","exact");
		put("keyword","exact");
		put("genre","exact");
		put("rating","");
		put("targetAudience","more specific");
		put("compression","exact");
		put("duration","exact");
		put("format","exact");
		put("publisher","exact");
		put("location","exact");
	}};
}
