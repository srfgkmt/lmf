/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.w3c.mawg.model.xpath;

import nu.xom.Nodes;
import org.apache.marmotta.commons.sesame.facading.FacadingFactory;
import org.apache.marmotta.commons.sesame.facading.api.Facading;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.w3c.mawg.model.Constants;
import org.w3c.mawg.model.LocationFacade;

import java.util.UUID;

/**
 * User: Thomas Kurz
 * Date: 09.03.12
 * Time: 18:28
 */
public class XPathLocationMapper implements XPathMapper{

	String xpath;
	public XPathLocationMapper(String xpath) {
		this.xpath = xpath;
	}

	@Override
    public void toTriples(URI resource, nu.xom.Document document, RepositoryConnection connection, ValueFactory factory, ConfigurationService configurationService) throws RepositoryException {
		Nodes nodes = document.query(xpath, Constants.ytContext);

        Facading facading = FacadingFactory.createFacading(connection);
        URI property = factory.createURI(Constants.MA_NAMESPACE+"location");

		for(int i=0; i<nodes.size();i++) {
			String []values = nodes.get(i).getValue().split(" ");
            LocationFacade loc = facading.createFacade(configurationService.getBaseUri()+"resource/"+ UUID.randomUUID(),LocationFacade.class);
			loc.setLongitude(Double.parseDouble(values[0]));
			loc.setLatitude(Double.parseDouble(values[1]));
            connection.add(resource,property,loc.getDelegate());
		}
	}
}
