/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function _out(value) {
    console.log(value);
}

var TEST = {
    container : "#container",
    container2 : "#all",
    assertEquals : {
        TEXT : function(name,a,b) {
            _out("test "+name+" run");

            var div = $("<div></div>").append("<h3 id='test_"+name+"'>Test "+name+"</h3>");
            $(TEST.container).append(div);

            var table = $("<table class='test'></table>");

            var result = $("<tr></tr>").appendTo(table);
            table.append("<tr><td class='header' style='width:50%'>original</td><td class='header'>response</td></tr>")
            var output = $("<tr></tr>").appendTo(table);

            function syntaxHighlight(text) {
                return text;
            }

            output.append($("<td></td>").append($("<pre></pre>").text(syntaxHighlight(a))));
            output.append($("<td></td>").append($("<pre></pre>").text(syntaxHighlight(b))));

                result.append($("<td class='ok' style='background-color:orange' colspan='2'></td>").text("APPROVE MANUALLY"));
                _out("test "+name+" successful");
                $(TEST.container2).append("<li><a style='color:orange' href='#test_"+name+"'>Test '"+name+"' must be approved manually</a></li>");

            div.append(table);
        },
        JSON :  function assertEqualsJson(name,d2,d1,remark) {
            _out("test "+name+" run");

            var div = $("<div></div>").append("<h3 id='test_"+name+"'>Test "+name+"</h3>");
            if(remark) div.append("<h4>"+remark+"</h4>");
            $(TEST.container).append(div);

            var table = $("<table class='test'></table>");

            var result = $("<tr></tr>").appendTo(table);
            table.append("<tr><td class='header' style='width:50%'>original</td><td class='header'>response</td></tr>")
            var output = $("<tr></tr>").appendTo(table);

            function syntaxHighlight(json) {
                if (typeof json != 'string') {
                    json = JSON.stringify(json, undefined, 2);
                }
                json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                    var cls = 'number';
                    if (/^"/.test(match)) {
                        if (/:$/.test(match)) {
                            cls = 'key';
                        } else {
                            cls = 'string';
                        }
                    } else if (/true|false/.test(match)) {
                        cls = 'boolean';
                    } else if (/null/.test(match)) {
                        cls = 'null';
                    }
                    return '<span class="' + cls + '">' + match + '</span>';
                });
            }

            /*
             Original script title: "Object.identical.js"; version 1.12
             Copyright (c) 2011, Chris O'Brien, prettycode.org
             http://github.com/prettycode/Object.identical.js
             */
            function equals(a, b, sortArrays) {

                function sort(object) {
                    if (sortArrays === true && Array.isArray(object)) {
                        return object.sort();
                    }
                    else if (typeof object !== "object" || object === null) {
                        return object;
                    }

                    return Object.keys(object).sort().map(function(key) {
                        return {
                            key: key,
                            value: sort(object[key])
                        };
                    });
                }

                var x = sort(a);
                var y = sort(b);

                output.append($("<td></td>").append($("<pre></pre>").html(syntaxHighlight(x))));
                output.append($("<td></td>").append($("<pre></pre>").html(syntaxHighlight(y))));

                return JSON.stringify(x) === JSON.stringify(y);
            };

            if(equals(JSON.parse(d1),JSON.parse(d2))){
                result.append($("<td class='ok' colspan='2'></td>").text("TEST SUCCESSFUL"));
                _out("test "+name+" successful");
                $(TEST.container2).append("<li><a style='color:green' href='#test_"+name+"'>Test '"+name+"' was successfull</a></li>");
            } else {
                result.append($("<td class='fail' colspan='2'></td>").text("TEST FAILED"));
                _out("test "+name+" failed");
                $(TEST.container2).append("<li><a style='color:red' href='#test_"+name+"'>Test '"+name+"' failed</a></li>");
            }

            div.append(table);
        }
    }
}

var HELPER = {

    createRandomURI : function () {
        return "http://my.mediaresource.eu/resource/"+Math.floor(Math.random()*1000000);
    },
    createTestName : function(url) {
        return url.substring("http://www.w3.org/2008/WebVideo/Annotations/drafts/API/tests/".length,url.lastIndexOf("."));
    }
}

