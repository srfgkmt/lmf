/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var MediaResourceOptions = {
    serviceURL : undefined
}

function MetadataSource(url,format) {
    this.url = url;
    this.format = format;
}

function MediaResource() {}


/**
 * returns the mode that is supported by the object
 * @returns 1 (asynchronous)
 */
MediaResource.prototype.getSupportedModes = function() {

    return 1;

}

/**
 * creates a media resource (synchronous)
 * @param mediaResource
 * @param metadataSources
 * @param mode
 * @returns {SyncMediaResource}
 */
MediaResource.prototype.createMediaResource = function(mediaResource, metadataSources, mode) {

    var resource;

    resource = new SyncMediaResource(mediaResource, metadataSources);

    return resource;
};


function SyncMediaResource(mediaResource, metadataSources) {

    var self = this;

    this._httpClient = new HTTP_Client(MediaResourceOptions.serviceURL);

    this.mediaResource = mediaResource;

    self._httpClient.post("create", {uri: self.mediaResource});

    for (var i in metadataSources) {

        self._httpClient.put("update", {uri: self.mediaResource, metadata: metadataSources[i].url, format: metadataSources[i].format});

    }
}

SyncMediaResource.prototype = new MediaResource();


SyncMediaResource.prototype.getMediaProperty = function(propertyNames, fragment, sourceFormat, language) {

    var self = this;

    if(fragment != null) throw new Error("fragment is not supported");

    if(language != null) throw new Error("language is not supported");

    return self._httpClient.get("read",{uri:self.mediaResource,properties:propertyNames,format:sourceFormat});

};


SyncMediaResource.prototype.getOriginalMetadata = function(sourceFormat) {

    var self = this;

    return self._httpClient.get("original",{uri:self.mediaResource,format:sourceFormat});

};












