/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * HTTP Client based on XMLHTTPRequest Object, allows RESTful interaction (GET;PUT;POST;DELETE)
 * @param url
 */
function HTTP_Client(url) {

    function createRequest() {
        var request = null;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            request = new ActiveXObject("Microsoft.XMLHTTP");
        } else {
            throw "request object can not be created"
        }
        return request;
    }

    //build a query param string
    function buildQueryParms(params) {
        if(params==null||params.length==0) return "";
        var s="?"
        for(var prop in params) {
            s+=prop+"="+encodeURIComponent(params[prop])+"&";
        } return s.substring(0,s.length-1);
    }

    //fire request, the method takes a callback object which can contain several callback functions for different HTTP Response codes
    function doRequest(method,path,queryParams,data,mimetype) {
        mimetype = mimetype ||  "application/json";
        var _url = url+path+buildQueryParms(queryParams);
        var request = createRequest();
        request.open(method, _url, false);
        if(method=="PUT"||method=="POST")request.setRequestHeader("Content-Type",mimetype);
        if(method=="GET")request.setRequestHeader("Accept",mimetype);
        request.send( data );

        if (request.status == 200) {
            return request.responseText;
        } else {
            throw "Status:" + request.status + ",Text:" + request.responseText;
        }
    }

    this.get = function(path,queryParams,data,mimetype) {
        return doRequest("GET",path,queryParams,data,mimetype);
    }

    this.put = function(path,queryParams,data,mimetype) {
        return doRequest("PUT",path,queryParams,data,mimetype);
    }

    this.post = function(path,queryParams,data,mimetype) {
        return doRequest("POST",path,queryParams,data,mimetype);
    }

    this.delete = function(path,queryParams,data,mimetype) {
        return doRequest("DELETE",path,queryParams,data,mimetype);
    }
}

var HTTP = new HTTP_Client("");