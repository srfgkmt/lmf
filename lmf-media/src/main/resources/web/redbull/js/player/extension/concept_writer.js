/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function ConceptWriter(object) {

    //default values
    this.endpoint = "http://localhost:8080/KiWi2/sparql/select";
    this.loader = "img/ajax-loader.gif";
    this.show_rectangles = true;

    //TODO remove
    this.video=$("#video");

    //initializer
    this.init = function(object) {
       if(object.target)this.target = object.target; else console.log("no 'target' defined for ConcepWriter");
       if(object.video)this.video = object.video; else console.log("no 'video' element defined for ConcepWriter");
       if(object.resource)this.resource = object.resource; else console.log("no 'resource' defined for ConcepWriter");
       if(object.endpoint)this.endpoint = object.endpoint; else console.log("using default endpoint for ConcepWriter");
       if(object.loader)this.loader = object.loader; else console.log("using default loader image for ConcepWriter");
       this.concept_list = [];
       if(this.resource.indexOf("#")>0) {
        this.resource = this.resource.substring(0,this.resource.indexOf("#"));
       }

       this.canvas_manger.init(this.video);
       this.getValues();
    }

    this.canvas_manger={
        parent:undefined,
        video:undefined,
        boxes:undefined,
        emphasized:undefined,
        init:function(video_div){
            parent=video_div.parent();
            video=video_div.get(0);
            boxes=[];
            emphasized=false;
        },
        paint:function(region,url,title) {
            var rectangle = $("<div class='video_overlay' style='cursor:pointer;position:absolute;z-index:1000'/>");
            var setPosition = function(div,x,y,w,h) {
                div.css("width",w+"px");
                div.css("height",h+"px");
                div.css("top",y+"px");
                div.css("left",x+"px");
            }
            setPosition(rectangle,region.x,region.y,region.w,region.h);
            rectangle.attr("id",hex_md5("overlay::"+url));
            parent.append(rectangle);

            rectangle.bind('click',function(){
                if(emphasized) {
                    emphasized=false;
                    //remove boxes
                    for(var i=0; i<boxes.length;i++){
                        boxes[i].remove();
                    }
                    video.play();
                } else {
                    emphasized=true;
                    //display boxes
                    video.pause();
                    var top = $("<div class='video_overlay_border' style='position:absolute;z-index:1001'/>");
                    var right = $("<div class='video_overlay_border' style='position:absolute;z-index:1001'/>");
                    var bottom = $("<div class='video_overlay_border' style='position:absolute;z-index:1001'/>");
                    var left = $("<div class='video_overlay_border' style='position:absolute;z-index:1001'/>");
                    setPosition(top,0,0,parent.width(),region.y);
                    setPosition(right,region.w+region.x+2,region.y,parent.width()-(region.w+region.x+2),region.h+2);
                    setPosition(bottom,0,region.y+region.h+2,parent.width(),$(video).height()-(region.y+region.h+2));
                    setPosition(left,0,region.y,region.x,region.h+2);
                    boxes.push(top);
                    boxes.push(right);
                    boxes.push(bottom);
                    boxes.push(left);
                    parent.append(top);
                    parent.append(right);
                    parent.append(bottom);
                    parent.append(left);
                }

                $("#"+hex_md5(url)).find("h2").click();
            });
            rectangle.show();
        },
        remove:function(url){
            for(var i=0; i<boxes.length;i++){
                boxes[i].remove();
            }
            $("#"+hex_md5("overlay::"+url)).remove();
        }
    }

    //get transcription data via sparql
    this.getValues = function() {
        var self = this;
        //locations
        var sparql_locations =  "SELECT ?title ?fragment ?url  WHERE {"+
                                "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#hasFragment> ?fragment."+
                                "   ?a <http://www.openannotation.org/ns/hasTarget> ?fragment."+
                                "   ?a <http://www.openannotation.org/ns/hasBody> ?url."+
                                "   ?url <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://schema.org/Place>."+
                                "   ?url <http://www.w3.org/2000/01/rdf-schema#label> ?title."+
                                "   FILTER(lang(?title) = \"en\")."+
                                "}";
        $.getJSON(this.endpoint+"?query="+encodeURIComponent(sparql_locations)+"&output=json",function(data){
            self.parseValues(data,"location");
        });

        var sparql_persons =    "SELECT ?title ?fragment ?url  WHERE {"+
                                "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#hasFragment> ?fragment."+
                                "   ?a <http://www.openannotation.org/ns/hasTarget> ?fragment."+
                                "   ?a <http://www.openannotation.org/ns/hasBody> ?url."+
                                "   ?url <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://xmlns.com/foaf/0.1/Person>."+
                                "   ?url <http://xmlns.com/foaf/0.1/name> ?title."+
                                "}";
        $.getJSON(this.endpoint+"?query="+encodeURIComponent(sparql_persons)+"&output=json",function(data){
            self.parseValues(data,"person");
        });
        var sparql_concepts =   "SELECT ?title ?fragment ?url  WHERE {"+
                                "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#hasFragment> ?fragment."+
                                "   ?a <http://www.openannotation.org/ns/hasTarget> ?fragment."+
                                "   ?a <http://www.openannotation.org/ns/hasBody> ?url."+
                                "   ?url <http://www.w3.org/2000/01/rdf-schema#label> ?title."+
                                "   FILTER(lang(?title) = \"en\")."+
                                "}";
        $.getJSON(this.endpoint+"?query="+encodeURIComponent(sparql_concepts)+"&output=json",function(data){
            self.parseValues(data,"concept");
        });
    }

    //parse transcription data and add to transcription_list
    this.parseValues = function(data,type) {
        for(var i=0; i<data.results.bindings.length;i++) {
            var data_object = data.results.bindings[i];
            var concept = {active:false,overlay_active:false};
            concept.hash = utils_fragments_url_HashParser(data_object.fragment.value);
            concept.title = data_object.title.value;
            concept.url = data_object.url.value;
            concept.type = type;
            concept.md5 = hex_md5(data_object.url.value);
            this.concept_list.push(concept);
        }
    }

    //maybe in use later
    this.reload = function() {
        this.getValues();
    }

    this.rendering = false;
    //MANDATORY
    this.listener = function(object) {
        if (this.rendering) return;
        this.rendering = true;

        //for all transcription fragments
        for (var i = 0; i < this.concept_list.length; i++) {
            var o = this.concept_list[i];
            //if it should be displayed
            if(o.hash.time.start<object.currentTime && o.hash.time.end>object.currentTime) {
                //show it if its new
                if(!o.active && !$("#"+o.md5).length) {
                    o.active = true;
                    this.target.append(this.create(o,this.endpoint));
                }

                //append rectangle
                if(this.show_rectangles && o.hash.region!=undefined && !o.overlay_active){
                    o.overlay_active = true;
                    this.canvas_manger.paint(o.hash.region,o.url,o.title);
                }
            } else {
                //delete it
                if(o.active && !$("#"+o.md5).attr("open")) {
                    o.active = false;
                    $("#"+o.md5).remove();
                }
                //remove rectangle
                if(this.show_rectangles && o.hash.region!=undefined && o.overlay_active){
                    o.overlay_active = false;
                    this.canvas_manger.remove(o.url);
                }
            }
        }

        this.rendering = false;
    }

    this.create = function(o,endpoint) {
        var self = this;

        function render(data,div) {
            var res = data.results.bindings[0];
            div.html("");
            div.append("<img src='"+res.img.value+"' />");
            div.append(res.abstract.value);
            div.append("<a target='_blank' href='"+res.link.value+"' class='more'>more</a>");
            div.append("<div style='clear:left;'></div>");
            div.slideDown();
        }

        function get_person(o,div,loader) {

           var sparql = "SELECT ?title ?abstract ?img ?link WHERE {"+
                        "   <"+o.url+"> <http://xmlns.com/foaf/0.1/name> ?title."+
                        "   <"+o.url+"> <http://purl.org/dc/elements/1.1/description> ?abstract."+
                        "   <"+o.url+"> <http://xmlns.com/foaf/0.1/img> ?img."+
                        "   <"+o.url+"> <http://xmlns.com/foaf/0.1/homepage> ?link."+
                        "}";
           $.getJSON(endpoint+"?query="+encodeURIComponent(sparql)+"&output=json",function(data){
               loader.remove();
               render(data,div);
           });
        }

        function get_location(o,div,loader) {
            var sparql = "SELECT ?title ?abstract ?img ?link WHERE {"+
                        "   <"+o.url+"> <http://www.w3.org/2000/01/rdf-schema#label> ?title."+
                        "   <"+o.url+"> <http://dbpedia.org/ontology/abstract> ?abstract."+
                        "   <"+o.url+"> <http://dbpedia.org/ontology/thumbnail> ?img."+
                        "   <"+o.url+"> <http://xmlns.com/foaf/0.1/page> ?link." +
                        "   FILTER(lang(?title) = \"en\" && lang(?abstract) = \"en\")."+
                        "}";
            $.getJSON(endpoint+"?query="+encodeURIComponent(sparql)+"&output=json",function(data){
                 loader.remove();
                 render(data,div);
            });
        }

        function get_concept(o,div,loader) {
            var sparql = "SELECT ?title ?abstract ?img ?link WHERE {"+
                "   <"+o.url+"> <http://www.w3.org/2000/01/rdf-schema#label> ?title."+
                "   <"+o.url+"> <http://dbpedia.org/ontology/abstract> ?abstract."+
                "   <"+o.url+"> <http://dbpedia.org/ontology/thumbnail> ?img."+
                "   <"+o.url+"> <http://xmlns.com/foaf/0.1/isPrimaryTopicOf> ?link." +
                "   FILTER(lang(?title) = \"en\" && lang(?abstract) = \"en\")."+
                "}";
            $.getJSON(endpoint+"?query="+encodeURIComponent(sparql)+"&output=json",function(data){
                loader.remove();
                render(data,div);
            });
        }

        function write(o) {
           var concept = $("<div class='concept' id='"+o.md5+"'></div>");

           var header = $("<h2 class='"+o.type+"_concept_header'></h2>").text(o.title).bind('click',function(){
              //test if div is open
              if(!$(this).parent().attr("open")) {
                  $(this).parent().attr("open",true);
                  var loader = $("<img class='concept_header_loader' src='"+self.loader+"'/>");
                  $(this).append(loader);
                  switch(o.type) {
                    case "person":get_person(o,$(this).parent().find('div.content'),loader);break;
                    case "location":get_location(o,$(this).parent().find('div.content'),loader);break;
                    case "concept":get_concept(o,$(this).parent().find('div.content'),loader);break;
                  }
              } else {
                  $(this).parent().find('div.content').slideUp(function(){
                     $(this).parent().attr("open",false);
                  });
              }
           });
           concept.append(header);
           concept.append("<div class='content' style='display:none;'/>");
           return concept;
        };

        return write(o);
    };

    //init writer
    this.init(object);

    return this;
}