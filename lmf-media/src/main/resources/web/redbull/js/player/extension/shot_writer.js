/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function ShotWriter(object) {

    //default values
    this.endpoint = "http://localhost:8080/KiWi2/sparql/select";

    //initializer
    this.init = function(object) {
       if(object.target)this.target = object.target; else console.log("no 'target' defined for TranscriptionWriter");
       if(object.resource)this.resource = object.resource; else console.log("no 'resource' defined for TranscriptionWriter");
       if(object.endpoint)this.endpoint = object.endpoint; else console.log("using default endpoint for TranscriptionWriter");
       this.transcription_list = [];
       if(this.resource.indexOf("#")>0) {
        this.resource = this.resource.substring(0,this.resource.indexOf("#"));
       }
       this.getValues();
    }

    //get transcription data via sparql
    this.getValues = function() {
        var self = this;
        var sparql =    "SELECT ?url ?text WHERE {" +
                        "   <"+this.resource+"> <http://www.w3.org/ns/ma-ont#hasFragment> ?url." +
                        "   ?a <http://www.openannotation.org/ns/hasTarget> ?url."+
                        "   ?a <http://www.openannotation.org/ns/hasBody> ?body."+
                        "   ?body <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://redbullcontentpool.com/ontology/Shot>." +
                        "   ?body <http://redbullcontentpool.com/ontology/text> ?text"+
                        "}";
        $.getJSON(this.endpoint+"?query="+encodeURIComponent(sparql)+"&output=json",function(data){
            self.parseValues(data);
        });
    }

    //parse transcription data and add to transcription_list
    this.parseValues = function(data) {
        for(var i=0; i<data.results.bindings.length;i++) {
            var data_object = data.results.bindings[i];
            var transcription = {active:false};
            transcription.time = utils_fragments_url_HashTimeParser(data_object.url.value);
            transcription.text = data_object.text.value;
            transcription.url = data_object.url.value;
            transcription.md5 = hex_md5(data_object.url.value);
            this.transcription_list.push(transcription);
        }
    }

    //maybe in use later
    this.reload = function() {
        this.getValues();
    }

    this.rendering = false;
    //MANDATORY
    this.listener = function(object) {
        if (this.rendering) return;
        this.rendering = true;

        //for all transcription fragments
        for (var i = 0; i < this.transcription_list.length; i++) {
            var o = this.transcription_list[i];
            //if it should be displayed
            if(o.time.start<object.currentTime && o.time.end>object.currentTime) {
                //show it if its new
                if(!o.active) {
                    o.active = true;
                    this.target.append("<div id='"+o.md5+"'>"+o.text+"</div>");
                }
            } else {
                //delete it
                if(o.active) {
                    o.active = false;
                    $("#"+o.md5).remove();
                }
            }
        }

        this.rendering = false;
    }

    //init writer
    this.init(object);

    return this;
}