/*
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
jQuery.fn.player = function(object) {
    //parameters
    var video = $(this).get(0);
    var interval = 100;

    //construct
    if (object) {
        if (object.interval) interval = object.interval;
    }

    //trigger manager
    function Manager() {
        this.editor_mode = false;
        this.listeners = [];
        this.add = function(listener) {
            this.listeners.push(listener);
        }
        this.trigger = function(object) {
            for (var i = 0; i < this.listeners.length; i++) {
                this.listeners[i].listener(object);
            }
        }
        this.setMode = function(editor_mode) {
            this.editor_mode = editor_mode;
            if(video.paused) {
                manager.trigger({currentTime:current,editor_mode:manager.editor_mode});
            } else {
               video.pause();
            }
        }
    }

    var manager = new Manager();

    //run circle
    var current = -1;

    function run() {
        if (current != video.currentTime) {
            current = video.currentTime;
            manager.trigger({currentTime:current,editor_mode:manager.editor_mode});
        }
        setTimeout(function() {
            run();
        }, interval);
    }

    run();

    return manager;
}
