curl -L -i -H "Content-Type: image/jpeg; rel=content" -X POST http://localhost:8080/LMF/resource/hans_meier.jpg
curl -L -i -H "Content-Type: image/jpeg; rel=content" -X PUT --data-binary @hans_meier.jpg http://localhost:8080/LMF/resource/hans_meier.jpg

curl -L -i -H "Content-Type: image/jpeg; rel=content" -X POST http://localhost:8080/LMF/resource/sepp_huber.jpg
curl -L -i -H "Content-Type: image/jpeg; rel=content" -X PUT --data-binary @sepp_huber.jpg http://localhost:8080/LMF/resource/sepp_huber.jpg

curl -L -i -H "Content-Type: image/jpeg; rel=content" -X POST http://localhost:8080/LMF/resource/anna_schmidt.jpg
curl -L -i -H "Content-Type: image/jpeg; rel=content" -X PUT --data-binary @anna_schmidt.jpg http://localhost:8080/LMF/resource/anna_schmidt.jpg
