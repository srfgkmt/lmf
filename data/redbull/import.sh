#!/bin/bash
URL=http://demo4.newmedialab.at/redbull/

echo $URL

#Search Program
#curl -i -H "Content-Type: text/plain" --user admin:pass123 -X DELETE ${URL}solr/cores/redbull
curl -i -H "Content-Type: text/plain" --user admin:pass123 -X POST -d @data/search.txt ${URL}solr/cores/redbull

#Reasoning Program
curl -i -H "Content-Type: text/plain" --user admin:pass123 -X DELETE ${URL}reasoner/program/redbull
curl -i -H "Content-Type: text/plain" --user admin:pass123 -X POST -d @data/reasoner.kwql ${URL}reasoner/program/redbull

#Generic
curl -i -H "Content-Type: application/rdf+xml" --user admin:pass123 -X POST -d @data/Generic/import.xml ${URL}import/upload

### FILE MI201003310017 ###
curl -i -H "Content-Type: application/rdf+xml" --user admin:pass123 -X POST -d @data/MI201003310017_Metadata/import.xml ${URL}import/upload

### FILE MI201003310018 ###
curl -i -H "Content-Type: application/rdf+xml" --user admin:pass123 -X POST -d @data/MI201003310018_Metadata/import.xml ${URL}import/upload

