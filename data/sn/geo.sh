#!/bin/bash

URL=${LMF_URL:-"http://localhost:8080/LMF"}
URL=${URL%/}
ROOT=${IMPORT_SRC:-"/home/jakob/Downloads/sn"}

#important config settings
declare -A CONF
CONF['resources.browsercache.enabled']="false"
CONF['resources.servercache.enabled']="false"
CONF['solr.enabled']="true"
CONF['solr.omit_cached']="false"
CONF['solr.local_only']="false"

for i in dc skos rdf; do
	curl -i -X DELETE "${URL}/solr/cores/$i" && echo
done

for key in "${!CONF[@]}"; do
	val="${CONF["$key"]}"
	echo "Setting $key = $val"
	curl -i -X POST -H "Content-Type: application/json" -d"[\"$val\"]" ${URL}/config/data/$key && echo
done

# LMF-Cache blacklists
curl -i -H "Content-Type: text/plain" -X POST "${URL}/cache/endpoint?name=Mediawiki&prefix=~http%3A%2F%2F%5B%5E%2F%5D*wiki%5Bpm%5Dedia%5C.org&kind=NONE&expiry=86400&endpoint=&mimetype=" && echo
curl -i -H "Content-Type: text/plain" -X POST "${URL}/cache/endpoint?name=SN%20Salzburger%20Nachrichten&prefix=~http%3A%2F%2F%5B%5E%2F%5D*salzburg%5C.com&kind=NONE&expiry=86400&endpoint=&mimetype=" && echo

#LMF-GEO
curl -i -H "Content-Type: text/plain" -X POST "${URL}/cache/endpoint?name=LMF%20GeoNames&prefix=http%3A%2F%2Fsws.geonames.org&kind=CACHE&expiry=86400&endpoint=http%3A%2F%2Flmf.newmedialab.at%2FLMF%2Fresource%3Furi%3D%7Buri%7D=&mimetype=application%2Frdf%2Bxml" && echo

curl -i -H "Content-Type: text/n3" -X POST -d @skos-snressorts.n3 ${URL}/import/upload && echo
curl -i -H "Content-Type: text/n3" -X POST -d @sn-model-ontology.n3 ${URL}/import/upload && echo

geo="${IMPORT_SRC:-${ROOT}/articles/2011_sel}"
curl -i -H "Content-Type: application/snxml+xml" -X POST "${URL}/import/external?url=file:${geo}" && echo
