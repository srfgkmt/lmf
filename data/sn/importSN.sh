#!/bin/bash
URL=${LMF_URL:-"http://localhost:8080/LMF/"}
URL=${URL%/}
ROOT=${IMPORT_SRC:-"/home/jakob/Downloads/sn"}

# import SNXML Articles
[ -d "$ROOT" ] && for f in $(find "$ROOT/articles" -type d -name '2011_sel*'); do
	[ -e "$f" ] && curl -i -H "Content-Type: application/snxml+xml" -X POST "${URL}/import/external?url=file:${f}" && echo
done

# import some comunity blogs
[ -r "$ROOT/blogs/com-blog/urls.txt" ] && cat "$ROOT/blogs/com-blog/urls.txt" | while read u; do
	curl -i -H "Content-Type: application/snrss+xml" -X POST "${URL}/import/external?url=${u}" && echo
done

# import some fotoblogs
[ -r "$ROOT/blogs/foto-blog/urls.txt" ] && cat "$ROOT/blogs/foto-blog/urls.txt" | while read u; do
        curl -i -H "Content-Type: application/snrss+xml" -X POST "${URL}/import/external?url=${u}" && echo
done

# import some videos
curl -i -H "Content-Type: application/x-snvideo" -X POST "${URL}/import/external?url=http://www.salzburg.com/nwas/video/" && echo
