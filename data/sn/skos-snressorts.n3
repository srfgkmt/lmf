@prefix sn : <http://lmf.salzburg.com/news-ns/> .
@prefix snc : <http://lmf.salzburg.com/news-ns/concepts/> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix fcp: <http://www.newmedialab.at/fcp/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .

snc:NewsRessorts rdf:type skos:ConceptScheme ;
    skos:prefLabel  "SN Ressort Thesaurus"@de .

snc:Ressort  rdfs:subClassOf skos:Concept .
sn:inRessort  rdfs:domain  snc:NewsItem ;
    rdfs:range snc:Ressort .

sn:Sport    skos:prefLabel    "Sport", "Sport"@de, "Sports"@en ;
    skos:hiddenLabel    "sport", "weitere-sportarten", "hintergrund", "news", "topnews" ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Tennis   skos:prefLabel    "Tennis", "Tennis"@de, "Tennis"@en ;
    skos:hiddenLabel    "tennis" ;
    skos:narrower    sn:Sport ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Eishockey   skos:prefLabel    "Eishockey", "Eishockey"@de, "Ice Hockey"@en ;
    skos:hiddenLabel    "eishockey" ;
    skos:narrower    sn:Sport ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Motorsport   skos:prefLabel    "Motorsport", "Motorsport"@de, "Motorsports"@en;
    skos:hiddenLabel    "motorsport" ;
    skos:narrower    sn:Sport ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .
    
sn:Formel1  skos:prefLabel    "Formel 1", "Formel 1"@de, "Formula One"@en ;
    skos:hiddenLabel    "formel1" ;
    skos:narrower    sn:Motorsport ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .
    
sn:Politik   skos:prefLabel    "Politik", "Politik"@de, "Politics"@en ;
    skos:hiddenLabel    "dreier" ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .
    
sn:Innenpolitik skos:prefLabel    "Innenpolitik", "Innenpolitik"@de, "Domestic Politics"@en ;
    skos:hiddenLabel    "innen", "innenpolitik" ;
    skos:narrower    sn:Politik ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Aussenpolitik skos:prefLabel    "Außenpolitik", "Außenpolitik"@de, "Foreign Policy"@en ;
    skos:hiddenLabel    "aussen" ;
    skos:narrower    sn:Politik ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Weltpolitik skos:prefLabel    "Weltpolitik", "Weltpolitik"@de, "Global Politics"@en ;
    skos:hiddenLabel    "weltpolitik" ;
    skos:narrower    sn:Politik ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Chronik  skos:prefLabel    "Chronik", "Chronik"@de, "Chronicle"@en ;
    skos:hiddenLabel    "chronik", "gericht", "pano", "tourismus" ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Salzburg skos:prefLabel    "Salzburg" ;
    skos:hiddenLabel    "salzburg", "salzb", "erweiterung", "mensch" ;
    skos:narrower    sn:Chronik ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Pinzgau skos:prefLabel    "Pinzgau" ;
    skos:hiddenLabel    "pinzgau", "pinzgauer nachrichten" ;
    skos:narrower    sn:Salzburg ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Lungau skos:prefLabel    "Lungau" ;
    skos:hiddenLabel    "lungau", "lungauer nachrichten" ;
    skos:narrower    sn:Salzburg ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Kultur   skos:prefLabel    "Kultur" ;
    skos:hiddenLabel    "kultur", "kunst" ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .
    
sn:Wirtschaft   skos:prefLabel    "Wirtschaft", "Wirtschaft"@de, "Economy"@en ;
    skos:hiddenLabel    "wirtschaft", "wirtsch" ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Tennengau skos:prefLabel    "Tennengau" ;
    skos:hiddenLabel    "tennengau", "tennengauer nachrichten" ;
    skos:narrower    sn:Salzburg ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:TMobileLiga  skos:prefLabel    "T-Mobile Liga" ;
    skos:hiddenLabel    "t-mobile-liga", "bund" ;
    skos:narrower    sn:Fussball ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .
    
sn:Fussball skos:prefLabel    "Fußball", "Fußball"@de, "Soccer"@en ;
    skos:hiddenLabel    "fussball", "fußball" ;
    skos:narrower    sn:Sport ;    
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Wintersport  skos:prefLabel    "Wintersport", "Wintersport"@de, "Winter Sports"@en ;
    skos:hiddenLabel    "wintersport" ;
    skos:narrower    sn:Sport ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Flachgau skos:prefLabel    "Flachgau";
    skos:hiddenLabel    "flachgau", "flachgauer nachrichten" ;
    skos:narrower    sn:Salzburg ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .
    
sn:Pongau skos:prefLabel    "Pongau";
    skos:hiddenLabel    "pongau", "pongauer nachrichten" ;
    skos:narrower    sn:Salzburg ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:StadtSalzburg skos:prefLabel    "Stadt Salzburg", "Stadt Salzburg"@de, "Salzburg City"@en;
    skos:hiddenLabel    "stadt salzburg", "thema", "stadt nachrichten" ;
    skos:narrower    sn:Salzburg ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

sn:Beilagen skos:prefLabel  "Beilagen" ;
    skos:hiddenLabel    "beilagen" ;
    skos:inScheme   snc:NewsRessorts ;
    rdf:type    snc:Ressort .

