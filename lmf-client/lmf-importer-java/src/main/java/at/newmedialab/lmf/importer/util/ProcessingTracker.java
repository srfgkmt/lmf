/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.importer.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Processing tracker
 * 
 * @author Sergio Fernández
 * @author Rupert Westenthaler
 *
 */
public class ProcessingTracker<T> {
    
    private static final Logger log = LoggerFactory.getLogger(ProcessingTracker.class);
    
    /**
     * The number of completed items (including failed)
     */
    private long completed;
    
    /**
     * Global timing information
     */
    private long processingTime;
    private long importingStartTime = -1;
    private int maxRegistered = 100;
    private long[] minMaxProcessingTimes = new long[]{Long.MAX_VALUE,-1};
    
    /**
     * Timing information for the last chunk
     */
    private int chunkSize = 1000;
    private long chunkStart;
    private long chunkProcessing;
    private long[] chunkMinMax = new long[]{Long.MAX_VALUE,-1};
    
    /**
     * The {@link System#currentTimeMillis()} when the last Job completed
     */
    private long currentTime;
    
    /**
     * Registered but not yet completed jobs. Used to limit the number of
     * registered Jobs to avoid out-of-memory problems if adding of jobs is
     * faster than processing.
     */
    private Set<ProcessingJob<? extends T>> registered = new HashSet<ProcessingJob<? extends T>>();
    
    /**
     * Keeps the {@link ProcessingJob#getItem()}s of failed jobs
     */
    private List<T> failed = Collections.synchronizedList(new ArrayList<T>());
    
    /**
     * Service used for async processing of {@link #register(ProcessingJob) registered jobs}.
     */
    private ExecutorService executorService;

    public ProcessingTracker(ExecutorService executorService) {
        this.executorService = executorService;
    }
    
    /**
     * Ensures that only {@link #maxRegistered} or less Files are 
     * registered.
     * @param file
     */
    public void register(ProcessingJob<? extends T> job) {
        synchronized (registered) {
            while(registered.size() >= maxRegistered){
                try {
                    registered.wait();
                } catch (InterruptedException e) {
                    //interrupped
                }
            }
            registered.add(job);
        }
        if (importingStartTime < 0){
            importingStartTime = System.currentTimeMillis();
        }
        job.setTracker(this);
        executorService.execute(job);
    }
    
    void completed(ProcessingJob<? extends T> job) {
        synchronized (registered) {
            if (registered.remove(job)){
                if(!job.hasSucceeded()){
                    failed.add(job.getItem());
                }
                completed++;
                currentTime = System.currentTimeMillis();
                long time = job.getProcessingTime();
                processingTime = processingTime+time;
                chunkProcessing = chunkProcessing+time;
                if(time < minMaxProcessingTimes[0]){
                    minMaxProcessingTimes[0] = time;
                }
                if(time > minMaxProcessingTimes[1]){
                    minMaxProcessingTimes[1] = time;
                }
                if(time < chunkMinMax[0]){
                    chunkMinMax[0] = time;
                }
                if(time > chunkMinMax[1]){
                    chunkMinMax[1] = time;
                }
                if(completed > 0 && completed%chunkSize == 0){
                    logChunk();
                }
                registered.notifyAll();
            }
        }
    }
    
    /**
     * Internally used to log statistics after {@link #chunkSize} processed
     * {@link ProcessingJob}s
     */
    private void logChunk() {
        if(chunkStart < 0) {
            chunkStart = importingStartTime;
        }
        double chunkTime = currentTime-chunkStart;
        chunkStart = currentTime;
        log.info("  > " + completed + " completed :: chunk in " + Math.round(chunkTime/1000.0) + "sec (" +
        		chunkTime/chunkSize + "ms/item | avr:" + (double)chunkProcessing/(double)chunkSize + "ms | min:" +
        		chunkMinMax[0] + "ms | max:" + chunkMinMax[1] + "ms)");
        chunkMinMax[0] = Long.MAX_VALUE;
        chunkMinMax[1] = -1;
        chunkProcessing = 0;
        if(completed%(chunkSize*10) == 0){
            logStatistics();
        }
    }
    
    /**
     * Logs Statistics over all processed {@link ProcessingJob}s. This is also called
     * every 10 {@link #chunkSize} items
     */
    public void logStatistics() {
        double duration = currentTime-importingStartTime;
        log.info("Statistics:");
        log.info(" - completed " + completed + " items (" + failed.size() + " failed)");
        log.info(" - duration  " + duration/1000.0 + "sec (" + duration/(double)completed + "ms/item)");
        log.info(" - processing " + (double)processingTime/duration + "ms/item (min:" + minMaxProcessingTimes[0] + "ms | max:" + minMaxProcessingTimes[1] + "ms)");
    }
    
    public int getNumPending() {
        synchronized (registered) {
            return registered.size();
        }
    }
    
    /**
     * Read-only live list of the failed requests. Non basic access MUST BE
     * syncronised on the list while the requests are still pending as newly
     * failed requests will modify this list
     * @return
     */
    public List<T> getFailed() {
        return Collections.unmodifiableList(failed);
    }
    
    public long getNumCompleted(){
        return completed;
    }

}