package at.newmedialab.lmf.importer.test;

import static org.easymock.EasyMock.createMockBuilder;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import at.newmedialab.lmf.importer.cli.BaseCommandLine;

/**
 * Add file description here!
 *
 * @author Sebastian Schaffert (sschaffert@apache.org)
 */
public class BaseCommandLineTest {

    private BaseCommandLine<?> cli;

    @Before
    public void setup() {
        cli = createMockBuilder(BaseCommandLine.class).addMockedMethod("buildImporter").createMock();
    }

    @Test
    public void testConfiguration() throws Exception {
        File tmpFile = File.createTempFile("config",".properties");
        try {
            FileUtils.copyInputStreamToFile(this.getClass().getResourceAsStream("config-example.properties"), tmpFile);

            String[] args = new String[] { "--config", tmpFile.getAbsolutePath()};

            CommandLine line = cli.buildCommandLine(args);

            Assert.assertTrue(line.hasOption("config"));
            Assert.assertEquals(tmpFile.getAbsolutePath(), line.getOptionValue("config"));

            Configuration configuration = cli.buildConfiguration(line);
            Assert.assertEquals("http://ws.geonames.org/", configuration.getString("geonames.url"));
            Assert.assertFalse(configuration.getBoolean("archive.delete_on_success"));

        } finally {
            tmpFile.delete();
        }
    }

    /**
     * Test overwriting configuration options.
     *
     * @throws Exception
     */
    @Test
    public void testOverwrite() throws Exception {
        File tmpFile = File.createTempFile("config",".properties");
        try {
            FileUtils.copyInputStreamToFile(this.getClass().getResourceAsStream("config-example.properties"), tmpFile);

            String[] args = new String[] { "--config", tmpFile.getAbsolutePath(), "-Darchive.delete_on_success=true", "-Dgeonames.url=http://geonames.example.com/"};

            CommandLine line = cli.buildCommandLine(args);

            Assert.assertTrue(line.hasOption("config"));
            Assert.assertEquals(tmpFile.getAbsolutePath(), line.getOptionValue("config"));

            Configuration configuration = cli.buildConfiguration(line);
            Assert.assertEquals("http://geonames.example.com/", configuration.getString("geonames.url"));
            Assert.assertTrue(configuration.getBoolean("archive.delete_on_success"));


        } finally {
            tmpFile.delete();
        }
    }



}
