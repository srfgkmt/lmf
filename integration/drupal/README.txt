LMF Drupal Module
=================

The Linked Media Framework (aka LMF) is a server application that bundles central 
Semantic Web technologies to offer some advanced services.

This Drupal module provides some basic features to use LMF as backend for some 
experiments.

This project have been developed by Salzburg Research as part of the myTV project, 
partially funded by the Federal Ministry for Transport Innovation and Technology 
(BMVIT) and the Austrian Research Promotion Agency (FFG).

Setup
-----

 1. Copy the auth file template to the right one:

        cp auth.inc.tpl auth.inc

    and fill there an authorized Google API access.

 2. TODO


