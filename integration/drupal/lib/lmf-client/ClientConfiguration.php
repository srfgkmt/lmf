<?php
namespace LMFClient;

/**
 * Created by IntelliJ IDEA.
 * User: sschaffe
 * Date: 24.01.12
 * Time: 22:04
 * To change this template use File | Settings | File Templates.
 */
class ClientConfiguration
{

    protected $baseUrl;

    protected $username;

    protected $password;

    function __construct($baseUrl, $username = null, $password = null)
    {
        $this->baseUrl  = $baseUrl;
        $this->username = $username;
        $this->password = $password;
    }

    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getBaseUrlComponents($path = "") {
        $components = parse_url($this->getBaseUrl() . $path);
        if (!in_array("path", $components)) {
            $components["path"] = "";
        }
        return $components;
    }

}
