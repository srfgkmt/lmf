<?php
/**
 * Created by IntelliJ IDEA.
 * User: sschaffe
 * Date: 27.01.12
 * Time: 15:55
 * To change this template use File | Settings | File Templates.
 */

require_once 'autoload.php';

use LMFClient\ClientConfiguration;
use LMFClient\Clients\SearchClient;

$config = new ClientConfiguration("http://localhost:8080/LMF");

$client = new SearchClient($config);

foreach($client->simpleSearch("dc","summary:Sepp") as $result) {
    echo "Result: " . $result->uri . "\n";
}

?>