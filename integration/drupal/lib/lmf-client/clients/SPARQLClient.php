<?php
namespace LMFClient\Clients;

require_once 'vendor/.composer/autoload.php';
require_once 'model/content/Content.php';
require_once 'exceptions/LMFClientException.php';
require_once 'exceptions/NotFoundException.php';
require_once 'exceptions/ContentFormatException.php';

use \LMFClient\Model\Content\Content;
use \LMFClient\ClientConfiguration;

use \LMFClient\Exceptions\LMFClientException;
use \LMFClient\Exceptions\NotFoundException;
use \LMFClient\Exceptions\ContentFormatException;

use LMFClient\Model\RDF\Literal;
use LMFClient\Model\RDF\URI;
use LMFClient\Model\RDF\BNode;


use Guzzle\Http\Client;
use Guzzle\Http\Message\BadResponseException;

/**
 * A client for running SPARQL 1.1 Queries and Updates on the LMF Server.
 * User: sschaffe
 * Date: 27.01.12
 * Time: 10:18
 * To change this template use File | Settings | File Templates.
 */
class SPARQLClient
{
    protected $config;

    private static $URL_QUERY_SERVICE  = "/sparql/select?query=";
    private static $URL_UPDATE_SERVICE = "/sparql/update?query=";

    function __construct(ClientConfiguration $config)
    {
        $this->config = $config;
    }


    /**
     * Run a SPARQL Select query against the LMF Server and return the results as an array of rows, each consisting of a
     * map mapping query variable names to result values (instances of RDFNode). Results will be transfered and parsed
     * using the SPARQL JSON format.
     * @param query a SPARQL Select query to run on the database
     * @return
     * @throws IOException
     * @throws LMFClientException
     */
    public function select($query) {
        $serviceUrl = $this->config->getBaseUrl() . SPARQLClient::$URL_QUERY_SERVICE . urlencode($query);


        try {
            $client = new Client();
            $request = $client->get($serviceUrl,array(
                "User-Agent"   => "LMF Client Library (PHP)",
                "Accept" => "application/sparql-results+json"
            ));
            // set authentication if given in configuration
            if(!is_null($this->config->getUsername())) {
                $request->setAuth($this->config->getUsername(),$this->config->getPassword());
            }
            $response = $request->send();

            $sparql_result = json_decode($response->getBody(true),true);

            $variables = $sparql_result["head"]["vars"];
            $bindings  = $sparql_result["results"]["bindings"];

            $result = array();
            foreach($bindings as $binding) {
                $row = array();
                foreach($binding as $var => $value) {

                    if($value["type"] == "uri") {
                        $object = new URI($value["value"]);
                    } else if($value["type"] == "literal" || $value["type"] == "typed-literal") {
                        $object = new Literal(
                            $value["value"],
                            isset($value["language"])?$value["language"]:null,
                            isset($value["datatype"])?$value["datatype"]:null);

                    } else if($value["type"] == "bnode") {
                        $object = new BNode($value["value"]);
                    }
                    $row[$var] = $object;
                }
                $result[] = $row;
            }
            return $result;

        } catch(BadResponseException $ex) {
            throw new LMFClientException("error evaluating SPARQL Select Query $query; ".$ex->getResponse()->getReasonPhrase());
        }

    }


    /**
     * Carry out a SPARQL ASK Query and return either true or false, depending on the query result.
     *
     * @param askQuery
     * @return boolean
     * @throws IOException
     * @throws LMFClientException
     */
    public function ask($askQuery) {
        $serviceUrl = $this->config->getBaseUrl() . SPARQLClient::$URL_QUERY_SERVICE . urlencode($askQuery);


        try {
            $client = new Client();
            $request = $client->get($serviceUrl,array(
                "User-Agent"   => "LMF Client Library (PHP)",
                "Accept" => "application/sparql-results+json"
            ));
            // set authentication if given in configuration
            if(!is_null($this->config->getUsername())) {
                $request->setAuth($this->config->getUsername(),$this->config->getPassword());
            }
            $response = $request->send();

            $body = str_replace("boolean:","\"boolean\":",str_replace("head:","\"head\":",(string)$response->getBody(true)));
            $sparql_result = json_decode($body,true);

            if(count($sparql_result) == 0) {
                return False;
            } else {
                $result  = $sparql_result["boolean"];
                if($result == "true") {
                    return True;
                } else {
                    return False;
                }
            }



        } catch(BadResponseException $ex) {
            throw new LMFClientException("error evaluating SPARQL Ask Query $askQuery; ".$ex->getResponse()->getReasonPhrase());
        }
    }


    /**
     * Execute a SPARQL Update query according to the SPARQL 1.1 standard. The query will only be passed to the server,
     * which will react either with ok (in this the method simply returns) or with error (in this case, the method
     * throws an LMFClientException).
     *
     * @param updateQuery         the SPARQL Update 1.1 query string
     * @throws IOException        in case a connection problem occurs
     * @throws LMFClientException in case the server returned and error and did not execute the update
     */
    public function update($updateQuery) {
        $serviceUrl = $this->config->getBaseUrl() . SPARQLClient::$URL_UPDATE_SERVICE . urlencode($updateQuery);


        try {
            $client = new Client();
            $request = $client->get($serviceUrl,array(
                "User-Agent"   => "LMF Client Library (PHP)"
            ));
            // set authentication if given in configuration
            if(!is_null($this->config->getUsername())) {
                $request->setAuth($this->config->getUsername(),$this->config->getPassword());
            }
            $response = $request->send();


        } catch(BadResponseException $ex) {
            throw new LMFClientException("error evaluating SPARQL Update Query $updateQuery; ".$ex->getResponse()->getReasonPhrase());
        }
    }
}
