<?php
/**
 * Created by IntelliJ IDEA.
 * User: sschaffe
 * Date: 27.01.12
 * Time: 14:13
 * To change this template use File | Settings | File Templates.
 */

require_once 'autoload.php';

use LMFClient\ClientConfiguration;
use LMFClient\Clients\ImportClient;
use LMFClient\Clients\ResourceClient;

$config = new ClientConfiguration("http://localhost:8080/LMF");

$client = new ImportClient($config);

// list supported types
echo "Supported Types: ";
foreach($client->getSupportedTypes() as $type) {
    echo $type . ",";
}
echo "\n";


// import a simple data set
$data = "<http://example.com/resource/r1> <http://example.com/resource/p1> \"Test Data\".";
$client->uploadDataset($data,"text/rdf+n3");


// wait a bit for import to finish
sleep(1);


$rclient = new ResourceClient($config);

foreach($rclient->getResourceMetadata("http://example.com/resource/r1") as $property => $value) {
    echo $property . " = " . $value[0] . "\n";
}




?>