<?php

function listContentTypes() {
    $content_types = array();
    foreach(node_type_get_types() as $type) {
        $content_types[$type->type] = $type->name;
    }
    return $content_types;
}

function listNodes($content_type) {
    //return listNodesEntitiesImpl(($content_type);
    return listNodesViewsImpl($content_type);
}

function listNodesViewsImpl($content_type) {
    return node_load_multiple(array(), array("type" => $content_type));
}

function listNodesEntitiesImpl($content_type) {
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition("entity_type", "node")
                      ->entityCondition("bundle", $content_type)
                      ->propertyCondition("status", 1)
                      ->execute();

    $nodes = entity_load("node", array_keys($entities["node"]));
    $nids = array();
    foreach($nodes as $node) {
        var_dump($node);
    }
    return $nids;
}

?>
