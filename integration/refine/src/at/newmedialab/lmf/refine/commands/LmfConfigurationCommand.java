
package at.newmedialab.lmf.refine.commands;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.newmedialab.lmf.refine.model.LmfConfigurationOverlayModel;

import com.google.refine.ProjectManager;
import com.google.refine.commands.Command;
import com.google.refine.model.Project;

public class LmfConfigurationCommand extends Command {

    private static Logger log = LoggerFactory.getLogger(LmfConfigurationCommand.class);
    private static final String URI = "uri";
    private static final String USER = "user";
    private static final String PASS = "pass";
    private static final String CTX = "context";
    private static final String RESET = "reset";

    /**
     * Return the current configuration
     * 
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        LmfConfigurationOverlayModel overlay = this.retrieveOverlay(req);
        Writer writer = res.getWriter();
        JSONWriter jsonWriter = new JSONWriter(writer);
        try {
            overlay.write(jsonWriter, null);
        } catch (JSONException e) {
            res.sendError(500, e.getMessage());
        }
        writer.write("\n");
        writer.flush();
    }

    /**
     * Saves the LMF configuration
     * 
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        ProjectManager.singleton.setBusy(true);
        try {
            Project project = this.getProject(req);
            String uri = req.getParameter(URI);
            String user = req.getParameter(USER);
            String pass = req.getParameter(PASS);
            String ctx = req.getParameter(CTX);
            boolean reset = ("true".equalsIgnoreCase(req.getParameter(RESET)) ? true : false);
            LmfConfigurationOverlayModel overlay = this.retrieveOverlay(req);
            overlay.setUri(uri);
            overlay.setUser(user);
            overlay.setPass(pass);
            overlay.setContext(ctx);
            overlay.setReset(reset);
            project.overlayModels.put(LmfConfigurationOverlayModel.NAME, overlay);
            project.setLastSave();
            log.info("LMF Configuration saved for project " + project.id + " (" + ctx + ")");
            res.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            String msg = "Error saving configuration: " + e.getMessage();
            log.error(msg);
            throw new ServletException(msg, e);
        } finally {
            ProjectManager.singleton.setBusy(false);
        }
    }

    /**
     * Retrieve the overlay for this project
     * 
     * @param project
     *            project
     * @return overlay
     * @throws ServletException
     */
    private LmfConfigurationOverlayModel retrieveOverlay(HttpServletRequest req) {
        Project project = null;
        try {
            project = this.getProject(req);
        } catch (ServletException e) {
            log.error("Error retrieving project: " + e.getMessage());
        }
        if (project == null) {
            return new LmfConfigurationOverlayModel();
        } else {
            LmfConfigurationOverlayModel overlay = (LmfConfigurationOverlayModel) project.overlayModels
                    .get(LmfConfigurationOverlayModel.NAME);
            if (overlay == null) {
                overlay = new LmfConfigurationOverlayModel(req);
                project.overlayModels.put(LmfConfigurationOverlayModel.NAME, overlay);
                project.getMetadata().updateModified();
            }
            return overlay;
        }
    }

}
