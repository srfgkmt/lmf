
LMF = {};

LMF.getUri = function() {
	if (theProject.overlayModels.lmf) {
		return theProject.overlayModels.lmf.uri;
	} else {
		return undefined;
	}
}

LMF.getBaseUri = function() {
	return LMF.getUri() + "/resource/";
}

LMF.save = function() {
	if (!theProject.overlayModels.rdfSchema) {
		alert("You haven't done any RDF schema alignment yet!");
	} else {
		var dismissBusy = DialogSystem.showBusy("Importing RDF data into LMF...");
		$.post("command/lmf/import", 
				{
					"engine" : JSON.stringify(ui.browsingEngine.getJSON()),
					"project" : theProject.id
				}, function(data) {
					dismissBusy();
					alert("saved!");
				}, 
				"json")
				.error(function() {
					dismissBusy();
					alert("An error ocurred saving the data. Please, check your LMF configuration and service availability.");
				});
	}
}

LMF.go = function() {
	window.open(LMF.getUri());
}

LMF.buildContext = function(base) {
	if (theProject.metadata && theProject.metadata.name) {
		return base + "/context/refine/" + theProject.metadata.name.encodeURL();
	} else {
		return base + "/context/refine";
	}
}

LMF.readConfiguration = function(handler) {
	$.ajax({
		url : "command/lmf/configuration",
		data : { "project" : theProject.id },
		dataType : "json",
		success : handler
	});
}

LMF.saveConfiguration = function(uri, user, pass, context, reset, handler) {
	$.post("command/lmf/configuration", {
		"uri" : uri,
		"user" : user,
		"pass" : pass,
		"context" : context,
		"reset" : reset,
		"project" : theProject.id
	}, handler, "json");
}

LMF.writeOverlay = function(uri, user, pass, context, reset) {
	if (!theProject.overlayModels.lmf) {
		theProject.overlayModels.lmf = {};
	}
	theProject.overlayModels.lmf.uri = normalizeUri(uri);
	theProject.overlayModels.lmf.user = user;
	theProject.overlayModels.lmf.pass = pass;
	if (context.length > 0) {
		theProject.overlayModels.lmf.context = context;
	} else {
		theProject.overlayModels.lmf.context = LMF.buildContext(uri);
	}
	theProject.overlayModels.lmf.reset = reset;
}

//some ugly initialization code
$(document).ready(
	function() {
		if (!theProject.overlayModels) {
			theProject.overlayModels = {};
		}
		if (!theProject.overlayModels.lmf) {
			LMF.readConfiguration(function(data) {
				
				//initialize overlay
				LMF.writeOverlay(data.uri, data.user, data.pass, data.context, data.reset);
				LMF.saveConfiguration(theProject.overlayModels.lmf.uri,
						theProject.overlayModels.lmf.user,
						theProject.overlayModels.lmf.pass,
						theProject.overlayModels.lmf.context,
						theProject.overlayModels.lmf.reset, function() {});
				
				//pre-registration of stanbol if it's running locally (typically inside lmf)
				var stanbol = theProject.overlayModels.lmf.uri + "/stanbol/config/";
				$.ajax({
					url: stanbol,
					type: "HEAD",
					success: function(res) {
	                		$.post("command/lmf/inspect-stanbol",
	        				    {
	        					    "uri": stanbol,
	        					    "engine": JSON.stringify(ui.browsingEngine.getJSON()),
	        					    "project": theProject.id
	        				    },
	        				    function(data) {
	        				    	$.each(data, function(i, obj) {
	        				    		//check issue #579: http://code.google.com/p/google-refine/issues/detail?id=579
	        				    		if (ReconciliationManager.getServiceFromUrl(obj.uri)) {
	        				    			self.printAddedService(registering, obj, false)
	        				    		} else {
	        					    	    ReconciliationManager.registerStandardService(obj.uri, function(index) {
	        					    	    	self.printAddedService(registering, obj, true)
	        					    	    });	
	        				    		}
	        				    	});
	        		            },
	        	                "json");
	    			},
	    			error: function(jqXHR, textStatus, errorThrown) {}
	    		});
				
    			//pre-registration of the lmf sparql endpoint
    			//TODO: add a flag to avoid duplicate registration, which causes a backend exception
    			var lmfSparqlEndpoint = theProject.overlayModels.lmf.uri + "/sparql/select";				
    			$.post("command/rdf-extension/addService",
    					{
    						datasource: "sparql",
    						name: "LMF SPARQL Endpoint",
    						url: lmfSparqlEndpoint,
    						type: "plain",
    						properties: "http://www.w3.org/2000/01/rdf-schema#label \nhttp://www.w3.org/2004/02/skos/core#prefLabel \nhttp://www.w3.org/2004/02/skos/core#altLabel \nhttp://purl.org/dc/terms/title \nhttp://purl.org/dc/elements/1.1/title \nhttp://xmlns.com/foaf/0.1/name"
    					},
    					function(data) {
    						if (data.service && data.service.id) {
	    						var url = location.href;  // entire url including querystring - also: window.location.href;
	    						var baseURL = url.substring(0,url.lastIndexOf('/'));
	    						var service_url = baseURL + '/extension/rdf-extension/services/' + data.service.id;
	    						if (!ReconciliationManager.getServiceFromUrl(lmfSparqlEndpoint)) {
	    							RdfReconciliationManager.registerService(data);			
	    						}
    						}
    					},
    					"json");
    			
    			/*
    			var lmfPrefixes = theProject.overlayModels.lmf.uri + "/prefix";
    			$.getJSON(lmfPrefixes, function(data) {
    				$.each(data, function(prefix, namespace) {
    	    			$.post("command/rdf-extension/add-prefix",
    	    					{
    	    						name: prefix,
    	    						uri: namespace,
    	    						fetch-url: namespace,
    	    						fetch: "web",
    	    						project: theProject.id
    	    					},
    	    					function(data) {});
    				});
    			});  			
				*/
    			
			});
		}
});
