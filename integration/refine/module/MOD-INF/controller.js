
var html = "text/html";
var encoding = "UTF-8";
var ClientSideResourceManager = Packages.com.google.refine.ClientSideResourceManager;

/*
 * Extension initialization
 */
function init() {
    //Packages.java.lang.System.err.println("Initializing sample extension");
    //Packages.java.lang.System.err.println(module.getMountPoint());

    //Overlay models
    Packages.com.google.refine.model.Project.registerOverlayModel("lmf", Packages.at.newmedialab.lmf.refine.model.LmfConfigurationOverlayModel);

    //Commands
    var GRefineServlet = Packages.com.google.refine.RefineServlet;
    GRefineServlet.registerCommand(module, "configuration", new Packages.at.newmedialab.lmf.refine.commands.LmfConfigurationCommand());
    GRefineServlet.registerCommand(module, "import", new Packages.at.newmedialab.lmf.refine.commands.LmfImportCommand());
    GRefineServlet.registerCommand(module, "inspect-stanbol", new Packages.at.newmedialab.lmf.refine.commands.StanbolReconciliationInspectCommand());

    // Script files to inject into /project page
    ClientSideResourceManager.addPaths(
        "project/scripts",
        module,
        [
         	"scripts/common.js",
            "scripts/overlay.js",
            "scripts/lmf.js",
            "scripts/lmf-configuration.js",
            "scripts/stanbol-reconciliation-configuration.js"
        ]
    );

    // Style files to inject into /project page
    ClientSideResourceManager.addPaths(
        "project/styles",
        module,
        [
            "styles/lmf.css"
        ]
    );

}
