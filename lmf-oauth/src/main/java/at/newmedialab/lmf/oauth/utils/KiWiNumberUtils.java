/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.utils;

/**
 * @author Stephanie Stroka
 *         User: sstroka
 *         Date: 23.11.2011
 *         Time: 13:34:29
 */
public class KiWiNumberUtils {

    /**
     * Returns the byte array for an integer
     * @param value
     * @return
     */
    public static final byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }

    /**
     * Returns the unsigned int representation of
     * a (signed) int by converting it to a long
     * and shifting the bytes.
     *  
     * @param i
     * @return
     */
    public static final long toUnsignedInt(int i) {
        long anUnsignedInt = 0;

        int firstByte = 0;
        int secondByte = 0;
        int thirdByte = 0;
        int fourthByte = 0;

        byte buf[] = intToByteArray(i);
        // Check to make sure we have enough bytes
        if(buf.length < 4) {
            System.err.println("buffer length: " + buf.length);
            throw new NumberFormatException();
        }
        int index = 0;

        firstByte = (0x000000FF & ((int)buf[index]));
        secondByte = (0x000000FF & ((int)buf[index+1]));
        thirdByte = (0x000000FF & ((int)buf[index+2]));
        fourthByte = (0x000000FF & ((int)buf[index+3]));
//        index = index+4;
        anUnsignedInt  = ((long) (firstByte << 24
                            | secondByte << 16
                            | thirdByte << 8
                            | fourthByte))
                           & 0xFFFFFFFFL;

        return anUnsignedInt;
    }
}
