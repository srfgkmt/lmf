/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.utils;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Stephanie Stroka
 *
 * This utility class creates ServletResponses with JSON messages.
 */
public class KiWiJSONresponse {

    private static Logger log = LoggerFactory.getLogger(KiWiJSONresponse.class);

    public static byte[] buildJSONresponseEntity(Map<String,String> message) {
        ObjectMapper objectMapper = new ObjectMapper();

        byte[] ret = null;
        try {
            ret = objectMapper.writeValueAsBytes(message);
            return ret;
        } catch (IOException e) {
            log.error("Could not construct JSON message");
            return null;
        }
    }

    // TODO: change type of exception
    public static byte[] buildJSONresponseEntity(String[] message) throws ArrayIndexOutOfBoundsException {

        if(message.length != 2) {
            throw new ArrayIndexOutOfBoundsException("Expected 2 arguments for the message: {key, value}");
        }
        Map<String,String> msg = new HashMap<String,String>();
        msg.put(message[0], message[1]);

        return buildJSONresponseEntity(msg);
    }
}
