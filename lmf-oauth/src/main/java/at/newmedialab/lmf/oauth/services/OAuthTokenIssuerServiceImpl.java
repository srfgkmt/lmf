/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.services;

import at.newmedialab.lmf.oauth.model.AccessToken;
import at.newmedialab.lmf.oauth.model.RefreshToken;

import at.newmedialab.lmf.oauth.exception.AccessTokenNotValidException;

import at.newmedialab.lmf.oauth.api.CookieService;
import at.newmedialab.lmf.oauth.api.OAuthTokenIssuerService;
import at.newmedialab.lmf.oauth.api.TokenManagerService;

import kiwi.core.api.config.ConfigurationService;
import kiwi.core.model.user.KiWiUser;
import org.apache.amber.oauth2.as.issuer.MD5Generator;
import org.apache.amber.oauth2.as.issuer.OAuthIssuer;
import org.apache.amber.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.amber.oauth2.as.request.OAuthTokenRequest;
import org.apache.amber.oauth2.as.response.OAuthASResponse;
import org.apache.amber.oauth2.common.exception.OAuthProblemException;
import org.apache.amber.oauth2.common.exception.OAuthSystemException;
import org.apache.amber.oauth2.common.message.OAuthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;

/**
 * OAuthTokenIssuerService
 * User: Stephanie Stroka
 * Date: 06.05.2011
 * Time: 13:17:47
 */
public class OAuthTokenIssuerServiceImpl implements OAuthTokenIssuerService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private TokenManagerService tokenManagerService;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private CookieService cookieService;

    /**
     * constructOAuthResponse() constructs an OAuthResonse entity for a user, which
     * can be sent to the client. The constructed accessToken and refreshToken
     * are stored in memory together with the expiration time and the associated
     * user.
     * @param request
     * @param user
     * @return
     */
    public Response constructOAuthResponse(HttpServletRequest request, KiWiUser user, String redirect_uri) {
        OAuthTokenRequest oauthRequest = null;
        OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
        try {
            if(request != null) {
                Enumeration<String> parameterNames = request.getParameterNames();
            }
            oauthRequest = new OAuthTokenRequest(request);

            if(oauthRequest == null) {
                log.error("OAuthRequest received a bad HttpServletRequest.");
                 OAuthResponse r = null;
                try {
                    return buildASErrorResponse
                            (400, OAuthProblemException.error("OAuthRequest received a bad HttpServletRequest"));
                    
                } catch (OAuthSystemException e1) {
                    e1.printStackTrace();
                }
                return Response.status(400).entity("OAuthRequest received a bad HttpServletRequest").build();
            }

            String accessToken = generateAccessToken(oauthIssuerImpl);
            String refreshToken = generateRefreshToken(oauthIssuerImpl);
            Integer exp_Int = getDefaultExpiration();

            String expStr = exp_Int.toString();

            Response r = buildASTokenResponse(accessToken, expStr, refreshToken);

            try {
                tokenManagerService.setToken(user, accessToken, refreshToken, exp_Int.intValue());
            } catch (AccessTokenNotValidException e) {
                e.printStackTrace();
                return buildASErrorResponse(500,
                        OAuthProblemException.error("Access token was invalid."));
            }
            NewCookie c = cookieService.buildCookie("oauth_token", accessToken, exp_Int);

            if(redirect_uri != null) {
                try {
                    URI uri = new URI(redirect_uri);
                    return Response.seeOther(uri).cookie(c).build();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST)
                            .entity("Redirect to " + redirect_uri + " failed")
                            .build();
                }
            }
            return r;

        } catch (OAuthSystemException e) {
            Response r = null;
            try {
                r = buildASErrorResponse(
                        401, OAuthProblemException.error(e.getMessage()));
            } catch (OAuthSystemException e1) {
                e1.printStackTrace();
            }

            log.debug(r.getEntity().toString());

            return Response.status(500).build();
        } catch (OAuthProblemException e) {
            Response r = null;
            try {
                r = buildASErrorResponse(401, e);
            } catch (OAuthSystemException e1) {
                e1.printStackTrace();
            }

            log.debug(r.getEntity().toString());

            return Response.fromResponse(r).build();
        }
    }

        /**
     * refreshToken() constructs a new access token for a user
     * @param refreshToken
     * @return newAccessToken, a new access token assigned for the user holding the
     *          given refreshToken.
     */
    @Override
    public Response refreshOAuthToken(String refreshToken) {

        Response r = null;
        String newAccessToken = null;
        RefreshToken refreshTokenEntity;
        if((refreshTokenEntity = tokenManagerService.getRefreshToken(refreshToken)) == null) {
            log.error("Refresh token could not be found.");
            return null;
        }

        // remove old access token
        AccessToken accessTokenEntity = refreshTokenEntity.getAccessToken();
        KiWiUser user = accessTokenEntity.getKiwiUser();
        tokenManagerService.removeAccessToken(accessTokenEntity.getAccessToken());

        OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

        try {
            newAccessToken = generateAccessToken(oauthIssuerImpl);

            int exp = getDefaultExpiration();
            try {
                tokenManagerService.setToken(user, newAccessToken, refreshToken, exp);
            } catch (AccessTokenNotValidException e) {
                e.printStackTrace();
                buildASErrorResponse(401, OAuthProblemException.error("New access token is broken. Refreshing failed."));
            }

            r = buildASTokenResponse(newAccessToken, String.valueOf(exp), refreshToken);

        } catch (OAuthSystemException e) {
            e.printStackTrace();
            log.error("{}", e.getMessage());
        }
        return r;
    }

    private Response buildASTokenResponse(String accessToken, String expiration, String refreshToken) throws OAuthSystemException {
        OAuthResponse r = OAuthASResponse
                .tokenResponse(HttpServletResponse.SC_OK)
                .setAccessToken(accessToken)
                .setExpiresIn(expiration)
                .setRefreshToken(refreshToken)
                .buildJSONMessage();
        return Response.status(r.getResponseStatus()).entity(r.getBody()).build(); 
    }

    private Response buildASErrorResponse(int statuscode, OAuthProblemException e) throws OAuthSystemException {
        OAuthResponse r = OAuthResponse
                .errorResponse(statuscode)
                .error(e)
                .buildJSONMessage();
        return Response.status(r.getResponseStatus()).entity(r.getBody()).build(); 
    }

    private String generateAccessToken(OAuthIssuer oauthIssuer) throws OAuthSystemException {
        return oauthIssuer.accessToken();
    }

    private String generateRefreshToken(OAuthIssuer oauthIssuer) throws OAuthSystemException {
        return oauthIssuer.refreshToken();
    }

    private int getDefaultExpiration() {
        int exp;
        if(configurationService.getConfiguration("session_expiration") == null) {
            exp = 3600;
            configurationService.setConfiguration("session_expiration", String.valueOf(3600));
        } else {
            String expStr = (String) configurationService.getConfiguration("session_expiration");
            log.debug("expStr: {}", expStr);

            exp = Integer.parseInt(expStr);
        }
        return exp;
    }
}
