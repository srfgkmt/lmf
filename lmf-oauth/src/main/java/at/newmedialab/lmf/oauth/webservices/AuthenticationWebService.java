/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.webservices;

import at.newmedialab.lmf.oauth.api.OAuthTokenIssuerService;
import at.newmedialab.lmf.oauth.api.TokenManagerService;

import at.newmedialab.lmf.user.api.AuthenticationService;
import com.google.common.base.Preconditions;
import kiwi.core.api.config.ConfigurationService;
import kiwi.core.api.facading.FacadingService;
import kiwi.core.api.transaction.Transaction;
import kiwi.core.api.transaction.TransactionService;
import kiwi.core.api.user.UserService;
import kiwi.core.exception.UserExistsException;
import kiwi.core.model.rdf.KiWiResource;
import kiwi.core.model.rdf.KiWiUriResource;
import kiwi.core.model.user.KiWiUser;
import kiwi.core.model.user.OnlineAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Stephanie Stroka
 * AuthenticationWebService.java
 * User: Stephanie Stroka
 * Date: 18.02.2011
 * Time: 12:21:21
 */
@Path("auth")
public class AuthenticationWebService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private UserService userService;

    @Inject
    private TokenManagerService tokenManagerService;

    @Inject
    private TransactionService transactionService;

    @Inject
    private FacadingService facadingService;

    @Inject
    private OAuthTokenIssuerService oauthTokenIssuerService;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private AuthenticationService authenticationService;

    /**
     * registerUser() registers a new user. Returns status 200 if the registration was successful.
     * Returns status 403 if the registration was not successful, e.g. if
     * a user with the same name already existed.
     * @param username the user name for login
     * @param password the login password
     * @return 403 if the registration was not successful, 200 if registration was successful
     * @HTTP 403 registration not successful
     * @HTTP 200 registration successful
     */
    @Path("/register")
    @POST
    @Produces("text/plain")
    @Consumes("application/x-www-form-urlencoded")
    public Response registerUser(
            @FormParam("username") String username,
            @FormParam("password") String password,
            @FormParam("onlineaccount") String online_account,
            @QueryParam("redirect_uri") String redirect_uri) {
        try {
            Preconditions.checkArgument(username != null);
            Preconditions.checkArgument(password != null);

            Transaction tx = transactionService.getTransaction();
            transactionService.begin(tx);
            KiWiResource userResource = userService.createUser(username);
            KiWiUser user = facadingService.createFacade(userResource, KiWiUser.class, (KiWiUriResource) userResource);
            if(online_account != null) {
                OnlineAccount account = facadingService.createFacade(online_account, OnlineAccount.class);
                Set<OnlineAccount> accounts = user.getOnlineAccounts();
                if(accounts == null) {
                    accounts = new HashSet<OnlineAccount>();
                }
                accounts.add(account);
                user.setOnlineAccounts(accounts);
            }
            transactionService.commit(tx);
            if(redirect_uri != null) {
                URI uri = null;
                try {
                    uri = new URI(redirect_uri);
                    return Response.seeOther(uri).build();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST)
                            .entity("Redirect to " + redirect_uri + " failed")
                            .build();
                }
            }
            authenticationService.setUserPassword(username,password);
            return Response.status(Response.Status.OK).build();
        } catch (UserExistsException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return Response.status(Response.Status.FORBIDDEN)
                    .entity("Cannot create user. User " + username + " already exists")
                    .build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Cannot create user. The request was invalid. "+ex.getMessage())
                    .build();
        }
    }

    /**
     * Build an XHTML representation for a register form if the user logged in for
     * the first time with his/her (external) oauth2 account.
     * @param username the user name that was extracted from the online account data
     * @return An XHTML response entity
     */
    private Response buildRegisterFirstForm(String username, String redirect_uri) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\"?> \n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"> \n");
        sb.append("<head>");
        sb.append("<title>\n");
        sb.append("LMF - Welcome " + username + "\n");
        sb.append("<style type=\"text/css\" media=\"screen\">@import \"" + configurationService.getServerUri() + "css/basic.css\";</style> \n" +
                "\t<style type=\"text/css\" media=\"screen\">@import \"" + configurationService.getServerUri() + "css/tabs.css\";</style> \n");
        sb.append("</title> \n");
        sb.append("</head>");

        sb.append("<body>");
        sb.append("<div id='registerForm'>");
        sb.append("<h3>");
        sb.append("Please register your account with LMF");
        sb.append("</h3>");
        sb.append("<br/>");
        if(redirect_uri != null) {
            sb.append("<form method='POST' action='" + configurationService.getServerUri() + "auth/register?redirect_uri=" + redirect_uri + "'>");
        } else {
            sb.append("<form method='POST' action='" + configurationService.getServerUri() + "auth/register'>");
        }
        sb.append("Username: ");
        sb.append("<input name='username' type='text' value='"+username+"'></input>");
        sb.append("<br/>");
        sb.append("Password: ");
        sb.append("<input name='password' type='password' value=''></input>");
        sb.append("<br/>");
        sb.append("<input name='Register new LMF account' type='submit'></input>");
        sb.append("</form>");
        sb.append("</div>");
        sb.append("</body>");

        sb.append("</html>");

        return Response.status(200).entity(sb.toString().getBytes()).build();
    }

    /**
     * authorizeUser() validates the given username and password hash against an existing user in the system.
     * If a user could be identified and authenticated, the response status is set to 200 and an OAuth2
     * accessToken, refreshToken and tokenExpiration is returned as JSON message.
     * If no user was found or if the given passwordHash did not match the stored passwordHash, status
     * 403 is returned.
     * @param username
     * @param password
     * @param request
     * @return HTTP 200 if user could be authenticated, HTTP 403 if passwordHash did not match the stored password.
     *         (body is a JSON message)
     * @HTTP 200 user successfully authenticated
     * @HTTP 403 user authentication failed
     */
    // TODO: add mime type!
    @Path("/login")
    @POST
    @Produces( {"application/json", "application/xhtml+xml"} )
    public Response login(
            @FormParam("username") String username,
            @FormParam("password") String password,
            @FormParam("client_id") String client_id,
            @QueryParam("redirect_uri") String redirect_uri,
            @Context HttpServletRequest request) {
        log.debug("Trying to authorize user {} ", username);

        if(username == null || password == null) return Response.status(Response.Status.FORBIDDEN).
                entity("{ error: \"User authentication failed\" }").build();

        if(client_id == null) {
            client_id = "1";
        }

        Transaction tx = transactionService.getTransaction();
        transactionService.begin(tx);
        KiWiResource userResource = userService.getUser(username);
        KiWiUser userFacade = facadingService.createFacade(userResource,KiWiUser.class);

        if(userResource == null || !authenticationService.authenticateUser(username, password)) //            return Response.status(Response.Status.UNAUTHORIZED).
            //                    entity("{ error: \"User authentication failed\" }").build();
            return buildRegisterFirstForm(username, redirect_uri);

        transactionService.commit(tx);

        Response r = oauthTokenIssuerService.constructOAuthResponse(request, userFacade, redirect_uri);
        return r;
    }

    /**
     * refreshAccessToken() creates a new access token for a user who holds a refresh token
     * @param grant_type the grant_type parameter according to the oauth2 implementation
     * @param refresh_token the refresh token that has been provided to the user
     * @param request
     * @return HTTP response (body is a JSON message)
     * @HTTP 403 grant_type was not 'refresh_token'
     * @HTTP 200 refresh token was ok, new access token is returned.
     * @HTTP 500 refresh token was corrupt
     */
    @Path("/refresh")
    @POST
    @Produces("application/json")
    public Response refreshAccessToken(
            @FormParam("grant_type") String grant_type,
            @FormParam("refresh_token") String refresh_token,
            @Context HttpServletRequest request) {

        if(!grant_type.equals("refresh_token")) return Response.status(Response.Status.FORBIDDEN).
                entity("{ error: \"Expected grant_type 'refresh_token'\" }").build();

        log.debug("Refresh token {} ", refresh_token);

        Response r;
        if((r = oauthTokenIssuerService.refreshOAuthToken(refresh_token)) != null) {
            log.debug(r.getEntity().toString());

            return r;
        } else
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                    entity("{ error: \"The refresh token might be corrupt\" }").build();
    }

    /**
     * logout() removes the access token of the user from memory.
     * @param token
     * @return HTTP response (body is a String message)
     * @HTTP 200 user login was successful
     * @HTTP 406 access token not provided
     */
    @Path("/logout")
    @GET
    @Produces("text/plain")
    public Response logout(
            @QueryParam("oauth_token") String token) {
        if(token == null) return Response.status(
                Response.Status.NOT_ACCEPTABLE)
                .entity("Please provide an oauth token as GET parameter 'oauth_token'")
                .build();
        tokenManagerService.removeAccessToken(token);
        tokenManagerService.removeRefreshToken(token);
        return Response.ok("Successfully logged out").build();
    }
}
