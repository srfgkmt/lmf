/**
 * Copyright (C) 2013 Salzburg Research.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.newmedialab.lmf.oauth.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: sstroka
 * Date: 08.08.2011
 * Time: 17:42:35
 * To change this template use File | Settings | File Templates.
 */
public class KiWiUrlUtils {

    /**
     * getHost() renames urls containing 127.0.0.1 to localhost.
     * This helper method is necessary to enable requests to facebooks oauth2
     * services, as they do not accept base urls containing ip addresses.
     * @param baseUri the uri of the server implementation
     * @return a url containing localhost instead of 127.0.0.1 as string
     */
    public static String getHost(String baseUri) {
        Pattern p = Pattern.compile("127\\.0\\.0\\.1");
        Matcher m = p.matcher(baseUri);
        StringBuffer hostname_builder = new StringBuffer();
        boolean found = m.find();
        if(found) {
            m.appendReplacement(hostname_builder, "localhost");
        }
        m.appendTail(hostname_builder);
        return hostname_builder.toString();
    }

}
